# coding: utf-8

import sys

from direct.showbase.ShowBase import ShowBase
from direct.gui.DirectGui import *



class MyInterface(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)

        # Image d'arrière plan
        self.background = OnscreenImage(parent = render2d, image = 'PandaAtWar.jpg')

        # mapsInfo est ici un exemple de ce qu'un DTO pourrait retourner pour 
        # populer le menu.
        # Il est constitué d'une liste de tuple de map construit par : 
        #   - le nom de la map
        #   - l'id de la map dans la bd
        mapsInfo =   [ 
                        ('Cool map', 3),
                        ('Carte magique', 5),
                        ('Tankem-all!', 6),
                        ('Champ de guerre', 7),
                        ('Carte test 1', 8),
                        ('Carte test 3', 9),
                        ('Carte test 666', 10),
                        ('Awesome map', 12),
                        ('Another map', 18),
                        ('Une carte avec un nom vraiment long inutilement pour faire des tests bidons d''affichage', 22),
                        ('WMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWM', 72),
                        ('Blood bath', 666)
                    ]
        # Ou la même structure avec une génération automatique.
        # mapsInfo = [('Level {0}'.format(i), i) for i in range(0, 10)]

        self.controlTextScale = 0.10
        self.controlBorderWidth = (0.005, 0.005)

        self.scrollItemButtons = self.createAllItems(mapsInfo)

        verticalOffsetControlButton = 0.225
        verticalOffsetCenterControlButton = -0.02
        self.myScrolledListLabel = DirectScrolledList(
                decButton_pos = (0.0, 0.0, verticalOffsetControlButton + verticalOffsetCenterControlButton),
                decButton_text = "Monter",
                decButton_text_scale = 0.08,
                decButton_borderWidth = (0.0025, 0.0025),
                decButton_frameSize = (-0.35, 0.35, -0.0375, 0.075),
                decButton_text_fg = (0.15, 0.15, 0.75, 1.0),

                incButton_pos = (0.0, 0.0, -0.625 - verticalOffsetControlButton + verticalOffsetCenterControlButton),
                incButton_text = "Descendre",
                incButton_text_scale = 0.08,
                incButton_borderWidth = (0.0025, 0.0025),
                incButton_frameSize = (-0.35, 0.35, -0.0375, 0.075),
                incButton_text_fg = (0.15, 0.15, 0.75, 1.0),

                pos = (0, 0, 0.5),

                items = self.scrollItemButtons,
                numItemsVisible = 5,
                forceHeight = 0.175,
                
                frameSize = (-1.05, 1.05, -0.95, 0.325),
                frameColor = (0.5, 0.5, 0.5, 0.75),

                itemFrame_pos = (0.0, 0.0, 0.0),
                itemFrame_frameSize = (-1.025, 1.025, -0.775, 0.15),
                itemFrame_frameColor = (0.35, 0.35, 0.35, 0.75),
                itemFrame_relief = 1
            )

        self.quitButton = DirectButton(
                text = ("Quitter", "Quitter", "Quitter", "disabled"),
                text_scale = self.controlTextScale,
                borderWidth = self.controlBorderWidth,
                relief = 2,
                pos = (0.0, 0.0, -0.75),
                frameSize = (-0.5, 0.5, -0.0625, 0.105),
                command = lambda : sys.exit(),
            )

    def createItemButton(self, mapName, mapId):
        return DirectButton(
                text = mapName,
                text_scale = self.controlTextScale, 
                borderWidth = self.controlBorderWidth, 
                relief = 2,
                frameSize = (-1.0, 1.0, -0.0625, 0.105),
                command = lambda: self.loadGame(mapId))

    def createAllItems(self, mapsInfo):
        scrollItemButtons = [self.createItemButton(u'-> Carte aléatoire <-', None)]
        for mapInfo in mapsInfo:
            scrollItemButtons.append(self.createItemButton(self.formatText(mapInfo[0]), mapInfo[1]))
        return scrollItemButtons

    def formatText(self, text, maxLength = 20):
        return text if len(text) <= maxLength else text[0:maxLength] + '...'

    def loadGame(self, mapId):
        # À ajuster... évidemment...
        print mapId


    # Ne pas oublier qu'il y a des ajustements à faire dans la fonction 
    # 'cacher' de Tankem et à d'autres endroits.


i = MyInterface()
i.run()


