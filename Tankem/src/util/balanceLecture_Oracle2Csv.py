import sys
import os.path

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import common.internal.dao.dao_oracle_balance as oracle
import common.internal.dao.dao_csv as csv
import Tkinter
import tkMessageBox
import sysconfig
import cx_Oracle


# Connexion a la BD et gestion de l'erreur si ca fonctionne pas
try:
	dao_oracle = oracle.DAO_Oracle_Balance()
	dao_oracle.connection_db()
except cx_Oracle.DatabaseError as e:
	tkMessageBox.showinfo("Erreur de connexion", "Malheureusement, la connexion avec la base de donnee ne s'est pas effectuee")
	exit()

# Lecture de la BD, la fonctionne retourne les 3 DTO
dao_csv = csv.DAO_Csv()
listeDAO = dao_oracle.retrieve()
dtoMin = listeDAO[0]
dtoMax = listeDAO[1]
dtoCourant = listeDAO[2]

# On ecrit dans le CSV
dao_csv.update(dtoMin, dtoMax, dtoCourant)

