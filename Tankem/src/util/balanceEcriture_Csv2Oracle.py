

import sys
import os.path

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import common.internal.dao.dao_oracle_balance as dao_oracle
import common.internal.dao.nettoyeur as nettoyeur
import common.internal.dao.dao_csv as csv

import Tkinter
import tkMessageBox
import sysconfig
import cx_Oracle
 
# Je vais lire ce qui se trouve dans le fichier csv. La fonction retourne une liste de 3 DTO (min, max et lui avec les donnees)
dao_csv = csv.DAO_Csv()
listeDAO = dao_csv.retrieve()

dtoMin = listeDAO[0]
dtoMax = listeDAO[1]
dto = listeDAO[2]

# Tente de se connecter a la BD Oracle
try:
	dao = dao_oracle_balance.DAO_Oracle_Balance()
	dao.connection_db()
except cx_Oracle.DatabaseError as e:
	message = "Malheureusement, la connexion avec la base de donnee ne s'est pas effectuee"
	tkMessageBox.showinfo("Suivi de votre demande", message)
	exit()

# Si la connexion fonctionne, on passe les donnees dans le nettoyeur 
nettoyeur = nettoyeur.Nettoyeur()

dtoMin = nettoyeur.validerTypeDTO(dtoMin)
dtoMax = nettoyeur.validerTypeDTO(dtoMax)

dto = nettoyeur.validerDTO(dto, dtoMin, dtoMax)

# Creation du message permettant a l'usager de savoir si ca fonctionne ou pas
if(len(nettoyeur.erreur) == 0):
	message = "Votre demande s'est effectuee avec succes"
else:
	message = nettoyeur.erreur
	message.append("\nMalgre les erreurs, nous avons modifie les donnees afin de modifier les donnees erronees")


# On modifie la BD
listeDAO = dao.update(dtoMin, dtoMax, dto)
tkMessageBox.showinfo("Suivi de votre demande", message)


