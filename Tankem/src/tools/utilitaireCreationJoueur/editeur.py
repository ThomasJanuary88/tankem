from __future__ import print_function
import os
from joueur import Joueur
from dao_editeur import DAO_Editeur

class Editeur():

	def __init__(self):
		self.listJoueur = []
		self.__quitter = False
		self.menu = {}
		self.menu [1] = self.__ajoutJoueur
		self.menu [2] = self.__delJoueur
		self.menu [3] = self.__affiche
		self.menu [4] = self.__quit
		self.dao = DAO_Editeur()
		self.recupereListeJoueur()
		os.system('cls') # on windows

		
	def recupereListeJoueur(self):
		result = self.dao.retrieve()
		if result is not None:
			for j in result:
				self.__enregistreJoueur(j[0],j[1],j[2])


	def __quit(self):
		self.__quitter = True
		print("Au revoir!")
		
	def run(self):
		while(not self.__quitter):
			value = ""
			while value not in self.menu:
				self.__show_menu()
				try:
					value = raw_input("\nVeuillez faire un choix dans le menu: ").strip()
					value = int(value)
					os.system('cls') # on windows
				except:
					value = ""
			self.menu.get(value)()
			

	def __ajoutJoueur(self):
		self.__afficheTitre("Ajout de joueur")
		nom = self.__nomJoueur()
		mdp = self.__mdpJoueur()
		id = self.dao.update(None,nom,mdp)
		if (id is None):
			print("Impossible de faire l'ajout!")
		else:
			self.__enregistreJoueur(id,nom,mdp)
			

	def __enregistreJoueur(self,id,nom, mdp):
		self.listJoueur.append(Joueur(nom,mdp,id))

	def __delJoueur(self):
		self.__afficheTitre("Suppression d'un joueur")
		self.__affiche()
		index = self.__choixJoueur()
		if index >= 0 and index < len(self.listJoueur):
			self.dao.delete(self.listJoueur[index].id)
			self.listJoueur.pop(index)
		else:
			print("Index hors de la plage permise!")

	def __nomJoueur(self):
		userName = ""
		while(len(userName)==0):
			userName = raw_input('Entrez votre nom: ').strip()
		return userName

	def __mdpJoueur(self):
		mdp = ""
		while(len(mdp)==0):
			mdp = raw_input('Entrez votre mdp: ').strip()
		return mdp
			
	def __choixJoueur(self):
		value = ""
		while not type(value) == int:
			value=input("Entrer l'index du joueur a efface(negatif pour annuler):")
			return value

	def __affiche(self):
		self.__afficheTitre("Afficher liste des joueurs")
		index =0
		for joueur in self.listJoueur:
			print("Index %s( %s)"%(index,joueur.affichage()))
			index +=1

	def __show_menu(self):
		self.__afficheTitre("menu")
		print("1. Ajouter un joueur")
		print("2. Supprimer un joueur")
		print("3. Afficher la liste des joueurs")
		print("---------------------------")
		print("4. Quitter l'editeur'")

	def __afficheTitre(self, titre):
		n = len(titre)
		print("-"*n)
		print(titre)
		print("-"*n+"\n")