# -*- coding: utf-8 -*-

# voir : http://www.oracle.com/technetwork/articles/dsl/mastering-oracle-python-1391323.html

from __future__ import print_function

from dao_oracle import DAO_Oracle
from collections import namedtuple

import cx_Oracle

class DAO_Editeur(DAO_Oracle):
		
	def retrieve(self):
		try:
			self.connection_db()
			result = self.listeJoueur()
			self.close_connection_db()
			return result
		
		except cx_Oracle.DatabaseError as e:
			print("Impossible de récupérer les données.")
			return None
    
    

	def delete(self, id):
		try:
			self.connection_db()
			result = self.delJoueur(id)
			self.commit()
			return result
		
		except cx_Oracle.DatabaseError as e:
			print("Impossible de supprimer les données.")
			return False

	def update(self,id,nom, mdp):
		try:
			self.connection_db()
			result = self.ajoutJoueur(id,nom, mdp)
			self.close_connection_db()
			return result
		
		except cx_Oracle.DatabaseError as e:
			print("Impossible de mettre à jour les données.")
			return None

	def listeJoueur(self):
		try:
			self.execute_query("SELECT %s,%s,%s FROM %s" %("id","nom_usager","mot_de_passe","Joueur"))
			result = []
			result = self.cursor.fetchall()
			return result

		except Exception as e:
			result = None
			return result
	
	def ajoutJoueur(self,id,nom, mdp):
		try:
			params = [None, 0,0,0,'Je suis un nouveau',0,0,0,0,0,nom, mdp]
			self.cursor.callproc('setJoueur', params)
			self.execute_query("SELECT MAX(id) FROM joueur")
			id = self.cursor.fetchone()
			return id[0]

		except Exception as e:
			id = None
			return id
	
	def delJoueur(self,id):
		try:
			self.execute_query("DELETE FROM joueur WHERE id =%d" %(id))
			return True

		except Exception as e:
			return False