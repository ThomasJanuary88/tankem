# -*- coding: utf-8 -*-

# voir : http://www.oracle.com/technetwork/articles/dsl/mastering-oracle-python-1391323.html

from __future__ import print_function

from dao_abstract import DAO_Abstract
from collections import namedtuple

import cx_Oracle

class DAO_Oracle(DAO_Abstract):

	def __init__(self):
		# self.id = "e0155685"
		# self.password = "Jacob111"
		self.id = "e1506728"
		self.password = "09fc9a96"

	def connection_db(self):
		self.connection = cx_Oracle.connect(self.id, self.password, 'delta/decinfo.edu')
		self.cursor = self.connection.cursor()

	def close_connection_db(self):
		self.cursor.close()
		self.connection.close()

	def create(self):
		raise NotImplementedError("Subclass must implement abscract method.")

	def retrieve(self):
		raise NotImplementedError("Subclass must implement abscract method.")

	def update(self):
		raise NotImplementedError("Subclass must implement abscract method.")

	def delete(self):
		raise NotImplementedError("Subclass must implement abscract method.")
		
	def commit(self):
		try:
			self.connection.commit()
			self.close_connection_db()
		except cx_Oracle.DatabaseError as excep:
			print("Impossible de se commit.")

	def execute_query(self, sql_statement, confirmation = False):
		try:
			self.cursor.execute(sql_statement)
			if (confirmation):
				print(u"Requête terminée avec succès : " + sql_statement)
		except cx_Oracle.DatabaseError as excep:
			error, = excep.args
			if (confirmation):
				print(u"!Erreur d'exécution d'une requête à la base de données!")
				print(" - Error code : " + str(error.code))			# Code de l'erreur pour chercher sur Google
				print(" - Error message : " + str(error.message)) 	# Message du SGBD
				print(" - Error context : " + str(error.context)) 	# Donne la fonction qui a plantée
				