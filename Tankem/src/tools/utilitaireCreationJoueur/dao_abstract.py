# -*- coding: utf-8 -*-

# voir : http://www.oracle.com/technetwork/articles/dsl/mastering-oracle-python-1391323.html

from __future__ import print_function
# from DAO import *

import cx_Oracle

class DAO_Abstract(object):

	def create(self, dtoMin, dtoMax, dto):
		raise NotImplementedError("Subclass must implement abscract method.")

	def retrieve(self):
		raise NotImplementedError("Subclass must implement abscract method.")

	def update(self, dtoMin, dtoMax, dto):
		raise NotImplementedError("Subclass must implement abscract method.")

	def delete(self):
		raise NotImplementedError("Subclass must implement abscract method.")

	def commit(self):
		raise NotImplementedError("Subclass must implement abscract method.")