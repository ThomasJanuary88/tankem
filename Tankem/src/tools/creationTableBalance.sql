SET SERVEROUTPUT ON;

-- *********************************************************************--
--                       SUPPRESSION DES TABLES                         --
-- *********************************************************************--

DROP TABLE unite;
DROP TABLE affichage;


-- *********************************************************************--
--                          CREATION DES TABLES                         --
-- *********************************************************************--

--  TABLE UNITE 

CREATE TABLE unite
(
  id NUMBER GENERATED ALWAYS AS IDENTITY, 
  nom VARCHAR2(150) NOT NULL, 
  min NUMBER(5, 1) NOT NULL, 
  max NUMBER(5, 1) NOT NULL, 
  courant NUMBER(5, 1) NOT NULL, 
  CONSTRAINT pk_unite PRIMARY KEY (id)
);

--  TABLE AFFICHAGE 

CREATE TABLE affichage
(
  id NUMBER GENERATED ALWAYS AS IDENTITY, 
  nom VARCHAR2(150) NOT NULL, 
  contenu_max NUMBER(3) NOT NULL, 
  contenu_courant VARCHAR2(999) NOT NULL, 
  CONSTRAINT pk_affichage PRIMARY KEY (id)
);


-- *********************************************************************--
--                           CREATION DES INDEX                         --
-- *********************************************************************--

CREATE UNIQUE INDEX idx_unite_nom ON unite (lower(nom));
CREATE UNIQUE INDEX idx_affichage_nom ON affichage (lower(nom));

 
-- *********************************************************************--
--                 CREATION DES PROCEDURES D'INSERTION                  --
-- *********************************************************************--

-- Insertion table unite

CREATE OR REPLACE PROCEDURE procedure_unite(	val_nom IN unite.nom%TYPE, 
									val_min IN unite.min%TYPE, 
									val_max IN unite.max%TYPE, 
									val_courant IN unite.courant%TYPE	)
IS 
    nom_double unite.nom%TYPE;
BEGIN 
    BEGIN
        SELECT nom INTO nom_double 
        FROM unite
        WHERE nom = val_nom;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                nom_double := NULL;
    END;
    
    IF (nom_double IS NOT NULL) 
    THEN
        UPDATE unite 
        SET min = val_min, 
            max = val_max, 
            courant = val_courant
        WHERE nom = val_nom; 
    ELSE
        INSERT INTO unite( nom, min, max, courant) 
        VALUES(val_nom, val_min, val_max, val_courant);
    END IF;
	COMMIT;
END;
/


-- Insertion table affichage

CREATE OR REPLACE PROCEDURE procedure_affichage(	val_nom IN affichage.nom%TYPE, 
                                                    val_contenu_max in affichage.contenu_max%TYPE, 
                                                    val_contenu_courant IN affichage.contenu_courant%TYPE)															
IS 
    nom_double affichage.nom%TYPE;
BEGIN
    BEGIN
        SELECT nom INTO nom_double 
        FROM affichage 
        WHERE nom = val_nom;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                nom_double := NULL;
    END;

    IF nom_double IS NOT NULL 
    THEN
        UPDATE affichage 
        SET contenu_max = val_contenu_max, 
            contenu_courant = val_contenu_courant
        WHERE nom = val_nom; 
    ELSE 
        INSERT INTO affichage( nom, contenu_max, contenu_courant) 
        VALUES(val_nom, val_contenu_max, val_contenu_courant);
    END IF;
    
	COMMIT;
END;
/
-- *********************************************************************--
--                        CREATION DES TRIGGERS                         --
-- *********************************************************************--

-- TABLE UNITE

CREATE OR REPLACE TRIGGER valid_insertion_unite
BEFORE INSERT OR UPDATE ON unite
FOR EACH ROW
BEGIN
  IF :new.max < :new.min
    THEN :new.max := :new.min;
    DBMS_OUTPUT.PUT_LINE('Valeur maximale ajustee a la valeur minimale');
  END IF;
  IF :new.courant < :new.min 
    THEN :new.courant := :new.min;
    DBMS_OUTPUT.PUT_LINE('Valeur courante ajustee a la valeur minimale');
  ELSIF :new.courant > :new.max 
    THEN :new.courant := :new.max;
    DBMS_OUTPUT.PUT_LINE('Valeur courante ajustee a la valeur maximale');
  END IF;
END;
/

-- TABLE AFFICHAGE

CREATE OR REPLACE TRIGGER valid_insertion_message
BEFORE INSERT OR UPDATE ON affichage
FOR EACH ROW
BEGIN
    IF LENGTH(:new.contenu_courant) > :new.contenu_max
        THEN :new.contenu_courant := SUBSTR(:new.contenu_courant, 0, :new.contenu_max);
        DBMS_OUTPUT.PUT_LINE('Contenu maximum ajuste avec longueur du message courant');
    END IF;
END;
/


-- *********************************************************************--
--                        INSERTIONS DE DONNEES                         --
-- *********************************************************************--

-- TABLE UNITE

-- Chars

    EXECUTE procedure_unite('vitesseLinChar', '4,0' , '12,0' , '7,0');
    EXECUTE procedure_unite('vitesseRotationChar', '1000,0' , '2000,0' , '1500,0');
    EXECUTE procedure_unite('pointDeVieChar', '100,0' , '2000,0' , '200,0');
    

-- Blocs
    
    EXECUTE procedure_unite('vitesseBloc', '0,2' , '2,0' , '0,8');
    

-- Canon
    EXECUTE procedure_unite('vitesseCanon', '4,0' , '30,0' , '14,0');
    EXECUTE procedure_unite('delaiCanon', '0,2' , '10,0' , '1,2');


-- Mitraillette

    EXECUTE procedure_unite('vitesseMitraillette', '4,0' , '30,0' , '18,0');
    EXECUTE procedure_unite('delaiMitraillette', '0,2' , '10,0' , '0,4');    


-- Grenade

    EXECUTE procedure_unite('vitesseGrenade', '10,0' , '25,0' , '16,0');
    EXECUTE procedure_unite('delaiGrenade', '0,2' , '10,0' , '0,8'); 


-- Shotgun

    EXECUTE procedure_unite('vitesseShotgun', '4,0' , '30,0' , '13,0'); 
    EXECUTE procedure_unite('delaiShotgun', '0,2' , '10,0' , '1,8'); 
    EXECUTE procedure_unite('ouvertureShotgun', '0,1' , '1,5' , '0,4'); 


-- Piege

    EXECUTE procedure_unite('vitessePiege', '0,2' , '4,0' , '1,0'); 
    EXECUTE procedure_unite('delaiPiege', '0,2' , '10,0' , '0,8');


-- Missile

    EXECUTE procedure_unite('vitesseMissile', '20,0' , '40,0' , '30,0');
    EXECUTE procedure_unite('delaiMissile', '0,2' , '10,0' , '3,0');
    

-- Spring

    EXECUTE procedure_unite('vitesseSpring', '6,0' , '20,0' , '10,0');
    EXECUTE procedure_unite('delaiSpring', '0,2' , '10,0' , '0,5');


-- Grosseur de l'explosion

    EXECUTE procedure_unite('rayonExplosion', '1,0' , '30,0' , '8,0');
    
    
-- Duree message d'accueil    

    EXECUTE procedure_unite('msgAccueuilDuree', '1,0' , '10,0' , '3,0');
    
-- Duree compte a rebours
    
    EXECUTE procedure_unite('msgRebourDuree', '0,0' , '10,0' , '3,0');


-- TABLE AFFICHAGE

-- Message d'accueil

    EXECUTE procedure_affichage('strMsgAccueuilContenu', 60, 'Appuyer sur F1 pour de l''aide'); 


-- Signal de debut partie

    EXECUTE procedure_affichage('strMsgDebutContenu', 50, 'Tankem !'); 


-- Message de fin de partie 

    EXECUTE procedure_affichage('strMsgFinContenu', 70, '\n Joueur {0} a gagne !'); 

COMMIT;
