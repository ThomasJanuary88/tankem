<?php
	require_once("action/AjaxTauxVictoireAction.php");

	$action = new AjaxTauxVictoireAction();
	$action->execute();

	echo json_encode($action->result);