<?php
	require_once("action/BilanAction.php");

	$action = new BilanAction();
	$action->execute();

	require_once("partiel/header.php");
	require_once("partiel/navBar.php");
?>   

<link rel="stylesheet" href="css/bilan.css">
<script src="js/bilan.js"></script>
<script src="js/Canvas.js"></script>
<script src="js/Util.js"></script>
<script src="js/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<fieldset>
    <legend><h3>Choix d'une statistique</h3></legend>
    <select name="stat" id="stat">
        <option value="1">Arme préférée</option>
        <option value="2">Efficacité des armes</option>
        <option value="3">Ratio victoire</option>
        <option value="4">Parties jouées</option>
        <option value="5">Autre</option>
    </select>
    <input type="date" name="datePartie" id="datePartie" style = "display:none">
    <select name="pricisionDate" id="precisionDate"  style = "display:none">
        <option value="1">Par jour dans un mois</option>
        <option value="2">Par mois selon l'année</option>
        <option value="3">Selon l'heure dans un mois</option>      
    </select>
    <button type="button" class = "btn btn-info btn-lg" id="validerStat">Soumettre</button>
</fieldset>
<fieldset id = "fieldsetResultat"  style = "display:none">
    <legend><h3>Résultat de la requête</h3></legend>
    <label id= "resultatReqSql"></label>
</fieldset>
<div id = "canvas-container">
    <canvas id="graphique" width="400" height="400"></canvas>
</div>

<?php

	require_once("partiel/footer.php");