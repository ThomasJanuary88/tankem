<?php
	require_once("action/AjaxArmePrefereAction.php");

	$action = new AjaxArmePrefereAction();
	$action->execute();

	echo json_encode($action->result);