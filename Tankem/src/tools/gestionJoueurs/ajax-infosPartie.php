<?php
	require_once("action/AjaxInfosPartieAction.php");

	$action = new AjaxInfosPartieAction();
	$action->execute();

	echo json_encode($action->result);