<?php
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();

	require_once("partiel/header.php");
?>   

<link rel="stylesheet" href="css/index.css">
<script src="js/index.js"></script>
<script src="js/Util.js"></script>
    <aside>
        <form action="index.php" method="post">
            <fieldset>
                <legend><h3>Connexion</h3></legend>
                <div class = "ligne">
                    <input autofocus class = "focus " type="text" name="username" id="nomJoueur" minlength="3" maxlength="20" size="50" 
                    pattern="[a-zA-Z0-9_]{3,20}" required autofocus placeholder="Nom du joueur" />
                    <span class="validity"></span>
                </div>
                <div>
                    <input type="password" name="pwd" id="mdp" placeholder = "Mot de passe">
                </div>
                <button type="submit" id = "soumettre" class="btn btn-info btn-lg">Soumettre</button>
            </fieldset>
        </form>

<?php
	require_once("partiel/footer.php");