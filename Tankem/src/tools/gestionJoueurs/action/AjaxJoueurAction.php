<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/Data.php");
	require_once("action/JoueurDTO.php");
	require_once("action/AnalyseJoueurDTO.php");

	class AjaxJoueurAction extends CommonAction {
		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
            if (isset($_SESSION["username"])){
				$this->result = new JoueurDTO(Data::recupererJoueur($_SESSION["username"]));

				$this->result->message = $_POST['message'];
				$this->result->couleur_r = $_POST['couleur_r'];
				$this->result->couleur_g = $_POST['couleur_g'];
				$this->result->couleur_b = $_POST['couleur_b'];
				$this->result->vie = $_POST['vie'];	
				$this->result->force = $_POST['force'];	
				$this->result->agilite = $_POST['agilite'];	
				$this->result->dexterite = $_POST['dexterite'];	

				$this->result = new AnalyseJoueurDTO($this->result);
				$this->result = $this->result->DTO;
				
				Data::sauvegardeJoueur($this->result);
            }
		}
	}	