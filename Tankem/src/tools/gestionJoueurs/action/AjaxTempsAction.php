<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/Data.php");

	class AjaxTempsAction extends CommonAction {
		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		
		}

		protected function executeAction() {
			$option = $_POST["option"];
            if (isset($_SESSION["id"])){
				if($_POST["date"]){
					switch($option){
						case 1:
							$this->result = Data::partiePourUnMois($_SESSION["id"],$_POST["date"]);
							break;
						case 2:
							$this->result = Data::partiePourUnAn($_SESSION["id"],$_POST["date"]);
							break;
						case 3:
							$this->result = Data::partieHeureMois($_SESSION["id"],$_POST["date"]);
							break;
					}
					
				}
            }
		}
	
	}	