<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/Data.php");
	require_once("action/JoueurDTO.php");
	require_once("action/AnalyseJoueurDTO.php");

	class JoueurAction extends CommonAction {
		
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			$this->result = new JoueurDTO(Data::recupererJoueur($_SESSION["username"]));
			$this->result = new AnalyseJoueurDTO($this->result);
			$this->result = $this->result->DTO;
		}
	}	