<?php
	require_once("action/DAO/connection.php");
	class Data {
		public static function recupererJoueur($nomUsager){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT * FROM joueur WHERE nom_usager = ?");
				$statement->bindParam(1, $nomUsager);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall()[0];
			}
			catch(Exception $e){
				$array[] = array("ID"=>"Connexion impossible!");
				return $array;
			}
		}

		public static function sauvegardeJoueur($DTO) {
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("UPDATE joueur SET message = ?, couleur_r = ?, couleur_g = ?, couleur_b = ?, vie = ?, force = ?, agilite = ?, dexterite = ? WHERE id = ?");
				$statement->bindParam(1, $DTO->message);
				$statement->bindParam(2, $DTO->couleur_r);
				$statement->bindParam(3, $DTO->couleur_g);
				$statement->bindParam(4, $DTO->couleur_b);
				$statement->bindParam(5, $DTO->vie);
				$statement->bindParam(6, $DTO->force);
				$statement->bindParam(7, $DTO->agilite);
				$statement->bindParam(8, $DTO->dexterite);
				$statement->bindParam(9, $_SESSION['id']);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
			}
			catch(Exception $e){
				$array[] = array("ID"=>"Connexion impossible!");
				return $array;
			}
		}

		public static function recupereTir($idJoueur){
			// Retourne la liste des armes avec le nb de tirs
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT arme.nom,sum(nb_tirs) tirs,sum(nb_tirs_succes) 
					tirs_succes FROM JoueurArmePartie j INNER JOIN arme ON j.id_arme = arme.id 
					WHERE id_joueur = ? GROUP BY arme.nom");
				$statement->bindParam(1, $idJoueur);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){;
				return [];
			}
		}
		public static function recupererDistMap($idJoueur){
			// Retourne la liste des maps avec la distance parcourue
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT map.nom, SUM(CASE WHEN p.id_joueur1 = ? THEN p.dist_joueur1 ELSE 
					(CASE WHEN p.id_joueur2 = ? THEN p.dist_joueur2 ELSE 0 END) END) distance
					FROM partie p INNER JOIN map
					ON map.id = p.id_map
					GROUP BY map.nom");
				$statement->bindParam(1, $idJoueur);
				$statement->bindParam(2, $idJoueur);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){;
				return [];
			}
		}
		public static function recupereTaux($idJoueur){
			// Retourne la liste des armes avec % de succes
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT arme.nom,ROUND((sum(nb_tirs_succes)/nullif(sum(nb_tirs),0) *100),2
                )pourcentage FROM JoueurArmePartie j INNER JOIN arme ON j.id_arme = arme.id 
					WHERE id_joueur = ? GROUP BY arme.nom");
				$statement->bindParam(1, $idJoueur);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){;
				return [];
			}
		}
		public static function recupereGagnant($idJoueur){
			// Retourne le gagnant de toutes les parties que le joueur a participe
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare(" SELECT sum(case when id_gagnant = ? then 0 else 1 end) Defaite,sum(case when id_gagnant = ? then 1 else 0 end) 
					Victoire FROM partie WHERE id_joueur1 = ? OR id_joueur2 = ?");
				$statement->bindParam(1, $idJoueur);
				$statement->bindParam(2, $idJoueur);
				$statement->bindParam(3, $idJoueur);
				$statement->bindParam(4, $idJoueur);				
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){;
				return [];
			}
		}


		public static function recupereDatePartie($idJoueur){
			// Retourne la date de toutes les parties que le joueur a participe
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("	SELECT to_char(date_debut,'YYYY/MM/DD HH24:MI:SS') dtHeure , j1.nom_usager joueur1, j2.nom_usager joueur2, partie.id
												   	FROM partie 
												   	INNER JOIN joueur j1 
												   		ON j1.id = partie.id_joueur1 
													INNER JOIN joueur j2 
														ON j2.id = partie.id_joueur2
													WHERE id_joueur1 = ? OR id_joueur2 = ? 
													ORDER BY dtHeure DESC");
				$statement->bindParam(1, $idJoueur);
				$statement->bindParam(2, $idJoueur);				
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){;
				return [];
			}
		}

		public static function recupereInfosPartie($id){
			// Retourne les informations d'une partie
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("	SELECT p.*, p.date_fin - p.date_debut duree, j1.nom_usager nom_j1, j2.nom_usager nom_j2, m.nom nom_carte
													FROM partie p
													INNER JOIN joueur j1
														ON j1.id = p.id_joueur1
													INNER JOIN joueur j2
														ON j2.id = p.id_joueur2
													INNER JOIN map m
														ON m.id = p.id_map
													WHERE p.id = ?");
				$statement->bindParam(1, $id);				
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){;
				return [];
			}
		}

		public static function recupereInfosArmes($idJoueur, $idPartie){
			// Retourne les informations d'utilisation des armes d'un joueur
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("	SELECT a.nom, jap.* 
													FROM joueurarmepartie jap 
													INNER JOIN arme a 
														ON a.id = jap.id_arme 
													WHERE id_joueur = ? AND id_partie = ?");
				$statement->bindParam(1, $idJoueur);	
				$statement->bindParam(2, $idPartie);			
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){;
				return [];
			}
		}
		public static function partiePourUnMois($id,$date){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT COUNT(to_char(DATE_DEBUT, 'YYYY-MM-DD')) nombre, extract(day FROM date_debut) dt
													from PARTIE
													WHERE extract(month FROM DATE_DEBUT) = extract(month FROM  to_date(?,'YYYY-MM-DD'))
													AND extract(year FROM DATE_DEBUT) = extract(year FROM  to_date(?,'YYYY-MM-DD'))													
													AND (id_joueur1 = ? OR id_joueur2 = ?)
													group by extract(day FROM date_debut)
													ORDER BY dt");
				$statement->bindParam(1, $date);
				$statement->bindParam(2, $date);
				$statement->bindParam(3, $id);
				$statement->bindParam(4, $id);			
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){;
				return ["blabla"];
			}
		}
		public static function partiePourUnAn($id,$date){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT COUNT(to_char(DATE_DEBUT, 'YYYY-MM')) nombre, extract(month FROM date_debut) dt
													FROM PARTIE
													WHERE extract(year from DATE_DEBUT)= extract(year FROM  to_date(?,'YYYY-MM-DD'))
													AND (id_joueur1 = ? OR id_joueur2 = ?)
													group by extract(month FROM date_debut)
													ORDER BY dt");
				$statement->bindParam(1, $date);
				$statement->bindParam(2, $id);
				$statement->bindParam(3, $id);			
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){;
				return ["blabla"];
			}
		}
		public static function partieHeureMois($id,$date){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT COUNT(to_char(DATE_DEBUT, 'hh24')) nombre, extract(hour from cast (date_debut as timestamp)) dt
													FROM PARTIE
													WHERE extract(month from DATE_DEBUT) = extract(month FROM  to_date(?,'YYYY-MM-DD'))
													AND extract(year from DATE_DEBUT) = extract(year FROM  to_date(?,'YYYY-MM-DD'))
													AND (id_joueur1 = ? OR id_joueur2 = ?)
													group by extract(hour from cast (date_debut as timestamp))
													ORDER BY dt");
				$statement->bindParam(1, $date);
				$statement->bindParam(2, $date);				
				$statement->bindParam(3, $id);
				$statement->bindParam(4, $id);			
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){;
				return ["blabla"];
			}
		}
	}
