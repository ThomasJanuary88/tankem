<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/UserDAO.php");

	class IndexAction extends CommonAction {
		public $wrongLogin = false;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			$this->wrongLogin = false;

			if (isset($_POST["username"])) {
				$user = UserDAO::authenticate($_POST["username"], $_POST["pwd"]);
				if (!empty($user)) {
					$_SESSION["visibility"] = CommonAction::$VISIBILITY_MEMBER;

					$_SESSION["username"] = $_POST["username"];

					$_SESSION["id"] = $user["ID"];

					// $DTO = new JoueurDTO($user);
					// var_dump($DTO->getDTO());

					header("location:joueur.php");
					exit;
				}
				else {
					$this->wrongLogin = true;
				}
			}
		}
	}	