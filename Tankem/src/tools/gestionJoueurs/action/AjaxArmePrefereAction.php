<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/Data.php");

	class AjaxArmePrefereAction extends CommonAction {
		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		
		}

		protected function executeAction() {
            if (isset($_SESSION["id"])){
                $this->result = Data::recupereTir($_SESSION["id"]);
            }
		}
	
	}	