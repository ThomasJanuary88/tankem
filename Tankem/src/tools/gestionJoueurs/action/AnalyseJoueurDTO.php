<?php
	require_once("action/DAO/Data.php");
	class AnalyseJoueurDTO {
		public $DTO = null;
		const maxCool = 20;

		public function __construct($DTO) {
			$DTO->niveau = $this->setLevel($DTO->experience);
			//validation cool point
			$DTO->vie = $this->verifCoolPoint($DTO->vie);
			$DTO->force = $this->verifCoolPoint($DTO->force);
			$DTO->dexterite = $this->verifCoolPoint($DTO->dexterite);
			$DTO->agilite = $this->verifCoolPoint($DTO->agilite);
			
			//set cool point restants
			$DTO->coolPoints = $this->setCoolPoints($DTO->niveau, $DTO->vie,$DTO->force,$DTO->dexterite,$DTO->agilite);

			$date = Data::recupereDatePartie($DTO->id);
			if($date) {
				$DTO->date_partie = $date[0]['DTHEURE'];
			}
			else{
				$DTO->date_partie = 'Aucune partie jouée';
			}

			//uniformisation couleur
			if(!ctype_digit($DTO->couleur_r)) {
				$DTO->couleur_r = $this->hexaToInt($DTO->couleur_r);
			}
			else{
				$DTO->couleur_r = $this->intToHexa($DTO->couleur_r);
			}

			if(!ctype_digit($DTO->couleur_g)) {
				$DTO->couleur_g = $this->hexaToInt($DTO->couleur_g);
			}
			else{
				$DTO->couleur_g = $this->intToHexa($DTO->couleur_g);
			}

			if(!ctype_digit($DTO->couleur_b)) {
				$DTO->couleur_b = $this->hexaToInt($DTO->couleur_b);
			}
			else{
				$DTO->couleur_b = $this->intToHexa($DTO->couleur_b);
			}

			$this->DTO = $DTO;
		}

		private function verifCoolPoint($value){
			//validation du type
			if(!is_numeric($value)){
				$value = 0;
			}
			else{
				$value = intval($value);
			}
			//validation de la valeur
			return $this->clamp($value,self::maxCool);

		}
		private function clamp($value,$max,$min = 0){
			if($max < $min){
				$temp = $min;
				$min = $max;
				$min = $temp;
			}
			if($value < $min){
				return $min;
			}
			else if ($value > $max){
				return $max;
			}
			return $value;
		}

		public function intToHexa($value) {
			$value = dechex ($value);
			if(strlen($value) == 1) {
				$value = $value."0";
			}
			return $value;
		}

		public function hexaToInt($value) {
			$value = hexdec($value);
			return $value;
		}

		public function setLevel($experience) {
			$exp = $experience/50 - 1;
			if ($exp <= 0){
				return 0;
			}
			$level = floor(sqrt($exp) - 1);
			return $level;
		}

		public function setCoolPoints($level, &$vie, &$force, &$dexterite, &$agilite) {
			$coolPoints = min(80,(5 * $level));
			if($coolPoints  - ($vie + $force + $dexterite + $agilite) < 0) { 
				// Trop de cool points attribués : Reset des points, le joueur doit les redistribuer au complet
				$vie = 0;
				$force = 0;
				$dexterite = 0;
				$agilite = 0;
			}
			return $coolPoints;
		}
	}