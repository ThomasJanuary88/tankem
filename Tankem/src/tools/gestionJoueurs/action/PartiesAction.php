<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/Data.php");

	class PartiesAction extends CommonAction {
		public $listeParties = null;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			$this->listeParties = Data::recupereDatePartie($_SESSION["id"]);
		}
	}	