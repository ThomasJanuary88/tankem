<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/Data.php");

	class AjaxInfosArmesAction extends CommonAction {
		public $result;


		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			$idPartie = $_POST['idPartie'];
			$idJoueur1 = $_POST['idJoueur1'];
			$idJoueur2 = $_POST['idJoueur2'];
			$info1 = Data::recupereInfosArmes($idJoueur1, $idPartie);
			$info2 = Data::recupereInfosArmes($idJoueur2, $idPartie);
			$this->result = [ $info1, $info2];
			// var_dump("idpartie: ".$idPartie);
			// var_dump("j1: ".$idJoueur1);
			// var_dump("j2: ".$idJoueur2);
		}
	}	