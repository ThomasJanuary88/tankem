<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/Data.php");

	class AjaxInfosPartieAction extends CommonAction {
		public $result;


		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			$idPartie = $_POST['id'];
			$this->result = Data::recupereInfosPartie($idPartie);
		}
	}	