<?php
	class JoueurDTO {
		public $id = null;
		public $message = null;
		public $experience = null;
		public $niveau = null;
		public $coolPoints = null;
		public $vie = null;
		public $force = null;
		public $dexterite = null;
		public $agilite = null;
		public $couleur_r = null;
		public $couleur_g = null;
		public $couleur_b = null;
		public $date_partie = null;

		public function __construct($joueur) {
			$this->id = $joueur['ID'];
			$this->message = $joueur['MESSAGE'];
			$this->experience = $joueur['EXPERIENCE'];
			$this->vie =  $joueur['VIE'];
			$this->force = $joueur['FORCE'];
			$this->dexterite = $joueur['DEXTERITE'];
			$this->agilite = $joueur['AGILITE'];
			$this->couleur_r = $joueur['COULEUR_R'];
			$this->couleur_g = $joueur['COULEUR_G'];
			$this->couleur_b = $joueur['COULEUR_B'];
		}
	}