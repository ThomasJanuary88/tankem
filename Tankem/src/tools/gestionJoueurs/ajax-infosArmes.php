<?php
	require_once("action/AjaxInfosArmesAction.php");

	$action = new AjaxInfosArmesAction();
	$action->execute();

	echo json_encode($action->result);