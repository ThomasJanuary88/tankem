<?php
	require_once("action/AjaxJoueurAction.php");

	$action = new AjaxJoueurAction();
	$action->execute();

	echo json_encode($action->result);