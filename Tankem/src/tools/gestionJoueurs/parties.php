<?php
	require_once("action/PartiesAction.php");

	$action = new PartiesAction();
	$action->execute();

	require_once("partiel/header.php");
	require_once("partiel/navBar.php");
?>   

<link rel="stylesheet" href="css/parties.css">
<link rel="stylesheet" href="css/style.css">
<script src="js/Util.js"></script>
<script src="js/parties.js"></script>
<fieldset>
    <legend><h3>Liste des parties</h3></legend>
	<label for='listeParties'>Choisir une partie à visualiser : </label>
	<select name="listeParties" id="listeParties">
		<?php
			for($i = 0;$i<sizeof($action->listeParties);$i++){
			?>
			document.write("<option value=<?= $action->listeParties[$i]['ID'] ?>><?= $action->listeParties[$i]['JOUEUR1'] ?> VS <?= $action->listeParties[$i]['JOUEUR2'] ?> <?= $action->listeParties[$i]['DTHEURE'] ?></option>")
			<?php
			}
		?>
	</select>
</fieldset>

<fieldset id='hidden' style='display:none'>
    <legend><h3>Statistiques de la partie</h3></legend>
	<div class='ligne'>
		<label for='datePartie'>Date de la partie :</label>
		<input disabled type='text' id='datePartie' size="50">
	</div>
	<div class='ligne'>
		<label for='dureePartie'>Durée (secondes) :</label>
		<input disabled type='text' id='dureePartie' size="50">
	</div>
	<div class='ligne'>
		<label for='nomCarte'>Nom de la carte :</label>
		<input disabled type='text' id='nomCarte' size="50">
	</div>
	<div class='ligne'>
		<label for='nomGagnant'>Nom du gagnant :</label>
		<input disabled type='text' id='nomGagnant' size="50">
	</div>
	<div id="grid-joueurs">
		<fieldset class="fs-joueur" id="fs-joueur1">
			<legend><h3>Statistiques du joueur 2</h3></legend>
			<div class='ligne'>
				<label for='joueur1'>Nom :</label>
				<input disabled type='text' id='joueur1' size="50">
			</div>
			<div class='ligne'>
				<label for='dist1'>Distance parcourue :</label>
				<input disabled type='text' id='dist1' size="50">
			</div>
			<table style="width:100%" id='armes-j1'>

			</table>
		</fieldset>
		<fieldset class="fs-joueur" id="fs-joueur2">
			<legend><h3>Statistiques du joueur 2</h3></legend>
			<div class='ligne'>
				<label for='joueur2'>Nom :</label>
				<input disabled type='text' id='joueur2' size="50">
			</div>
			<div class='ligne'>
				<label for='dist2'>Distance parcourue :</label>
				<input disabled type='text' id='dist2' size="50">
			</div>
			<table style="width:100%" id='armes-j2'>
				<tr>
					<th>Arme</th>
					<th>Tirs (total)</th> 
					<th>Tirs (succès)</th>
					<th>Coups reçus</th>
				</tr>
			</table>
		</fieldset>
	</div>
</fieldset>

<script type="template" id = "tableStat">
	<table style="width:100%" id='armes-j1'>
				<tr>
					<th>Arme</th>
					<th>Tirs (total)</th> 
					<th>Tirs (succès)</th>
					<th>Coups reçus</th>
				</tr>
			</table>
</script>

<?php

	require_once("partiel/footer.php");