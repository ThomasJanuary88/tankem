<?php
	require_once("action/AjaxDistMapAction.php");

	$action = new AjaxDistMapAction();
	$action->execute();

	echo json_encode($action->result);