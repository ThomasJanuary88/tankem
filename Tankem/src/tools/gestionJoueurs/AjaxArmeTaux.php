<?php
	require_once("action/AjaxArmeTauxAction.php");

	$action = new AjaxArmeTauxAction();
	$action->execute();

	echo json_encode($action->result);