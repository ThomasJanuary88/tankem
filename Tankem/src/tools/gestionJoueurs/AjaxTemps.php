<?php
	require_once("action/AjaxTempsAction.php");

	$action = new AjaxTempsAction();
	$action->execute();

	echo json_encode($action->result);