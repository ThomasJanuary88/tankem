class Util{
    constructor(){}

        static getInt(node){
            //vu que champ est number renvoit string vide si texte est saisi
            if(node.value == ""){
                ajoutErreur("Vous devez inscrire un chiffre!");
                return 0;
            }
            return parseInt(node.value);
        }

        static clampChiffre(valeur, min, max){
            if(valeur < min){
                return min;
            }
            else if(valeur > max){
                return max;
            }
            return Math.trunc(valeur);
        }
        static clampFloat(valeur, min, max){
            if(valeur < min){
                return min;
            }
            else if(valeur > max){
                return max;
            }
            return valeur;
        }
        static getFloat(node){
            if(node.value == ""){
                ajoutErreur("Vous devez inscrire un chiffre!");
                return 0;
            }
            return parseFloat(node.value);
        }
        static getIndexMax(liste){
            let longueur = liste.length;
            if (longueur == 0){
                return -1;
            }
            let maxIndex = [0];
            for (let i=1;i<longueur;i++){
                console.log(liste[i] + "" +liste[maxIndex[0]])
                if(parseInt(liste[i]) > parseInt(liste[maxIndex[0]])){
                    console.log("true")
                    maxIndex = [];
                    maxIndex = [i];
                }
                else if(parseInt(liste[i]) == parseInt(liste[maxIndex[0]])){
                    maxIndex.push(i);
                }
            }
            return maxIndex;
        }

        static setUpBase(){
            //récupération du nom de la page
            let str = window.location.href;
            let debut = str.lastIndexOf("/")+1;
            let fin = str.lastIndexOf("?");
            let nomPage = "";
            if(fin == -1){
                nomPage = str.substr(debut);
            }
            else{
                nomPage = str.substr(debut, fin-debut);
            }
            nomPage = nomPage.toLowerCase();
            // setattribute active au lien de la page en cour
            ($('a[href="'+nomPage+'"]')[0]).setAttribute("class","active");
        }
    }