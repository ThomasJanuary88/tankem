let myChart = null;

window.onload = ()=>{
    Util.setUpBase();
    document.getElementById("validerStat").onclick = ()=>{
        afficherStat();
    }
    document.getElementById("stat").onchange = ()=>{
        if($(this+"option:selected").val() == 4){
            $("#datePartie").fadeIn("slow","linear");
            $("#precisionDate").fadeIn("slow","linear");            
        }
        else{
            $("#datePartie").fadeOut("slow","linear");
            $("#precisionDate").fadeOut("slow","linear");            
        }
    }
    document.getElementById("datePartie").setAttribute("max","9999-12-31");
    afficherStat();
}

function setResultat(titre = "", texte = []){
    let node = document.getElementById("resultatReqSql");
    let resultat = "";
    texte.forEach(m =>{
        if(resultat !=""){
            resultat += ", "
        }
        resultat += m;
    })
    resultat = titre + resultat;
    node.innerHTML = resultat;
    if(resultat == ""){
        $("#fieldsetResultat").slideUp("slow");
        // document.getElementById("fieldsetResultat").style.visibility = "hidden";
    }else{
        $("#fieldsetResultat").slideDown("slow");
        // document.getElementById("fieldsetResultat").style.visibility = "visible";
    }
}
function setOptions(type, title){
    let options = [];
    if (type =="bar"){
        options = {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
    }

    options["animation"] = {
        animationEasing: "easeOutBack",
        scaleSteps:60,
        maintainAspectRatio: true,
    };
    options["title"] = {
        display: true,
        text: title
    };
    options["legend"] = {
        display:false
    };
    return options;
}
function setBackgroundColor(type){
    let background = "";

    if(type == "line"){
        background = 'rgba(255, 99, 132, 0.2)';
    }
    else{
        background = [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
        ]
    }

    return background;
}

function setBorderColor(type){
    let border = "";

    if(type == "line"){
        border = 'rgba(255,99,132,1)';
    }
    else{
        border = [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
        ]
    }

    return border;
}

function setCanvas(listLabels, listData, typeGraph = "pie", title = ""){
    let listOptions = setOptions(typeGraph, title);
    let listBackgroundColor = setBackgroundColor(typeGraph);
    let listBorderColor = setBorderColor(typeGraph);
    let ctx = document.getElementById("graphique");
    if(myChart){
        myChart.destroy();
    }
    myChart = new Chart(ctx, {
    type: typeGraph,
    data: {
        labels: listLabels,
        datasets: [{
            label: title,
            data: listData,
            backgroundColor: listBackgroundColor,
            borderColor: listBorderColor,
            borderWidth: 1
        }]
    },
    options: listOptions
    });
}
function afficherStat(){
    setResultat();
    let choix = $("#stat").val();
    switch(parseInt(choix)){
        case 1:
            ajaxArmePrefere();
            break;
        case 2:
            ajaxArmeEfficace();
            break;
        case 3:
            ajaxRatio();
            break;
        case 4:
            ajaxTemps();
            break;
        case 5:
            ajaxAutre();
            break;
    }
}
function ajaxArmePrefere(){
    $.ajax({
        type : "POST",
        url : "AjaxArmePrefere.php",
        data : {}
    })
    .done(response => { 
        response = JSON.parse(response);
        let label = [];
        let data = [];
        response.forEach(arme => {
            label.push(arme["NOM"]);
            data.push(arme["TIRS"]);
        });

        getResultat("L'arme préférée du joueur est: ",label,data);
        setCanvas(label, data,"bar","Arme préférée")
    })
}
function getResultat(titre,label = [],data = []){
    let max = Util.getIndexMax(data);
    let resultat = [];
    if(max !=-1){
        max.forEach(index =>{
            resultat.push(label[index]);
        });
    }
    
    setResultat(titre,resultat);
}
function ajaxArmeEfficace(){
    $.ajax({
        type : "POST",
        url : "AjaxArmeTaux.php",
        data : {}
    })
    .done(response => { 
        response = JSON.parse(response);
        let label = [];
        let data = [];
        response.forEach(arme => {
            label.push(arme["NOM"]);
            if(arme["POURCENTAGE"]){
                data.push(arme["POURCENTAGE"].replace(",","."));
            }
            else{
                data.push(0);
            }
        });
        getResultat("L'arme la plus efficace du joueur est: ",label,data);
        setCanvas(label, data,"bar","Taux de réussite")
    })

}
function ajaxRatio(){
    $.ajax({
        type : "POST",
        url : "AjaxTauxVictoire.php",
        data : {}
    })
    .done(response => { 
        response = JSON.parse(response);
        let label = ["Défaite", "Victoire"];
        let victoire = response[0]["VICTOIRE"];
        let defaite = response[0]["DEFAITE"];
        let data = [defaite,victoire];
        let partie = parseInt(defaite)+parseInt(victoire);
        let titre = "Nb de partie jouées: "+ partie;
        setResultat(titre);
        setCanvas(label, data,"doughnut",titre);
    })
}
// function ajaxTemps(){
//     $.ajax({
//         type : "POST",
//         url : "AjaxTemps.php",
//         data : {}
//     })
//     .done(response => { 
//         response = JSON.parse(response);
//         let label = [];
//         let data = [];
//         let test = 0;
//         response.forEach(partie => {
//             let optionX = {year: 'numeric', month: '2-digit', day: '2-digit' };
//             let date = new Date(partie["DTHEURE"])
//             date = date.toLocaleDateString("fr-CA",optionX);

//             let temps = partie["DTHEURE"].substring(partie["DTHEURE"].indexOf(" ") + 1);
//             heure = temps.substring(0,2)
//             minute = temps.substring(3,5)
//             let valY = parseInt(heure) + parseFloat(minute/60);
//             label.push(date);
//             data.push({
//                 x:test,
//                 y:valY
//             });
//             test +=1;
//         });
//         setCanvas(label, data,"line","Partie jouée dans le temps")
//     })
// }
function ajaxTemps(){
    let option = $("#precisionDate option:selected").val()
    let titre = setTitre(option);
    console.log(titre)
    let date = $("#datePartie").val();
    if(date){
        $.ajax({
            type : "POST",
            url : "AjaxTemps.php",
            data : {
                "option":option,
                "date":date
            }
        })
        .done(response => { 
            response = JSON.parse(response);
            console.log(response)
            let label = [];
            let data = [];
            response.forEach(partie =>{
                label.push(partie["DT"]);
                data.push(partie["NOMBRE"]);
            })
            setCanvas(label, data,"bar", titre)
        })
    }
}
function setTitre(option){
    console.log(option)
    switch(parseInt(option)){
        case 1:
            return "Partie jouée selon le jour dans un mois";
            break;
        case 2:
            return "Partie jouée selon le mois selon l'année";
            break;
        case 3:
            return "Partie jouée selon l'heure selon un mois"
            break;
    }
}
function ajaxAutre(){
    $.ajax({
        type : "POST",
        url : "AjaxDistanceMap.php",
        data : {}
    })
    .done(response => { 
        response = JSON.parse(response);
        let label = [];
        let data = [];
        response.forEach(map => {
            label.push(map["NOM"]);
            data.push(map["DISTANCE"]);
        });
        getResultat("La carte la plus parcourure: ",label,data);
        setCanvas(label, data,"bar","Distance selon la carte.")
    })
}