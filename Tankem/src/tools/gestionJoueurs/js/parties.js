window.onload = () => {
	Util.setUpBase();
	getInfosPartie($("#listeParties").val());

	document.querySelector('#listeParties').onchange = ()=>{
		getInfosPartie($("#listeParties").val());
	}
}

function getInfosPartie(id) {
	$.ajax({
		type : "POST",
		url : "ajax-infosPartie.php",
		data : {
			id: id
		}
	})
	.done((response) => {
		response = JSON.parse(response);
		if(response.length > 0) {
			document.getElementById('hidden').style.display='block';
			document.getElementById('datePartie').value = response[0]['DATE_DEBUT'];
			let duree=  '0' + response[0]['DUREE'].replace(',', '.');
			document.getElementById('dureePartie').value = Math.floor(parseFloat(duree) * 24 * 3600);
	
			document.getElementById('nomCarte').value = response[0]['NOM_CARTE'];
			if(response[0]['ID_GAGNANT'] == response[0]['ID_JOUEUR1']) {
				document.getElementById('nomGagnant').value = response[0]['NOM_J1'];
			}
			else{
				document.getElementById('nomGagnant').value = response[0]['NOM_J2'];
			}
	
			document.getElementById('joueur1').value = response[0]['NOM_J1'];
			document.getElementById('dist1').value = response[0]['DIST_JOUEUR1'];
	
			document.getElementById('joueur2').value = response[0]['NOM_J2'];
			document.getElementById('dist2').value = response[0]['DIST_JOUEUR2'];
	
			getInfosArmesJoueur(response[0]['ID_JOUEUR1'], response[0]['ID_JOUEUR2'], response[0]['ID']);
		}
		else{
			document.getElementById('hidden').style.display='none';
		}
	})
}

function getInfosArmesJoueur(idJoueur1, idJoueur2, idPartie) {
	console.log(idJoueur1, idJoueur2, idPartie)
	$.ajax({
		type : "POST",
		url : "ajax-infosArmes.php",
		data : {
			idJoueur1: idJoueur1,
			idJoueur2: idJoueur2,
			idPartie: idPartie
		}
	})
	.done((response) => {
		response = JSON.parse(response);
		console.log(response)
		let table = null;
		$('#armes-j1').empty();
		$('#armes-j2').empty();
		for(let i = 0; i < response.length; i++) {

			if(i == 0) {
				table = document.getElementById('armes-j1');
			}
			else{
				table = document.getElementById('armes-j2');
			}

			let row = document.createElement('tr');
			let header1 = document.createElement('th');
			header1.innerHTML = "Arme";
			row.appendChild(header1);

			let header2 = document.createElement('th');
			header2.innerHTML = "Tirs (total)";
			row.appendChild(header2);

			let header3 = document.createElement('th');
			header3.innerHTML = "Tirs (succès)";
			row.appendChild(header3);

			let header4 = document.createElement('th');
			header4.innerHTML = "Coups reçus";
			row.appendChild(header4);

			table.appendChild(row);

			for(let j = 0; j < response[i].length; j++) {
				let row = document.createElement('tr');

				let c1 = document.createElement('td');
				c1.innerHTML = response[i][j]['NOM'];
				row.appendChild(c1);
	
				let c2 = document.createElement('td');
				c2.innerHTML = response[i][j]['NB_TIRS'];
				c2.setAttribute('class', 'center');
				row.appendChild(c2);
	
				let c3 = document.createElement('td');
				c3.innerHTML = response[i][j]['NB_TIRS_SUCCES'];
				c3.setAttribute('class', 'center');
				row.appendChild(c3);

				let c4 = document.createElement('td');
				if(i === 0) {
					coupsRecus = response[i+1][j]['NB_TIRS_SUCCES'];
				}
				else{
					coupsRecus = response[i-1][j]['NB_TIRS_SUCCES'];
				}
				c4.innerHTML = coupsRecus;
				c4.setAttribute('class', 'center');
				row.appendChild(c4);

				for(let i = 1; i< row.childNodes.length; i++) {
					if(row.childNodes[i].innerHTML != '0') {
						row.childNodes[i].setAttribute('class', 'bold');
					}
				}
				table.appendChild(row);
			}
		}
	})
}

