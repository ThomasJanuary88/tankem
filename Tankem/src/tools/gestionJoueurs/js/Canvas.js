class Canvas{
    constructor(){
        this.labels = ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"]
        this.data = [12, 19, 3, 5, 2, 3, 20]
        this.title = "Votes"
        this.type = "bar"
    }
    setTitle(title){
        this.title = title;
    }
    setType(type){
        this.type = type;
    }
    setLabels(labels){
        this.labels = labels;
    }
    setData(data){
        this.data = data;
    }
}