class Map{
	constructor(id)
	{
        //la grille est toujours de la grosseur maximale
        //on affiche et enregistre seulement la partie visible
        this.id = id;
        this.quadrille = true;
        this.grille = [];
        this.largeur;
        this.hauteur;
        this.tempsMin;
        this.tempsMax;
        this.time = 0;
        this.mobile = false;
        this.select = {"x":0,"y":0};
        for (let i = 0; i < maxDim; i++) {
            this.grille.push(["","","","","","","","","","","",""]);
        }
        this.validatationValeurCarte();
  
	}
	tick(){
    //compteur pour animation bloque qui bouge
       if(this.time <120){
            this.time++;
        }
        else if(this.time<240){
            this.time++;
            this.mobile = true;
        }
        else{
            this.time = 0;
            this.mobile = false;
        }
       for(let x = 0; x< this.largeur; x++){
           for(let y = 0; y< this.hauteur; y++){
               if(this.grille[x][y] != ""){
                   let img = new Image();
                   img.src = dicImage[this.grille[x][y]];
                    ctx.drawImage(img, x*tuile, y*tuile, tuile, tuile);
                   if(((this.grille[x][y] == "mobile" || this.grille[x][y] == "mobileArbre") && this.mobile) || 
                    ((this.grille[x][y] == "inverse"|| this.grille[x][y] == "inverseArbre") && !this.mobile)){
                       this.dessineCarre(10,"#877670",x,y);
                   }
               }
           }
       }
       if(this.quadrille){
            this.createLine();
       }
       this.dessineCarre(1,"#FF0000",this.select["x"],this.select["y"]);
    }
    modifGrille(objet, x, y){
        if(objet !=""){
            let caseX = x;
            let caseY = y;
            if(this.grille[caseX][caseY] == "mur"  &&  objet == "arbre" || this.grille[caseX][caseY] == "arbre" && objet == "mur"){
                this.grille[caseX][caseY] = "murArbre";
            }
            else if(this.grille[caseX][caseY] == "mobile"  &&  objet == "arbre" || this.grille[caseX][caseY] == "arbre" && objet == "mobile"){
                this.grille[caseX][caseY] = "mobileArbre";
            }
            else if(this.grille[caseX][caseY] == "inverse" && objet == "arbre" || this.grille[caseX][caseY] == "arbre" && objet == "inverse"){
                this.grille[caseX][caseY] = "inverseArbre";
            }
            else if(objet == "gazon"){
                this.grille[caseX][caseY] = "";
            }
            else{
                this.grille[caseX][caseY] = objet;
            }
        }
    }
    static getCase(valeur){
        return Math.trunc(valeur/tuile);
    }
    dessineCarre(largeur, couleur, x, y){
        ctx.beginPath();
        ctx.lineWidth = largeur;
        ctx.strokeStyle= couleur;
        ctx.strokeRect(x*tuile,y*tuile,tuile,tuile);
        ctx.closePath();
    }
    
    setSelect(x,y){
        this.select["x"] += x;
        this.select["y"] += y;

        this.select["x"] = this.loop(this.select["x"], 0, this.largeur);
        this.select["y"] = this.loop(this.select["y"], 0, this.hauteur);
        curseur.x = this.select["x"] * tuile+tuile/2;
        curseur.y = this.select["y"] * tuile+tuile/2;
        curseur.controle = false;
    }
    loop(value, min, max){
        if(value < min){
            return max-1;
        }
        else if(value > max-1){
            return min;
        }
        return value;
    }
    setQuadrille(check){
        this.quadrille = check;
    }
    createLine(){
        ctx.lineWidth = 1;
        for(let i=1; i < this.largeur; i++)
        {
            ctx.beginPath();
            ctx.strokeStyle="#000000";
            ctx.moveTo(i*tuile,0);
            ctx.lineTo(i*tuile,canvas.height);
            ctx.stroke();
        }
        for(let i=1; i < this.hauteur; i++)
        {
            ctx.beginPath();
            ctx.moveTo(0, i*tuile);
            ctx.lineTo(canvas.width, i*tuile);
            ctx.stroke();
        }
    
    }
    sauvegardeMap(){
        // création d'une liste de 3 éléments x, y, objet
        this.validatationValeurCarte();
        let liste = [];
        for(let x = 0; x< this.largeur; x++){
            for(let y = 0; y< this.hauteur; y++){
                if(this.grille[x][y] != ""){
                    liste.push ([x , y, this.grille[x][y]]);
                }
            }
        }
        return liste;
    }
    validatationValeurCarte(){
        // sauvegarde des données de la carte
        let tempsMin = Util.getFloat(document.getElementById("min"));
        let tempsMax = Util.getFloat(document.getElementById("max"));

        if (tempsMin > tempsMax){
            ajoutErreur("Le temps min et max ont été échangé!");
            let temp = tempsMax;
            tempsMax = tempsMin;
            tempsMin = temp;
        }
        this.tempsMin = Util.clampFloat(tempsMin, 0, 999);
        this.tempsMax = Util.clampFloat(tempsMax, 0, 999);

        document.getElementById("min").value = this.tempsMin;
        document.getElementById("max").value= this.tempsMax;
        // validation largeur
        let tLargeur = Util.getInt(document.getElementById("x"));
        let tHauteur =  Util.getInt(document.getElementById("y"));
        this.largeur = Util.clampChiffre(tLargeur, minDim, maxDim);
        this.hauteur = Util.clampChiffre(tHauteur, minDim, maxDim);
        this.redimCanvas(this.hauteur, this.largeur);
    }
    redimCanvas(hauteur, largeur){
        // va être égal au nombre de tuile * grosseur
        canvas.width = largeur * tuile;
        canvas.height = hauteur * tuile;
        document.getElementById("x").value = largeur;
        document.getElementById("y").value = hauteur;
    }

   

}