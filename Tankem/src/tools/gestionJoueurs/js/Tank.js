class Tank{
	constructor(nom, id)
	{
        this.x = null;
        this.y = null;
        this.id = id;
        this.nom = nom;
        this.image = new Image();
        this.image.src = dicImage[nom];
	}
	tick(){
        if(this.x != null && this.y != null){
            ctx.drawImage(this.image, this.x*tuile, this.y*tuile, tuile/2, tuile/2);
        }
    }
    setPosition(x, y){
        this.x = Map.getCase(x);
        this.y = Map.getCase(y);
    }
    sauvegardeTank(tank){
        // retourne son nom et coordonnées
        return [tank.x, tank.y, tank.nom, tank.id];
    }
    static validationTank(tank1, tank2){
        if(tank1.positionValide() && tank2.positionValide()){
            if(tank1.x != tank2.x || tank1.y !=tank2.y){
                return true;
            }
            ajoutErreur("Les 2 tanks ne peuvent pas être un par dessus l'autre!");
        }
        return false;
    }

    positionValide(){
        if(this.x == null || this.x > map.largeur-1 || this.y == null || this.y > map.hauteur-1){
            ajoutErreur("Vous devez positionner "+this.nom);
            return false;
        }
        else if(map.grille[this.x][this.y].toLowerCase().indexOf("arbre") >-1){
            //validation que tank pas sur un arbre
            ajoutErreur(this.nom+" est sur un arbre!");
            return false;
        }
        return true;
    }
    setCoord(x,y){
        this.x = x;
        this.y = y;
    }
}