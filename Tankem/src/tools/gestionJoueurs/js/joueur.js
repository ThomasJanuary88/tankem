let coolPoint  = 0;

window.onload = () => {
	Util.setUpBase();
	coolPoint  = Number(document.querySelector('#totalPoints').value);
	setPointsFields();
	let champNum  = document.querySelectorAll('input[type=number]');
	for (let i=0; i<champNum.length;i++){
		champNum[i].onkeydown = function(){
			console.log()
			return false;
		}
	}
	document.querySelector('#messagePerso').onkeyup = ()=>{
		if(document.querySelector('#messagePerso').value.trim().length <= 0) {
			document.querySelector('#btn-update').disabled = true;
			document.querySelector('#messagePerso').style.backgroundColor='pink';
		}
		else{
			document.querySelector('#btn-update').disabled = false;	
			document.querySelector('#messagePerso').style.backgroundColor='white';
		}
	}

	document.querySelector('#btn-update').onmouseup = ()=>{
        $.ajax({
            type : "POST",
			url : "ajax-joueur.php",
			data : {
				message : document.querySelector('#messagePerso').value,
				coolPoints : Number(document.querySelector('#totalPoints').value),
				couleur_r : document.querySelector('#couleurTank').value.substr(1,2),
				couleur_g : document.querySelector('#couleurTank').value.substr(3,2),
				couleur_b : document.querySelector('#couleurTank').value.substr(5,2),
				vie : Number(document.querySelector('#pointsVie').value),
				force : Number(document.querySelector('#pointsForce').value),
				agilite : Number(document.querySelector('#pointsAgilite').value),
				dexterite : Number(document.querySelector('#pointsDexterite').value)
			}
        })
        .done(() => {
			window.location.href = "joueur.php";
        })
	}
	$("input[type=number]").change(function(){
		setPointsFields();
	})
}

function setPointsFields(){
	let vie = Number(document.getElementById('pointsVie').value);
	let force = Number(document.getElementById('pointsForce').value);
	let dexterite = Number(document.getElementById('pointsDexterite').value);
	let agilite = Number(document.getElementById('pointsAgilite').value);
	document.getElementById('availablePoints').value  = coolPoint - vie - force - dexterite - agilite;
	validateCoolPoints();
}

function validateCoolPoints() {
	if(Number(document.getElementById('totalPoints').value) < 0) {
		document.querySelector('#btn-update').disabled = true;
		document.getElementById('totalPoints').style.backgroundColor = 'pink';
	}
	else{
		document.querySelector('#btn-update').disabled = false;
		document.getElementById('totalPoints').style.backgroundColor = 'white';
	}
}




