<?php
	require_once("action/JoueurAction.php");

	$action = new JoueurAction();
	$action->execute();

	require_once("partiel/header.php");
	require_once("partiel/navBar.php");
?>   

<link rel="stylesheet" href="css/joueur.css">
<script src="js/joueur.js"></script>
<script src="js/Util.js"></script>
<script src="js/jscolor.min.js"></script>

<!-- <div id="changerMdp">
	<button type="button" class="btn btn-info btn-lg" id="btn-modal-mdp">Changer le mot de passe</button>
</div> -->

<div id="containerInfos">
	<fieldset>
		<legend><h3>Informations du joueur</h3></legend>
		<div class="ligne">
			<label>Nom du joueur : </label>
			<input disabled type='text' id='nomJoueur' size="50" value= <?= $_SESSION["username"] ?>>
		</div>
		<div class="ligne">
			<label>Message personnalisé : </label>
			<input type='text' id='messagePerso' size="50" required  value=' <?= $action->result->message ?> '>
		</div>  
		<div class="ligne">
			<label>Couleur du tank : </label>
			<input type='color' id='couleurTank' value='#<?= $action->result->couleur_r ?><?= $action->result->couleur_g ?><?= $action->result->couleur_b ?>'>
		</div>  
		<div class="ligne">
			<label>Expérience : </label>
			<input disabled type='text' id='experience' size="20" value=<?= $action->result->experience ?>>
			<label> points</label>
		</div>  
		<div class="ligne">
			<label>Niveau : </label>
			<input disabled type='text' id='niveau' size="20" value=<?= $action->result->niveau ?>>
		</div> 
		<div class="ligne">
			<label>Dernière partie le : </label>
			<input disabled type='text' id='datePartie' size="30"  value='<?= $action->result->date_partie ?>'>
		</div>   
	</fieldset>

	<fieldset>
		<legend><h3>Attributs</h3></legend>
		<div class="ligne">
			<label>CoolPoints à attribuer : </label>
			<input disabled type='text' id='availablePoints' size="5">
			<input type='hidden' id='totalPoints' size="5" value=<?= $action->result->coolPoints ?>>
			<label> points</label>
		</div>
		<div class="ligne">
			<label>Vie : </label>
			<input type='number' id='pointsVie' min='0' step='1' max='20' value=<?= $action->result->vie ?>>
			<label> points</label>
		</div>  
		<div class="ligne">
			<label>Force : </label>
			<input type='number' id='pointsForce'  min='0' step='1'  max='20' value=<?= $action->result->force ?>>
			<label> points</label>
		</div>  
		<div class="ligne">
			<label>Agilité : </label>
			<input type='number' id='pointsAgilite'  min='0' step='1' max='20' value=<?= $action->result->dexterite ?>>
			<label> points</label>
		</div>  
		<div class="ligne">
			<label>Dextérité : </label>
			<input type='number' id='pointsDexterite'  min='0' step='1'  max='20' value=<?= $action->result->agilite ?>>
			<label> points</label>
		</div>  
	</fieldset>
	<div class="ligne">
		<button type="button" class="btn btn-info btn-lg" id="btn-update">SAUVEGARDER</button>
		<p id='displayMessage'></label>
	</div>  
</div>

<!-- <div id= "modalMDP" style="position: fixed; right: -200px; top: 130px; display:none" >
	<div style="position: relative; left: -50%; top:-50%;">
		<fieldset>
			<legend><h3>Mot de passe</h3></legend>
			<div class="ligne">
				<input autofocus class = "focus " type="password" name="reset-mdp" id="reset-mdp" minlength="4" maxlength="20" size="50" 
				pattern="[a-zA-Z0-9_]{4,20}" required autofocus placeholder="Mot de passe" />
				<span class="validity"></span>
			</div>
			<div class="ligne">
				<input autofocus class = "focus " type="password" name="confirm-reset-mdp" id="confirm-reset-mdp" minlength="4" maxlength="20" size="50" 
				pattern="[a-zA-Z0-9_]{4,20}" required autofocus placeholder="Confirmer le mot de passe" />
				<span class="validity"></span>
			</div>      
			<button type="button" class = "btn btn-info btn-lg" id="soumettreMDP">Soumettre</button>
			<button type="button" class = "btn btn-info btn-lg" id="annulerMDP">Annuler</button>
		</fieldset>
	</div>
</div> -->

<?php

	require_once("partiel/footer.php");