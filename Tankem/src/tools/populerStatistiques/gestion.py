from random import randint
from joueur import Joueur
from partie import Partie
import datetime

class Gestion():
    
    def __init__(self,nbJoueur = 3, skill = 0):
        self.script = []
        self.skill = skill
        self.listMapId = [1,2,3]
        self.listJoueur = []
        self.creationJoueur(nbJoueur,self.calculNivSkill())
        

    def calculNivSkill(self):
        neg = randint(0,100)
        mod = -1
        if neg < 50:
            mod = 1

        diff = randint(0,self.skill)
        return diff *mod


    def creationJoueur(self, nbJoueur = 1, skill = 0):
        listType = ["vie", "force", "agilite", "dexterite"]

        for i in range(nbJoueur):
            index = randint(0,len(listType)-1)
            id = len(self.listJoueur)
            self.listJoueur.append(Joueur(listType[index],id+1, skill))
        
    def choixMap(self):
        index = randint(0,len(self.listMapId)-1)
        return self.listMapId[index]

    def run(self, nbPartie = 255,creationJoueur = 5):
        #setup des armes disponibles pour les parties
        listArme = []
        listArme.append([1,"canon", 50, 1.2, 70])
        listArme.append([2,"mitraillette", 50, 0.4, 70])
        listArme.append([3,"grenade", 50, 16.0, 50])
        listArme.append([4,"shotgun", 50, 1.8, 80])
        listArme.append([5,"piege", 50, 1.0, 40])
        listArme.append([6,"missile", 50, 3.0, 80])
        listArme.append([7,"spring", 50, 0.5, 95])
        self.ajoutArmeScript(listArme)
        ####je dois ajouter une date debut fin
        dtDebut = datetime.datetime(2018,3,1)
        idPartie = 1
        # generation de x partie
        for i in range(nbPartie):
            #possibilite de nouveau joueur
            if randint(0,100) < creationJoueur:
                self.creationJoueur(1,self.calculNivSkill())
            #modification d'un debut d'une partie
            dtDebut += datetime.timedelta(minutes=randint(0,30), seconds=randint(0,60))
            # choix d'une map
            idMap = self.choixMap()
            # choix des joueurs
            index1 = self.choixJoueur()
            index2 = self.choixJoueur(index1)
            # creation d'une partie
            partie = Partie(self.listJoueur[index1],self.listJoueur[index2], idMap, listArme)
            #script pour la partie
            #le temps de la partie est un nombre de tour qu'on multiplie par le nombre de seconde voulu
            dtFin = dtDebut + datetime.timedelta(seconds = partie.temps*5)
            self.ajoutPartieScript(idPartie,dtDebut,dtFin,idMap,index1,index2,partie.gagnant())
            self.ajoutArmePartieScript(idPartie,partie.getStat())
            idPartie +=1
        self.ajoutJoueurScript(self.listJoueur)
        self.enregistrementFichier()
        print("Fin de l'execution")

    def enregistrementFichier(self):
        script = '\n'.join(self.script)
        fichier = open("tools/populerStatistiques/populerStatistiques.sql", "w")
        fichier.write(script)
        fichier.close()

    def enrPartie(self, partie):
        partie.getStat()

    def choixJoueur(self, diff = -1):
        index = diff
        while index == diff:
            index = randint(0,len(self.listJoueur)-1)
        return index

    def getCouleur(self):
        return randint(0,255)

    def ajoutJoueurScript(self, listJoueur):
        ####on doit ajouter au debut du script###
        #index pour differencier les joueurs
        listJoueur.reverse()
        index = len(listJoueur)
        for joueur in listJoueur:
            #id,phrase,experience,vie,force,dexterite,agilite,nom,mdp,couleurR,couleurG,couleurB
            texte = "Execute addJoueur(%d,%d,%d,%d,'%s',%d,%d,%d,%d,%d,'%s','%s')" \
            %(joueur.id,self.getCouleur(),self.getCouleur(),self.getCouleur(),"phrase"+str(index),joueur.xp,joueur.coolVie,joueur.coolForce,joueur.coolAgilite,joueur.coolDexterite,"nom"+str(index),"mdp"+str(index))
            self.script.insert(0,texte)
            index -=1

    def ajoutArmeScript(self, listArme):
        for arme in listArme:
            texte = "Execute addArme(%d,'%s')"%(arme[0],arme[1])
            self.script.append(texte)

    def ajoutArmePartieScript(self, idPartie, stat):
        for joueur in stat:
            id = joueur[0]
            for arme in joueur[1]:
                texte = "Execute setJoueurArmePartie(%d,%d,%d,%d,%d)"%(id,arme[0],idPartie,arme[1],arme[2])
                self.script.append(texte)

    def ajoutPartieScript(self,idPartie,dtDebut,dtFin,idMap,index1,index2,gagnant):
        idGagnant = "null"
        if gagnant:
            idGagnant = gagnant.id
        texte = "Execute addPartie(%d,to_date('%s','YYYY/MM/DD HH24:MI:SS'),to_date('%s','YYYY/MM/DD HH24:MI:SS'),%d,%d,%d,%s,%d,%d)" \
        %(idPartie,str(dtDebut),str(dtFin),idMap, self.listJoueur[index1].id,self.listJoueur[index2].id,idGagnant,self.listJoueur[index1].distance,self.listJoueur[index2].distance)
        self.script.append(texte)


