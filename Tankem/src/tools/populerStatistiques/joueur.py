from random import randint
from arme import Arme

class Joueur():
    
    def __init__(self, type, id, skill):
        self.id = id
        self.skill = skill
        self.ennemi = None
        self.type = type
        self.coolVie = 0
        self.coolForce = 0
        self.coolAgilite = 0
        self.coolDexterite = 0
        self.pointVieMaxBase = 200
        self.vitesseBase = 7.0
        self.xp = 0
        self.niv = 1
        self.nextLvl = self.xpNextLvl()
        self.coolParNiv = 5
        self.maxCoolPoint = 20
        self.modifBonus()

    def getStatJoueur(self):
        return []

    def getDistance(self):
        return self.distance

    def getStatPartie(self):
        listStat = []
        for arme in self.arme:
            listStat.append(arme.getStat())
        return listStat

    def lancerPartie(self, ennemi, listArme):
        self.setEnnemi(ennemi)
        self.resetInfo(listArme)

    def setEnnemi(self, ennemi):
        self.ennemi = ennemi

    def estVivant(self):
        return self.pointVie > 0

    def resetInfo(self, listArme):
        self.pointVie = self.pointVieMax
        self.distance = 0
        self.deltaTemps = 0
        self.arme =[]
        for arme in listArme:
            self.arme.append(Arme(arme[0],arme[1],arme[2],arme[3],arme[4]))

    def modifBonus(self):
        self.pointVieMax = int(self.pointVieMaxBase *0.05 *self.coolVie) + self.pointVieMaxBase
        self.pointVie = self.pointVieMax
        self.bonusDegat = self.coolForce *0.05
        self.vitesse = self.coolAgilite *0.025*self.vitesseBase + self.vitesseBase
        self.vitesseCharge = self.calcVitesseCharge(100,self.coolDexterite)
        
    def calcVitesseCharge(self,init,n):
        vitesse = 100
        for i in range(n):
            vitesse -= vitesse*0.025
        return vitesse

    def xpNextLvl(self):
        return 100*(self.niv + 1) +50*pow(self.niv,2)

    def monterNiv(self):
        self.niv +=1
        self.nextLvl = self.xpNextLvl()
        self.placerCoolPoint()

    def finPartie(self, xp):
        self.xp += xp
        if self.xp >= self.nextLvl:
            self.monterNiv()

    def choixAction(self):
        pourcentage = randint(0,100)
        if pourcentage < 50:
            self.bouge()

        self.deltaTemps -= 0.1
        if self.deltaTemps <=0:
            if pourcentage < 60:
                self.tireRegulier()
            elif pourcentage < 80:
                self.tireSpecial()

    def recevoirDegat(self, degat):
        self.pointVie -= degat
        if self.pointVie < 0:
            self.pointVie = 0

    def bouge(self):
        self.distance += self.vitesse *.9

    def tireRegulier(self): #canon
        self.ennemi.recevoirDegat(self.arme[0].tire(self.skill))
        self.deltaTemps = self.arme[0].recharge * (self.vitesseCharge/100)

    def tireSpecial(self):
        pourcentage = randint(0,100)
        courant = None
        if pourcentage <= 30: #mitraillette
            self.ennemi.recevoirDegat(self.arme[1].tire(self.skill))
            courant = self.arme[1]
        elif pourcentage <= 50: #grenade
            self.ennemi.recevoirDegat(self.arme[2].tire(self.skill))
            courant = self.arme[2]
        elif pourcentage <=70: #shotgun
            self.ennemi.recevoirDegat(self.arme[3].tire(self.skill))
            courant = self.arme[3]
        elif pourcentage <=80: #piege
            self.ennemi.recevoirDegat(self.arme[4].tire(self.skill))
            courant = self.arme[4]
        elif pourcentage <=90: #missile
            self.ennemi.recevoirDegat(self.arme[5].tire(self.skill))
            courant = self.arme[5]
        elif pourcentage <= 100: #spring
            self.ennemi.recevoirDegat(self.arme[6].tire())
            courant = self.arme[6]
        self.deltaTemps = courant.recharge * (self.vitesseCharge/100)

    def placerCoolPoint(self):
        primaire = 40
        deuxieme = 60
        troisieme = 80
        quatrieme = 100
        pointRestants = self.coolParNiv
        count = 0
        while pointRestants > 0 and count <1000:
            pourcentage = randint(0,100)
            if self.type == "vie":
                if self.coolVie < self.maxCoolPoint and pourcentage <= primaire:
                    pointRestants -= 1
                    self.coolVie += 1
                elif self.coolForce < self.maxCoolPoint and pourcentage <= deuxieme:
                    pointRestants -= 1
                    self.coolForce += 1
                elif self.coolAgilite < self.maxCoolPoint and pourcentage <= troisieme:
                    pointRestants -= 1
                    self.coolAgilite += 1
                elif self.coolDexterite < self.maxCoolPoint and pourcentage <= quatrieme:
                    pointRestants -= 1
                    self.coolDexterite += 1
            elif self.type == "force":
                if self.coolForce < self.maxCoolPoint and pourcentage <= primaire:
                    pointRestants -= 1
                    self.coolForce += 1
                elif self.coolVie < self.maxCoolPoint and pourcentage <= deuxieme:
                    pointRestants -= 1
                    self.coolVie += 1
                elif self.coolAgilite < self.maxCoolPoint and pourcentage <= troisieme:
                    pointRestants -= 1
                    self.coolAgilite += 1
                elif self.coolDexterite < self.maxCoolPoint and pourcentage <= quatrieme:
                    pointRestants -= 1
                    self.coolDexterite += 1
            elif self.type == "agilite":
                if self.coolAgilite < self.maxCoolPoint and pourcentage <= primaire:
                    pointRestants -= 1
                    self.coolAgilite += 1
                elif self.coolVie < self.maxCoolPoint and pourcentage <= deuxieme:
                    pointRestants -= 1
                    self.coolVie += 1
                elif self.coolForce < self.maxCoolPoint and pourcentage <= troisieme:
                    pointRestants -= 1
                    self.coolForce += 1
                elif self.coolDexterite < self.maxCoolPoint and pourcentage <= quatrieme:
                    pointRestants -= 1
                    self.coolDexterite += 1
            elif self.type == "dexterite":
                if self.coolDexterite < self.maxCoolPoint and pourcentage <= primaire:
                    pointRestants -= 1
                    self.coolDexterite += 1
                elif self.coolVie < self.maxCoolPoint and pourcentage <= deuxieme:
                    pointRestants -= 1
                    self.coolVie += 1
                elif self.coolForce < self.maxCoolPoint and pourcentage <= troisieme:
                    pointRestants -= 1
                    self.coolForce += 1
                elif self.coolAgilite < self.maxCoolPoint and pourcentage <= quatrieme:
                    pointRestants -= 1
                    self.coolAgilite += 1
            else:
                if self.coolDexterite < self.maxCoolPoint and pourcentage <= 25:
                    pointRestants -= 1
                    self.coolDexterite += 1
                elif self.coolVie < self.maxCoolPoint and pourcentage <= 50:
                    pointRestants -= 1
                    self.coolVie += 1
                elif self.coolForce < self.maxCoolPoint and pourcentage <= 75:
                    pointRestants -= 1
                    self.coolForce += 1
                elif self.coolAgilite < self.maxCoolPoint and pourcentage <= 100:
                    pointRestants -= 1
                    self.coolAgilite += 1
            count +=1
        self.modifBonus()

