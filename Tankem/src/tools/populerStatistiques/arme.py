from random import randint

class Arme():
    
    def __init__(self,id, nom, degat, recharge, reussite = 100):
        self.id = id
        self.nom = nom
        self.nbTirs = 0
        self.nbTirsReussi = 0
        self.degat = degat
        self.recharge = recharge
        self.reussite = reussite

    def tire(self, skill = 0):
        self.nbTirs +=1
        pourcentage = randint(0,100)
        if pourcentage < self.reussite +skill:
            self.nbTirsReussi += 1
            return self.getDegat()
        return 0

    def getDegat(self):
        variation = randint(80,120)
        return int(self.degat * variation /100)

    def getStat(self):
        return [self.id,self.nbTirs, self.nbTirsReussi]
