from random import randint
from arme import Arme

class Partie():
    
    def __init__(self, j1, j2, map, listArme):
        self.temps = 0 
        self.map = map
        self.listJoueur = [j1, j2]
        self.setupPartie(listArme)
        self.indexFavori = self.trouverFavori()
        self.deroulementPartie()
        
        
    def trouverFavori(self):
        indexFavori = None
        if self.listJoueur[0].niv > self.listJoueur[1].niv:
            indexFavori = 0
        elif self.listJoueur[0].niv < self.listJoueur[1].niv:
            indexFavori = 1
        return indexFavori

    def getStat(self):
        stat = []
        for joueur in self.listJoueur:
            stat.append([joueur.id, joueur.getStatPartie()])
        return stat

    def gagnant(self):
        gagnant = None
        for joueur in self.listJoueur:
            if joueur.estVivant():
                gagnant = joueur
        return gagnant

    def setupPartie(self,listArme):
        self.listJoueur[0].lancerPartie(self.listJoueur[1], listArme)
        self.listJoueur[1].lancerPartie(self.listJoueur[0], listArme)

    def deroulementPartie(self):
        enCour = True
        while enCour:
            nbEnVie = 0
            self.temps +=1
            for joueur in self.listJoueur:
                joueur.choixAction()
                if joueur.estVivant():
                    nbEnVie +=1
                enCour =  nbEnVie == len(self.listJoueur)
        for joueur in self.listJoueur:
            joueur.finPartie(self.calculXp(joueur, joueur.ennemi))

    def calculXp(self, joueur, ennemi):
        exp = 0
        favori = False
        if joueur.estVivant():
            if self.indexFavori:
                favori = joueur == self.listJoueur[self.indexFavori]
            exp = 100 + 100*int(favori) +2*joueur.pointVie
        else:
            exp = 2*(ennemi.pointVieMax - ennemi.pointVie)
        return exp

        
