class Util{
    constructor(){}

        static getInt(node){
            //vu que champ est number renvoit string vide si texte est saisi
            if(node.value == ""){
                ajoutErreur("Vous devez inscrire un chiffre!");
                return 0;
            }
            return parseInt(node.value);
        }

        static clampChiffre(valeur, min, max){
            if(valeur < min){
                return min;
            }
            else if(valeur > max){
                return max;
            }
            return Math.trunc(valeur);
        }
        static clampFloat(valeur, min, max){
            if(valeur < min){
                return min;
            }
            else if(valeur > max){
                return max;
            }
            return valeur;
        }
        static getFloat(node){
            if(node.value == ""){
                ajoutErreur("Vous devez inscrire un chiffre!");
                return 0;
            }
            return parseFloat(node.value);
        }
    }