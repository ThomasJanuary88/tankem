﻿let canvas = null;
let ctx = null;
let tuile = 75;
let minDim = 6;
let maxDim = 12;
let error = [];
let curseur = null;
let map = null;
let tank1 = null;
let tank2 = null;
let listeChamp = [];
let dicImage = {
    "tank1": "images/tankJ1.png",
    "tank2":"images/tankJ2.png",
    "mur":"images/wall.png",
    "arbre":"images/brush.png",
    "mobile":"images/flecheUp.png",
    "inverse":"images/flecheDown.png",
    "murArbre": "images/wallBrush.png",
    "mobileArbre":"images/flecheUpBrush.png",
    "inverseArbre":"images/flecheDownBrush.png",
    "gazon":"images/moss150px.png"
}

window.onload = ()=>{
    canvas = document.getElementById("canvas");
    ctx = canvas.getContext("2d");
    // ajout validation à l'intérieur du formulaire
    setAttributMap(document.getElementById("x"), minDim, maxDim, minDim);
    setAttributMap(document.getElementById("y"), minDim, maxDim, minDim);
    setAttributMap(document.getElementById("min"),0,999,3);
    setAttributMap(document.getElementById("max"),0,999,20);    
    // événement pour toute la fenêtre
    //création d'une liste en fonction de la classe focus
    //qui peut avoir le focus dans influencer le curseur

    let focus =  document.querySelectorAll(".focus");
    for(var i = 0; i < focus.length; i++) {
            listeChamp.push(focus[i].id);
    }
    window.onkeyup = function(event){
        let key = event.keyCode || event.which;
        if(key == 27){
            canvas.focus();
            curseur.annuleSelection();
        }
    }
    window.onkeypress = function(event){
        let key = event.keyCode || event.which;
        
        if(listeChamp.indexOf(document.activeElement.id) == -1){
            switch(key){
                case 32:
                    curseur.modifierMap(map.select["x"]*tuile,map.select["y"]*tuile)
                    break;
                case 49:
                    curseur.setSelection("tank1");
                    break;
                case 50:
                    curseur.setSelection("tank2");
                    break;
                case 51:
                    curseur.setSelection("mur");
                    break;
                case 52:
                    curseur.setSelection("arbre");
                    break;
                case 53:
                    curseur.setSelection("mobile");
                    break;
                case 54:
                    curseur.setSelection("inverse");
                    break;
                case 55:
                    curseur.setSelection("gazon");
                    break;
                case 97:
                    map.setSelect(-1,0);
                    break;
                case 115:
                    map.setSelect(0,1);
                    break;
                case 100:
                    map.setSelect(1,0);    
                    break;
                case 119:
                    map.setSelect(0,-1);            
                    break;
                default:
                    curseur.setSelection("");
                    break;

            }
        }
    }

    window.oncontextmenu = function(event){
        return false;
    }
    window.onmousedown = function(event){
        //annule sélection
        if(event.button ==2){
            curseur.annuleSelection();
        }
        
    }
    //items obligatoires
    curseur = new Curseur();
    map = new Map();
    tank1 = new Tank("tank1");
    tank2 = new Tank("tank2");
    // écouteurs élément spécifiques

    document.getElementById("buttonCharger").onclick = ()=>{
        chargerCarte();
        canvas.focus();
    }
    document.getElementById("buttonDim").onclick = ()=>{
        map.validatationValeurCarte();
        canvas.focus();
    }
    document.getElementById("buttonForm").onclick = ()=>{
        sauvegardeCarte();
        canvas.focus();
    }
    document.getElementById("buttonDel").onclick = ()=>{
        deleteCarte();
        canvas.focus();
    }
    document.getElementById("quadrille").onchange = ()=>{
        map.setQuadrille(document.getElementById("quadrille").checked);
    }
    let anchors =  document.querySelectorAll(".outils");
    for(var i = 0; i < anchors.length; i++) {
        anchors[i].onclick = function() {
            curseur.setSelection(this.id);
        }
    }
    canvas.onmouseenter = ()=>{
        curseur.setStatut(true);
    }
    canvas.onmouseout = ()=>{
        curseur.setStatut(false);
    }
    canvas.onmousemove = function (event) {
        curseur.controle = true;
		curseur.x = event.offsetX;
        curseur.y = event.offsetY;
        map.select["x"] = Map.getCase(curseur.x);
        map.select["y"] = Map.getCase(curseur.y);
    }
    canvas.onmouseup = function(event){
        curseur.modifierMap(event.offsetX,event.offsetY);
    }
    
    //boucle générale
    generalTick();
}

function generalTick(){
    ctx.clearRect(0,0,canvas.width, canvas.height);
    map.tick();
    curseur.tick();
    tank1.tick();
    tank2.tick();
	window.requestAnimationFrame(generalTick);
}

function ajoutErreur(message){
    error.push(message+"<br>");
    setTimeout(enleverErreur,10000);
    document.getElementById("erreur").innerHTML = error;
}
function enleverErreur(){
    error.shift();
    document.getElementById("erreur").innerHTML = error;
}
function setAttributMap(node, min, max, value){
    node.setAttribute("min", min);
    node.setAttribute("max", max);
    node.setAttribute("value", value);
}
function deleteCarte(){
    if(map.id!=null && tank1.id!=null && tank2!=null){
        $.ajax({
            type : "POST",
            url : "ajax-delete.php",
            data : {
                "idMap":map.id,
                "idTank1":tank1.id,
                "idTank2":tank2.id
            }
        })
        .done(() => {
            window.location.href = "index.php";
        })
    }
    else{
        ajoutErreur("Vous devez charger la carte à supprimer!");
    }
}

function sauvegardeCarte(){
    let listeTank = [];
    let liste = [];
    let existe = true;
    let overwrite = false;

    if(Tank.validationTank(tank1,tank2)){
        listeTank.push(tank1.sauvegardeTank(tank1));
        listeTank.push(tank2.sauvegardeTank(tank2));
        
        $.ajax({
            type : "POST",
            url : "ajaxMemeNom.php",
            data : {
                "nom":document.getElementById("nomCarte").value
            }
        })
        .done(response => {
            response = JSON.parse(response);
            existe = response;
            console.log(existe);
            if(existe){
                overwrite = confirm("Il y a deja une map existante avec le meme nom, voulez vous vraiment l'overwriter ?")
                if(overwrite){
                    liste = map.sauvegardeMap();
                    $.ajax({
                        type : "POST",
                        url : "ajax.php",
                        data : {
                            "tank":listeTank,
                            "nomCarte": document.getElementById("nomCarte").value,
                            "largeur": map.largeur,
                            "hauteur": map.hauteur,
                            "statut": document.getElementById("statutCarte").value,
                            "tempsMin": document.getElementById("min").value,
                            "tempsMax": document.getElementById("max").value,
                            "map":liste,
                            "idMap":map.id,
                        }
                })
                .done(response => {
                    try{
                        response = JSON.parse(response);
                        ajoutErreur(response);
                        document.getElementById("buttonForm").innerHTML = "Modifier";
                    }
                    catch(err){
                        ajoutErreur("Impossible de sauvegarder");
                    }
                    
        
                })
            }
            
            }
            else{
                liste = map.sauvegardeMap();
                    $.ajax({
                        type : "POST",
                        url : "ajax.php",
                        data : {
                            "tank":listeTank,
                            "nomCarte": document.getElementById("nomCarte").value,
                            "largeur": map.largeur,
                            "hauteur": map.hauteur,
                            "statut": document.getElementById("statutCarte").value,
                            "tempsMin": document.getElementById("min").value,
                            "tempsMax": document.getElementById("max").value,
                            "map":liste,
                            "idMap":map.id,
                        }
                })
                .done(response => {
                    document.getElementById("buttonForm").innerHTML = "Modifier";
                    try{
                        response = JSON.parse(response);
                        ajoutErreur(response);
                        document.getElementById("buttonForm").innerHTML = "Modifier";
                    }
                    catch(err){
                        ajoutErreur("Impossible de sauvegarder");
                    }
                })
            }
            
        })
        //console.log(existe);
        // liste = map.sauvegardeMap();
        // $.ajax({
        //     type : "POST",
        //     url : "ajax.php",
        //     data : {
        //         "tank":listeTank,
        //         "nomCarte": document.getElementById("nomCarte").value,
        //         "largeur": map.largeur,
        //         "hauteur": map.hauteur,
        //         "statut": document.getElementById("statutCarte").value,
        //         "tempsMin": document.getElementById("min").value,
        //         "tempsMax": document.getElementById("max").value,
        //         "map":liste,
        //         "idMap":map.id
        //     }
        // })
        // .done(() => {
        //     //response = JSON.parse(response);
        // })
    }  
}

function chargerCarte(){
    $.ajax({
        type : "POST",
        url : "ajax-carte.php",
        data : {
            "nomCarte":document.getElementById("chargerCarte").value
        }
    })
    .done(response => {
        response = JSON.parse(response);
        // récupération du nom
        document.getElementById("nomCarte").value = response["carte"]["NOM"];
        document.getElementById("min").value = response["carte"]["TEMPS_MIN"];
        document.getElementById("max").value = response["carte"]["TEMPS_MAX"];
        document.getElementById("x").value = response["carte"]["TAILLE_X"];
        document.getElementById("y").value = response["carte"]["TAILLE_Y"];
        document.getElementById("statutCarte").value = response["carte"]["STATUT"];
        // création d'un nouvel objet map
        map = new Map(response["carte"]["ID"]);
        map.validatationValeurCarte();
        let max = response["grille"].length;
        for(let i=0; i<max; i++){
            map.modifGrille(response["grille"][i]["NOM"],response["grille"][i]["COORD_X"],response["grille"][i]["COORD_Y"]);
            if(response["grille"][i]["ARBRE"] == "O"){
                map.modifGrille("arbre",response["grille"][i]["COORD_X"],response["grille"][i]["COORD_Y"]);
            }
        }
        // création des tanks
        tank1 = new Tank("tank1",response["carte"]["ID_TANK1"]);
        tank2 = new Tank("tank2",response["carte"]["ID_TANK2"]);
        tank1.setCoord(response["listTank"][0].COORD_X, response["listTank"][0].COORD_Y);
        tank2.setCoord(response["listTank"][1].COORD_X, response["listTank"][1].COORD_Y);
        document.getElementById("buttonForm").innerHTML = "Modifier";

    })
}