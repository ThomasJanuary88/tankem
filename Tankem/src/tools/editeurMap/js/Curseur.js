class Curseur{
	constructor(id)
	{
        this.x = null;
        this.y = null;
        this.statut = false;
        this.controle = true;
        this.selection = "";
        this.image = new Image();
        this.image.src = "";
	}
	tick(){
        if((this.statut || !this.controle) && this.selection !=""){
            ctx.drawImage(this.image, this.x - tuile/2, this.y -tuile/2, tuile, tuile);
        }
    }

    setSelection(objet){
        this.image.src = dicImage[objet] || "";
        this.selection = objet;
    }
    annuleSelection(){
        this.image.src = "";
        this.selection = "";
    }

    setStatut(bool){
        this.statut = bool;
    }
    modifierMap(posX, posY){
        if(this.selection == "tank1"){
            tank1.setPosition(posX, posY);
        }
        else if(this.selection == "tank2"){
            tank2.setPosition(posX, posY);            
        }
        else{
            map.modifGrille(this.selection,Map.getCase(posX),Map.getCase(posY));
        }
    }

}