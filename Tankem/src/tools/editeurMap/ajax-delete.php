<?php
	require_once("action/AjaxDeleteAction.php");

	$action = new AjaxDeleteAction();
	$action->execute();

	echo json_encode($action->result);