<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/Data.php");

	class AjaxDeleteAction extends CommonAction {

		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		
		}

		protected function executeAction() {
			#appel du DAO pour sauvegarder la map et les tanks
            $idMap = $_POST["idMap"];
            $idTank1 = $_POST["idTank1"];
            $idTank2 = $_POST["idTank2"];
			
			Data::suppUneMap($idMap,$idTank1,$idTank2);
		}
	
	}	