<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/Data.php");

	class AjaxCarteAction extends CommonAction {
		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		
		}

		protected function executeAction() {
			#appel du DAO recuperer données sur une carte
			$nomCarte = $_POST["nomCarte"];
			$carte = Data::recupererCarte($nomCarte);
            #appel du DAO recuperer données les infos sur les tanks liés à une carte
            $tank1 = Data::recupererTank($carte[0]["ID_TANK1"]);
			$tank2 = Data::recupererTank($carte[0]["ID_TANK2"]);
            #appel du DAO recuperer données les infos sur les tuiles liés à une carte
			$grille = Data::recupererGrille($carte[0]["ID"]);
			#construction d'un objet temporaire pour stocker les données
			$results= array(
				"carte" => $carte[0],
				"listTank" => array($tank1[0],$tank2[0]),
				"grille" => $grille
			);
			$this->result = $results;
		}
	
	}	