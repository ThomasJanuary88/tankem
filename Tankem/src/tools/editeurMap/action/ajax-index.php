<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/Data.php");

	class AjaxIndexAction extends CommonAction {

		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		
		}

		protected function executeAction() {
			#appel du DAO pour sauvegarder la map et les tanks
			$nomCarte = $_POST["nomCarte"];
			$largeur = $_POST["largeur"];
			$hauteur = $_POST["hauteur"];
			$listeTanks = $_POST["tank"];
			$statut = $_POST["statut"];
			$tempsMin = $_POST["tempsMin"];
			$tempsMax = $_POST["tempsMax"];
			if(isset($_POST["map"])){
				$listeTuiles = $_POST["map"];
			}
			else{
				$listeTuiles = null;
			}
			$idMap = $_POST["idMap"];
			$this->result = Data::update($nomCarte, $largeur, $hauteur, $listeTanks, $listeTuiles, $statut, $tempsMin, $tempsMax, $idMap);
		}
	
	}	