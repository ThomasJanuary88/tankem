<?php
	require_once("action/DAO/connection.php");
	class Data {
		public static function ajoutBd($nom,$message){
			try{
			$connection = Connection::getConnection();

			$statement = $connection->prepare("INSERT INTO stack_answers (nom,message) VALUES(?,?)");
			$statement->bindParam(1,$nom);
			$statement->bindParam(2,$message);
			$statement->execute();
		}
		catch(Exception $e){}
		}

		public static function listeCarte(){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT nom FROM map ORDER BY nom");
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){
				$array[] = array("NOM"=>"Connexion impossible!");
				return $array;
			}
		}

		public static function recupererCarte($nomCarte){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT m.id, m.nom, m.date_creation,
				 s.nom statut, m.taille_x, m.taille_y, m.id_tank1, m.id_tank2,
				 m.temps_min, m.temps_max 
				 FROM map  m INNER JOIN statut s
				 	ON m.id_statut = s.id WHERE m.nom = ?");
				$statement->bindParam(1, $nomCarte);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){
				$array[] = array("ID"=>"Connexion impossible!");
				return $array;
			}
		}
		public static function recupererTank($id){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT * FROM tank WHERE id = ?");
				$statement->bindParam(1, $id);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){
				$array[] = array("ID"=>"Connexion impossible!");
				return $array;
			}
		}
		public static function recupererGrille($id){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT r.coord_x,r.coord_y,ty.nom,t.arbre FROM relation r
				INNER JOIN tuile t ON r.id_tuile = t.id
					INNER JOIN type ty ON t.id_type = ty.id
				WHERE r.id_map = ?");
				$statement->bindParam(1, $id);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){
				$array[] = array("ID"=>"Connexion impossible!");
				return $array;
			}
		}

		public static function verifationMapExistante($nomMap){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT id, id_tank1, id_tank2 from map WHERE nom = ?");
				$statement->bindParam(1, $nomMap);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();

				return $statement->fetchall();
			}
			catch(Exception $e){
				echo $e->getMessage();
			}
		}

		public static function recupererIdCarte($nomCarte){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT id, id_tank1, id_tank2 from map WHERE nom = ?");
				$statement->bindParam(1, $nomCarte);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();

				return $statement->fetchall();
			}
			catch(Exception $e){
				echo $e->getMessage();
			}
		}

		public static function update($nomCarte, $largeur, $hauteur, $listeTanks, $listeTuiles, $statut, $tempsMin, $tempsMax, $idMap){
			try{
				$connection = Connection::getConnection();

				$date = date('y-m-d');
				$statut = Data::recupererIdStatut($statut)[0][0];

				// Verification si la map existe deja, si oui, ca retourne une liste avec les 2 id des tanks
				// $infoMap = Data::verifationMapExistante($nomCarte);
				// Nouvelle map
				if($idMap == -1){
					//Récupere le dernier ID de la map dans la variable
					// $idCarte, $nomCarte, $date, $statut, $largeur, $hauteur, $idTank1, $coord_x_tank1, $coord_y_tank1, $idTank2, $coord_x_tank2, $coord_y_tank2,  $tempsMin, $tempsMax
					$idMap = Data::insertMap(null , $nomCarte, $date, $statut, $largeur, $hauteur, null, $listeTanks[0][0], $listeTanks[0][1], null, $listeTanks[1][0], $listeTanks[1][1], $tempsMin, $tempsMax);
					if($idMap == null){
						return "Échec de l'ajout";
					}
					if(sizeof($listeTuiles) > 0){
						foreach($listeTuiles as $uneTuile){
							$idTuile = Data::insertTuile($uneTuile[2]);
							Data::insertRelation($idTuile[0][0], $idMap[0][0], $uneTuile[0], $uneTuile[1]);
						}
					}
				}
				//Map deja existante
				else{
					Data::insertMap($idMap, $nomCarte, $date, $statut, $largeur, $hauteur, $listeTanks[0][3], $listeTanks[0][0], $listeTanks[0][1], $listeTanks[1][3], $listeTanks[1][0], $listeTanks[1][1], $tempsMin, $tempsMax);
					//Si on edite une ancienne map
					if($idMap != null){
						Data::supprimerTuilesMap($idMap);
					}
					// Si on edite la map qu'on vient de créer et d'envoyer dans la BD
					else{
						Data::supprimerTuilesMap($idMap);
					}
					
					if(sizeof($listeTuiles) > 0){
						foreach($listeTuiles as $uneTuile){
							$idTuile = Data::insertTuile($uneTuile[2]);
							//idTuile, idMap, coord_x, coord_y
							Data::insertRelation($idTuile, $idMap, $uneTuile[0], $uneTuile[1]);
						}
					}
				}
			}
			
			catch(Exception $e){
				echo $e->getMessage();
			}
			return "La mise à jour c'est fait avec succès";
		}

		
		public static function suppUneMap($idMap,$idTank1,$idTank2){
			Data::supprimerTuilesMap($idMap);
			Data::suppTank($idTank1);
			Data::suppTank($idTank2);
			Data::suppMap($idMap);
		}

		public static function supprimerTuilesMap($idMap){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("DELETE FROM relation WHERE id_map = ?");
				$statement->bindParam(1, $idMap);
				$statement->execute();
			}
			catch(Exception $e){
				echo $e->getMessage();
			}
		}

		
		public static function suppTank($id){
			try{
			$connection = Connection::getConnection();
			$statement = $connection->prepare("DELETE FROM tank WHERE id = ?");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			}
			catch(Exception $e){}
		}

		
		public static function suppMap($id){
			try{
			$connection = Connection::getConnection();
			$statement = $connection->prepare("DELETE FROM map WHERE id = ?");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			}
			catch(Exception $e){}
		}

		public static function insertRelation($id_tuile, $id_map, $x, $y){
			
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("CALL setRelation(null, ?, ?, ?, ?)");
				$statement->bindParam(1, $id_tuile);
				$statement->bindParam(2, $id_map);
				$statement->bindParam(3, $x);
				$statement->bindParam(4, $y);
	
				$statement->execute();
			}
			catch(Exception $e){
				echo $e->getMessage();
			}
			
		}

		public static function insertTuile($object){
			try{
				$arbre = "O";
				$nomType = "vide";
				// Dans le switch case, c'est pour tout ce qui a des arbres, par défaut, on prend juste l'objet
				switch ($object) {
					case "inverseArbre":
						$nomType = "inverse";
						break;
					case "mobileArbre":
						$nomType = "mobile";
						break;
					case "murArbre":
						$nomType = "mur";
						break;
					case "arbre":
						$nomType = "vide";
						break;
					default:
						$arbre = "N";
						$nomType = $object;
				}

				$idType = Data::getIdType($nomType);
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT id FROM tuile WHERE id_type=? AND arbre = ?");
				$statement->bindParam(1, $idType[0]["ID"]);
				$statement->bindParam(2, $arbre);
				$statement->execute();
				$idTuile = $statement->fetchall();
				return $idTuile[0][0];
			}
			catch(Exception $e){
				echo $e->getMessage();
			}
		}

		public static function getIdType($nom){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT id FROM type WHERE nom = ?");
	
				$statement->bindParam(1, $nom);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){
				echo $e->getMessage();
			}
			
		}

		public static function insertMap($idCarte, $nomCarte, $date, $statut, $largeur, $hauteur, $idTank1, $coord_x_tank1, $coord_y_tank1, $idTank2, $coord_x_tank2, $coord_y_tank2,  $tempsMin, $tempsMax){

			try{
				$connection = Connection::getConnection();
// $idCarte, $nomCarte, $date, $statut, $largeur, $hauteur, $idTank1, $coord_x_tank1, $coord_y_tank1, $idTank2, $coord_x_tank2, $coord_y_tank2,  $tempsMin, $tempsMax				
				$statement = $connection->prepare("CALL setMap(?,?,to_date(?, 'YY-MM-DD'),?,?,?,?,?,?,?,?,?,?,?)");
				$statement->bindParam(1, $idCarte);
				$statement->bindParam(2, $nomCarte);
				$statement->bindParam(3, $date);
				$statement->bindParam(4, $statut);
				$statement->bindParam(5, $largeur);
				$statement->bindParam(6, $hauteur);
				$statement->bindParam(7, $idTank1);
				$statement->bindParam(8, $coord_x_tank1);
				$statement->bindParam(9, $coord_y_tank1);
				$statement->bindParam(10, $idTank2);
				$statement->bindParam(11, $coord_x_tank2);
				$statement->bindParam(12, $coord_y_tank2);
				$statement->bindParam(13, $tempsMin);
				$statement->bindParam(14, $tempsMax);
				if($statement->execute()){
					// Récupere le id de la map que l'on vient d'inserer
					$statementId = $connection->prepare("SELECT MAX(id) from map");
					$statementId->execute();
					return $statementId->fetchall();
				}
				return null;
				
			}
			catch(Exception $e){
				echo $e->getMessage();
			}
			

		}

		public static function recupererIdStatut($statut){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("SELECT id FROM statut WHERE nom = ?");
				$statement->bindParam(1, $statut);
				$statement->execute();
				return $statement->fetchall();
			}
			catch(Exception $e){
				echo $e->getMessage();
			}
		}

		public static function updateTank($unTank){
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("INSERT INTO tank (coord_x,coord_y) VALUES (?,?)");
				$statement->bindParam(1, $unTank[0]);
				$statement->bindParam(2, $unTank[1]);
				$statement->execute();

				// Va chercher le id du tank qu'on vien d'inserer
				$statementId = $connection->prepare("SELECT MAX(id) from tank");
				$statementId->execute();
				return $statementId->fetchall();
			}
			catch(Exception $e){
				echo $e->getMessage();
			}
		}

		
	}