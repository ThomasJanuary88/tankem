<?php
	class UserDAO {

		public static function authenticate($username, $pwd) {
			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("SELECT * FROM USERS WHERE username = ?");
			$statement->bindParam(1, $username);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			
			$user = null;

			if ($user = $statement->fetch()) {
				if (!password_verify($pwd, $user["PASSWORD"])) {
					$user = null;
				}
			}

			return $user;
		}

		public static function changePassword($id, $pwd) {
			$connection = Connection::getConnection();

			// $statement ...
		}
	}
// 	<?php
// $stmt = $dbh->prepare("CALL sp_takes_string_returns_string(?)");
// $value = 'hello';
// $stmt->bindParam(1, $value, PDO::PARAM_STR|PDO::PARAM_INPUT_OUTPUT, 4000); 

// // appel de la procédure stockée
// $stmt->execute();

// print "La procédure a retourné : $value\n";
// ?>