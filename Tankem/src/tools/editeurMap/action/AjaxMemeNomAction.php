<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/Data.php");

	class AjaxMemeNomAction extends CommonAction {

		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		
		}

		protected function executeAction() {
			#appel du DAO pour sauvegarder la map et les tanks
            $nomMap = $_POST["nom"];
			
			$existe = Data::verifationMapExistante($nomMap);
			if(sizeof($existe) == 0){
				$this->result = false;
			}
			else{
				$this->result = true;
			}
		}
	
	}	