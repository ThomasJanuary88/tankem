<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/Data.php");

	class IndexAction extends CommonAction {
		public $listeCarte;
		
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			// demande au DAO pour avoir la liste des cartes
			$this->listeCarte = Data::listeCarte();
		}
	}	