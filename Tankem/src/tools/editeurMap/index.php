<?php
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();

	require_once("partiel/header.php");
?>     
<script src="js/index.js"></script>
<script src="js/curseur.js"></script>
<script src="js/Map.js"></script>
<script src="js/Tank.js"></script>
<script src="js/Util.js"></script>
        <aside>
            <div id="form">
            <!-- <form action="index.php" method="post"> -->
                <fieldset>
                    <legend><h3>Charger une carte existante:</h3></legend>
                    <select name="chargerCarte" id="chargerCarte" class = "ligne">
                    <?php
                        foreach($action->listeCarte as $value){ 
                    ?>
                        <option value="<?=$value["NOM"]?>"><?=$value["NOM"]?></option>
                    <?php
                    }
                    ?>
                    </select>
                    <div>
                        <button class="button" id="buttonCharger" type="button">Charger</button>
                    </div>
                </fieldset>
                
                <fieldset>
                    <legend><h3>Nom du fichier:</h3></legend>
                        <div class = "ligne">
                            <input autofocus class = "focus " type="text" name="nomCarte" id="nomCarte" minlength="4" maxlength="20" size="50" 
                            pattern="[a-zA-Z0-9_]{4,20}" required autofocus placeholder="Le nom de la carte doit être unique" />
                            <span class="validity"></span>
                        </div>
                </fieldset>
                <fieldset>
                    <legend><h3>Info de la carte:</h3></legend>
                        <div>
                            <label for="x">Largeur : </label>
                            <input type="number" class = "focus " name="x" id="x"/>
                            <span class="validity"></span>
                        </div>
                        <div>
                            <label for="y">Hauteur : </label>
                            <input type="number" class = "focus " name="y" id="y" />
                            <span class="validity"></span>
                        </div>
                        <div>
                            <label for="statutCarte">Statut : </label>
                            <select name="statutCarte" class = "focus " id="statutCarte">
                                <option selected value="Actif">Actif</option>
                                <option value="Inactif">Inactif</option>
                                <option value="Test">Test</option>
                            </select>
                        <div>
                            <label for="min">Temps min : </label>
                            <input type="number" step="0.1" class = "focus " name="min" id="min"/>
                            <label for="min">sec</label>
                            <span class="validity"></span>
                        </div>
                        <div>
                            <label for="max">Temps max : </label>
                            <input type="number" step="0.1" class = "focus " name="max" id="max"/>
                            <label for="max">sec</label>
                            <span class="validity"></span>
                        </div>
                        </div>
                        <div>
                            <input type="checkbox" name="quadrille" id="quadrille" checked> Quadrille
                        </div>
                        <div>
                            <button class="button" id="buttonDim" type="button">Valider</button>
                        </div>
                </fieldset>
                <fieldset>
                    <legend><h3>Outils de création:</h3></legend>
                        <div>
                            <img class = "outils" id="tank1" src="images/tankJ1.png" alt="Image pour ajout tank j1" height="75" width="75" title = "Joueur1 (1)">
                            <img class = "outils" id="tank2" src="images/tankJ2.png" alt="Image pour ajout tank j2" height="75" width="75" title = "Joueur2 (2)">
                            <img class = "outils" id="mur" src="images/wall.png" alt="Image pour ajout mur fixe" height="75" width="75" title = "Mur (3)">
                            <img class = "outils" id="arbre" src="images/brush.png" alt="Image pour ajout arbre" height="75" width="75" title = "Arbre (4)">
                            <img class = "outils" id="mobile" src="images/flecheUp.png" alt="Image pour ajout mur mobile" height="75" width="75" title = "Mobile (5) ">
                            <img class = "outils" id="inverse" src="images/flecheDown.png" alt="Image pour ajout mur inversé" height="75" width="75" title = "Mobile inversé (6)">
                            <img class = "outils" id="gazon" src="images/moss150px.png" alt="Image pour ajout gazon" height="75" width="75" title = "Gazon (7)">
                            <p>Les touches de <strong>1 à 7</strong> permet la <strong>sélection</strong> des différents éléments de décor.</p>
                            <p>Les touches <strong>a-s-d-w</strong> permettent le <strong>déplacement</strong> sur la grille.</p>
                            <p>La touche <strong>d'espacement</strong> <strong>place</strong> l'élément à l'emplacement indiqué.</p>
                            <p>N'oubliez pas de ne pas être dans le formulaire lorsque vous utilisez les raccourcis clavier!</p>
                        </div>
                </fieldset>
                <div>
                    <button class="button" id="buttonForm" type="button">Soumettre</button>
                    <button class="button" id="buttonDel" type="button">Supprimer</button>
                </div>	
            <!-- </form> -->
            </div>
            <div id="erreur">

            </div>
        </aside>
        
        <div id="surface">
        <canvas tabindex="999" width="650" height="650" id="canvas">
				Veuillez mettre votre navigateur à jour afin d'utiliser le site!
		</canvas>
        </div>
<?php
	require_once("partiel/footer.php");