<?php
	require_once("action/ajax-carteAction.php");

	$action = new AjaxCarteAction();
	$action->execute();

	echo json_encode($action->result);