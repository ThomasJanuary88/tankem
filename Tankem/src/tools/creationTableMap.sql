--Validation de la forme normale
-- Nous respectons la 1ere forme normale car toutes nos tables sont atomiques, c'est-a-dire que les champs ne contiennent qu'une seule information.
-- Nous respectons la 2e forme normale car nous respectons la 1ere et que tous nos champs dependent directement d'une cle unique et non combinee donc d'une cle entiere.
-- Nous respectons la 3e forme normale car nous respectons la 2e et que tous nos champs non cles ne dependent pas d'un autre champ non cle.

-- *********************************************************************--
-- *                                                                   *--
-- *********************************************************************--
--      ***               PARTIE REALISEE PAR DANNY           ***       --
-- *********************************************************************--
-- *                                                                   *--
-- *********************************************************************--


SET SERVEROUTPUT ON;

-- *********************************************************************--
--                       SUPPRESSION DES TABLES                         --
-- *********************************************************************--

DROP TABLE map CASCADE CONSTRAINTS;
DROP TABLE statut CASCADE CONSTRAINTS;
DROP TABLE relation CASCADE CONSTRAINTS;
DROP TABLE tank CASCADE CONSTRAINTS;
DROP TABLE tuile CASCADE CONSTRAINTS;
DROP TABLE type CASCADE CONSTRAINTS;


-- *********************************************************************--
--                          CREATION DES TABLES                         --
-- *********************************************************************--

--  TABLE MAP

CREATE TABLE map
(
  id NUMBER GENERATED ALWAYS AS IDENTITY, 
  nom VARCHAR2(150) NOT NULL, 
  date_creation DATE NOT NULL,
  id_statut NUMBER(3) NOT NULL,
  taille_x NUMBER(3) NOT NULL,
  taille_y NUMBER(3) NOT NULL,
  id_tank1 NUMBER(3) NOT NULL,
  id_tank2 NUMBER(3) NOT NULL,
  temps_min NUMBER(3) NOT NULL,
  temps_max NUMBER(3) NOT NULL,
  CONSTRAINT pk_map PRIMARY KEY (id)
);

--  TABLE STATUT

CREATE TABLE statut
(
  id NUMBER GENERATED ALWAYS AS IDENTITY, 
  nom VARCHAR2(150) NOT NULL, 
  CONSTRAINT pk_statut PRIMARY KEY (id)
);

--  TABLE RELATION

CREATE TABLE relation
(
  id NUMBER GENERATED ALWAYS AS IDENTITY, 
  id_tuile NUMBER(3) NOT NULL,
  id_map NUMBER(3) NOT NULL,
  coord_x NUMBER(3) NOT NULL,
  coord_y NUMBER(3) NOT NULL,
  CONSTRAINT pk_relation PRIMARY KEY (id)
);

--  TABLE TANK

CREATE TABLE tank
(
  id NUMBER GENERATED ALWAYS AS IDENTITY, 
  coord_x NUMBER(3) NOT NULL,
  coord_y NUMBER(3) NOT NULL,
  CONSTRAINT pk_tank PRIMARY KEY (id)
);

--  TABLE TUILE

CREATE TABLE tuile
(
  id NUMBER GENERATED ALWAYS AS IDENTITY, 
  id_type NUMBER(3) NOT NULL,
  arbre CHAR NOT NULL,
  CONSTRAINT pk_tuile PRIMARY KEY (id)
);

--  TABLE TYPE

CREATE TABLE type
(
  id NUMBER GENERATED ALWAYS AS IDENTITY, 
  nom VARCHAR2(150) NOT NULL, 
  CONSTRAINT pk_type PRIMARY KEY (id)
);

-- AJOUT DES FOREIGN KEY
ALTER TABLE map ADD CONSTRAINT fk_map_statut foreign key (id_statut) REFERENCES statut(id);
ALTER TABLE map ADD CONSTRAINT fk_map_tank1 foreign key (id_tank1) REFERENCES tank(id);
ALTER TABLE map ADD CONSTRAINT fk_map_tank2 foreign key (id_tank2) REFERENCES tank(id);
ALTER TABLE tuile ADD CONSTRAINT fk_tuile_type foreign key (id_type) REFERENCES type(id);
ALTER TABLE relation ADD CONSTRAINT fk_relation_tuile foreign key (id_tuile) REFERENCES tuile(id);
ALTER TABLE relation ADD CONSTRAINT fk_relation_map foreign key (id_map) REFERENCES map(id);

-- AJOUT DES CHECKS
ALTER TABLE tuile ADD CONSTRAINT CHK_tuile_arbre CHECK (arbre='O' OR arbre='N');

-- *********************************************************************--
--                           CREATION DES INDEX                         --
-- *********************************************************************--

CREATE UNIQUE INDEX idx_map_nom ON map (lower(nom));


-- *********************************************************************--
-- *                                                                   *--
-- *********************************************************************--
--      ***            PARTIE REALISEE PAR CATHERINE          ***       --
-- *********************************************************************--
-- *                                                                   *--
-- *********************************************************************--


-- *********************************************************************--
--                 CREATION DES PROCEDURES D'INSERTION                  --
-- *********************************************************************--


-- Table Tank

CREATE OR REPLACE PROCEDURE setTank ( val_id IN tank.id%TYPE,
                                      val_coord_x IN tank.coord_x%TYPE,
                                      val_coord_y IN tank.coord_y%TYPE)
IS
BEGIN
  IF (val_id IS NOT NULL) 
  THEN -- UPDATE DU TANK EXISTANT
      UPDATE tank 
      SET coord_x = val_coord_x,
          coord_y = val_coord_y
      WHERE id = val_id; 
  ELSE -- AJOUT D'UN NOUVEAU TANK
      INSERT INTO tank( coord_x, coord_y ) 
      VALUES(val_coord_x, val_coord_y);
  END IF;
  COMMIT;
END;
/


-- Table Map

CREATE OR REPLACE PROCEDURE setMap (  val_id IN map.id%TYPE,
                                      val_nom IN map.nom%TYPE,
                                      val_date IN map.date_creation%TYPE,
                                      val_id_statut IN map.id_statut%TYPE,
                                      val_taille_x IN map.taille_x%TYPE,
                                      val_taille_y IN map.taille_y%TYPE,
                                      val_id_tank1 IN map.id_tank1%TYPE,
                                      val_coordx_tank1 IN tank.coord_x%TYPE,
                                      val_coordy_tank1 IN tank.coord_y%TYPE,
                                      val_id_tank2 IN map.id_tank2%TYPE,
                                      val_coordx_tank2 IN tank.coord_x%TYPE,
                                      val_coordy_tank2 IN tank.coord_y%TYPE,
                                      val_temps_min IN map.temps_min%TYPE,
                                      val_temps_max IN map.temps_max%TYPE)
IS
    id_tank1_new map.id_tank1%TYPE;
    id_tank2_new map.id_tank2%TYPE;
    
BEGIN
    -- Valider que les deux tnaks ne sont pas l'un sur l'autre;
    IF(val_coordx_tank1 = val_coordx_tank2 AND val_coordy_tank1 = val_coordy_tank2)
    THEN
        RAISE_APPLICATION_ERROR(-20400, 'Les deux tanks ne peuvent avoir les memes coordonnees');
    
    -- Valider que le tank 1 est dans les limites de la carte     
    ELSE IF(val_coordx_tank1 < 0 OR val_coordx_tank1 > val_taille_x OR val_coordy_tank1 < 0 OR val_coordy_tank1 > val_taille_y)
    THEN
        RAISE_APPLICATION_ERROR(-20500, 'Le tank 1 est a l''exterieur de la carte');  
    
    -- Valider que le tank 2 est dans les limites de la carte        
    ELSE IF(val_coordx_tank2 < 0 OR val_coordx_tank2 > val_taille_x OR val_coordy_tank2 < 0 OR val_coordy_tank2 > val_taille_y)
    THEN
        RAISE_APPLICATION_ERROR(-20500, 'Le tank 2 est a l''exterieur de la carte');
    
    -- Les positions des tanks sont valides    
    ELSE
        IF(val_id_tank1 IS NULL) -- Si le tank n'existe pas deja dans la BD
        THEN
            setTank(null, val_coordx_tank1, val_coordy_tank1); --Ajout du tank dans la table Tank via la procedure setTank
            BEGIN -- Recuperer le id du nouveau tank pour l'insertion dans la table Map
                SELECT MAX(id) INTO id_tank1_new 
                FROM Tank ;
            END;
        ELSE
            setTank(val_id_tank1, val_coordx_tank1, val_coordy_tank1); --Mise a jour du tank existant
            id_tank1_new := val_id_tank1;
        END IF;

        IF(val_id_tank2 IS NULL) -- Si le tank n'existe pas deja dans la BD
        THEN
            setTank(null, val_coordx_tank2, val_coordy_tank2); --Ajout du tank dans la table Tank via la procedure setTank
            BEGIN -- Recuperer le id du nouveau tank pour l'insertion dans la table Map
                SELECT MAX(id) INTO id_tank2_new 
                FROM Tank ;
            END;
        ELSE
            setTank(val_id_tank2, val_coordx_tank2, val_coordy_tank2); --Mise a jour du tank existant
            id_tank2_new := val_id_tank2;
        END IF;
    
        IF (val_id IS NOT NULL) -- Mise a jour de la carte existante
        THEN 
            UPDATE map
            SET nom = val_nom,
                date_creation = val_date,
                id_statut = val_id_statut,
                taille_x = val_taille_x,
                taille_y = val_taille_y,
                id_tank1 = id_tank1_new,
                id_tank2 = id_tank2_new,
                temps_min = val_temps_min,
                temps_max = val_temps_max
            WHERE id = val_id; 
        ELSE -- Ajout d'une nouvelle carte
            INSERT INTO map(  nom, date_creation, id_statut, taille_x, 
                              taille_y, id_tank1, id_tank2, temps_min, temps_max ) 
            VALUES( val_nom, val_date, val_id_statut, val_taille_x, val_taille_y, 
                    id_tank1_new, id_tank2_new, val_temps_min, val_temps_max);
        END IF;
    END IF;
    END IF;
    END IF;
END;
/


-- Table Statut

CREATE OR REPLACE PROCEDURE setStatut ( val_id IN type.id%TYPE,
                                        val_nom IN type.nom%TYPE)
IS
BEGIN
  IF (val_id IS NOT NULL) 
  THEN -- UPDATE DU TYPE EXISTANT
      UPDATE statut
      SET nom = val_nom
      WHERE id = val_id; 
  ELSE -- AJOUT D'UN NOUVEAU TYPE
      INSERT INTO statut( nom ) 
      VALUES(val_nom);
  END IF;
  COMMIT;
END;
/


-- Table Relation

CREATE OR REPLACE PROCEDURE setRelation ( val_id IN relation.id%TYPE,
                                          val_id_tuile IN relation.id_tuile%TYPE,
                                          val_id_map IN relation.id_map%TYPE,
                                          val_coord_x IN relation.coord_x%TYPE,
                                          val_coord_y IN relation.coord_y%TYPE)
IS
    coord_x_tank1 tank.coord_x%TYPE;
    coord_y_tank1 tank.coord_y%TYPE;
    coord_x_tank2 tank.coord_x%TYPE;
    coord_y_tank2 tank.coord_y%TYPE;
    
BEGIN
    BEGIN
        SELECT coord_x, coord_y INTO coord_x_tank1, coord_y_tank1
        FROM tank
        WHERE id = (SELECT id_tank1 FROM map WHERE id = val_id_map);
    END;
    
    BEGIN
        SELECT coord_x, coord_y INTO coord_x_tank2, coord_y_tank2
        FROM tank
        WHERE id = (SELECT id_tank2 FROM map WHERE id = val_id_map);
    END; 
    
    IF(coord_x_tank1 = val_coord_x AND coord_y_tank1 = val_coord_y AND val_id_tuile IN(1, 3, 5, 7))
    THEN
        RAISE_APPLICATION_ERROR(-20800, 'Conflit entre l''arbre sur la tuile et le tank 1');
    END IF;   
    
    IF(coord_x_tank2 = val_coord_x AND coord_y_tank2 = val_coord_y AND val_id_tuile IN(1, 3, 5, 7))
    THEN
        RAISE_APPLICATION_ERROR(-20900, 'Conflit entre l''arbre sur la tuile et le tank 2');    
    END IF;  
    
    IF (val_id IS NOT NULL) 
    THEN -- UPDATE DE LA RELATION EXISTANTE
      UPDATE relation 
      SET id_tuile = val_id_tuile, 
          id_map = val_id_map,
          coord_x = val_coord_x,
          coord_y = val_coord_y
      WHERE id = val_id; 
    ELSE -- AJOUT DUNE NOUVELLE RELATION
      INSERT INTO relation( id_tuile, id_map, coord_x, coord_y) 
      VALUES(val_id_tuile, val_id_map, val_coord_x, val_coord_y);
    END IF;
    
    COMMIT;
END;
/


-- Table Tuile

CREATE OR REPLACE PROCEDURE setTuile (  val_id IN tuile.id%TYPE,
                                        val_id_type IN tuile.id_type%TYPE,
                                        val_arbre IN tuile.arbre%TYPE)
IS
    nom_type type.nom%TYPE;
    
BEGIN
    BEGIN
        SELECT nom INTO nom_type
        FROM type
        WHERE id = val_id_type;
    END;
    IF (nom_type != 'vide' OR (nom_type = 'vide' AND val_arbre = 'O'))
    THEN   
        IF (val_id IS NOT NULL) 
        THEN -- UPDATE DE LA TUILE EXISTANTE
            UPDATE tuile 
            SET id_type = val_id_type,
                arbre = val_arbre
            WHERE id = val_id; 
        ELSE -- AJOUT D'UNE NOUVELLE TUILE
            INSERT INTO tuile( id_type, arbre ) 
            VALUES(val_id_type, val_arbre);
        END IF;
        COMMIT;
    END IF;
END;
/


-- Table Type

CREATE OR REPLACE PROCEDURE setType ( val_id IN type.id%TYPE,
                                      val_nom IN type.nom%TYPE)
IS
BEGIN
  IF (val_id IS NOT NULL) 
  THEN -- UPDATE DU TYPE EXISTANT
      UPDATE type
      SET nom = val_nom
      WHERE id = val_id; 
  ELSE -- AJOUT D'UN NOUVEAU TYPE
      INSERT INTO type( nom ) 
      VALUES(val_nom);
  END IF;
  COMMIT;
END;
/


-- *********************************************************************--
--                        CREATION DES TRIGGERS                         --
-- *********************************************************************--

-- Table Relation

CREATE OR REPLACE TRIGGER valid_coord_relation
BEFORE INSERT OR UPDATE ON relation
FOR EACH ROW
DECLARE
    map_size_x map.taille_x%TYPE;
    map_size_y map.taille_y%TYPE;
BEGIN
    BEGIN
        SELECT taille_x, taille_y INTO map_size_x, map_size_y
        FROM map
        WHERE id = :new.id_map;
    END;
    
    IF :new.coord_x < 0 OR :new.coord_x > map_size_x OR :new.coord_y < 0 OR :new.coord_y > map_size_y
    THEN
        RAISE_APPLICATION_ERROR(-20300, 'Les coordonnees de la relation sortent les limites de la carte');
    ELSE
        DBMS_OUTPUT.PUT_LINE('OK');
    END IF;
END;
/


-- *********************************************************************--
--                              INSERTIONS                              --
-- *********************************************************************--

EXECUTE setStatut(null, 'Actif');
EXECUTE setStatut(null, 'Test');
EXECUTE setStatut(null, 'Inactif');

EXECUTE setType(null, 'mur');
EXECUTE setType(null, 'mobile');
EXECUTE setType(null, 'inverse');
EXECUTE setType(null, 'vide');

EXECUTE setTuile(null, 1,'O');
EXECUTE setTuile(null, 1,'N');
EXECUTE setTuile(null, 2,'O');
EXECUTE setTuile(null, 2,'N');
EXECUTE setTuile(null, 3,'O');
EXECUTE setTuile(null, 3,'N');
EXECUTE setTuile(null, 4,'O');

Insert into TANK (COORD_X,COORD_Y) values ('0','0');
Insert into TANK (COORD_X,COORD_Y) values ('10','10');
Insert into TANK (COORD_X,COORD_Y) values ('1','1');
Insert into TANK (COORD_X,COORD_Y) values ('5','5');
Insert into TANK (COORD_X,COORD_Y) values ('1','6');
Insert into TANK (COORD_X,COORD_Y) values ('10','6');

Insert into MAP (NOM,DATE_CREATION,ID_STATUT,TAILLE_X,TAILLE_Y,ID_TANK1,ID_TANK2,TEMPS_MIN,TEMPS_MAX) values ('Aleatoire',to_date('18-04-17','RR-MM-DD'),'1','11','11','1','2','3','20');
Insert into MAP (NOM,DATE_CREATION,ID_STATUT,TAILLE_X,TAILLE_Y,ID_TANK1,ID_TANK2,TEMPS_MIN,TEMPS_MAX) values ('UneMap',to_date('18-04-17','RR-MM-DD'),'1','11','11','1','2','3','20');
Insert into MAP (NOM,DATE_CREATION,ID_STATUT,TAILLE_X,TAILLE_Y,ID_TANK1,ID_TANK2,TEMPS_MIN,TEMPS_MAX) values ('UneDeuxiemeMap',to_date('18-04-17','RR-MM-DD'),'1','7','7','3','4','3','20');
Insert into MAP (NOM,DATE_CREATION,ID_STATUT,TAILLE_X,TAILLE_Y,ID_TANK1,ID_TANK2,TEMPS_MIN,TEMPS_MAX) values ('UneTroisiemeMap',to_date('18-04-17','RR-MM-DD'),'1','12','12','5','6','3','20');



Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('4','1','0','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','1','1','2');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('6','2','1','4');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('4','2','1','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('6','2','1','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','2','1','8');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('4','2','2','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('6','2','3','4');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('4','2','3','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('6','2','3','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('4','2','4','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','2','5','1');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','2','5','2');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','2','5','8');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','2','5','9');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('6','2','6','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('4','2','7','4');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('6','2','7','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('4','2','7','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('6','2','8','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','2','9','2');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('4','2','9','4');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('6','2','9','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('4','2','9','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','2','9','8');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('6','2','10','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','0','0');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','0','1');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','0','2');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','0','3');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','0','4');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','0','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','0','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','1','0');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','1','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','2','0');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','2','2');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','2','3');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','2','4');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','2','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','3','0');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','3','2');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','3','3','3');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','3','4');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','3','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','4','0');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','4','2');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','4','3');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','4','4');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','4','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','5','0');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','5','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','6','0');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','6','1');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','6','2');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','6','3');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','6','4');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','6','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','3','6','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','0','3');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','0','9');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','1','3');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','1','9');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','2','0');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','2','1');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','2','2');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','2','3');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('4','4','2','4');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('4','4','2','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','2','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('6','4','2','7');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('6','4','2','8');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','2','9');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','2','10');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','2','11');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','3','0');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','3','1');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','3','2');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','3','3');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','3','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','3','9');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','3','10');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','3','11');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','8','0');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','8','1');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','8','2');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','8','3');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','8','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','8','9');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','8','10');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','8','11');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','9','0');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','9','1');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','9','2');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','9','3');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('4','4','9','4');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('4','4','9','5');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','9','6');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('6','4','9','7');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('6','4','9','8');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','9','9');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','9','10');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('2','4','9','11');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','10','3');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','10','9');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','11','3');
Insert into RELATION (ID_TUILE,ID_MAP,COORD_X,COORD_Y) values ('7','4','11','9');

COMMIT;