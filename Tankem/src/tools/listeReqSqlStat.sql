--req 1-2 Retourne la liste des armes avec le nb de tirs et le succes
SELECT arme.nom,sum(nb_tirs) tirs,sum(nb_tirs_succes) tirs_succes FROM JoueurArmePartie j INNER JOIN arme ON j.id_arme = arme.id WHERE id_joueur = 1 GROUP BY arme.nom;

--req 3 Retourne le gagnant de toutes les parties que le joueur a participe
SELECT id_gagnant FROM partie WHERE id_joueur1 = 1 OR id_joueur2 = 1;

--req 4 Retourne la date de toutes les parties que le joueur a participe
SELECT to_char(date_debut,'YYYY/MM/DD HH24:MI:SS') dtHeure FROM partie WHERE id_joueur1 = 1 OR id_joueur2 = 1;