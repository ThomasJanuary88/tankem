--Validation de la forme normale
-- Nous respectons la 1ere forme normale car toutes nos tables sont atomiques, c'est-a-dire que les champs ne contiennent qu'une seule information.
-- Nous respectons la 2e forme normale car nous respectons la 1ere et que tous nos champs dependent directement d'une cle unique et non combinee donc d'une cle entiere.
-- Nous respectons la 3e forme normale car nous respectons la 2e et que tous nos champs non cles ne dependent pas d'un autre champ non cle.

SET SERVEROUTPUT ON;

-- *********************************************************************--
--                       SUPPRESSION DES TABLES                         --
-- *********************************************************************--

DROP TABLE partie CASCADE CONSTRAINTS;
DROP TABLE joueur CASCADE CONSTRAINTS;
DROP TABLE arme CASCADE CONSTRAINTS;
DROP TABLE JoueurArmePartie CASCADE CONSTRAINTS;


-- *********************************************************************--
--                          CREATION DES TABLES                         --
-- *********************************************************************--

--  TABLE STATISTIQUES

CREATE TABLE partie
(
  id NUMBER GENERATED ALWAYS AS IDENTITY, 
  date_debut DATE NOT NULL,
  date_fin DATE NOT NULL,
  id_joueur1 NUMBER(3) NOT NULL,
  id_joueur2 NUMBER(3) NOT NULL,
  id_gagnant NUMBER(3),
  dist_joueur1 NUMBER(5) NOT NULL,
  dist_joueur2 NUMBER(5) NOT NULL,
  id_map NUMBER(3) NOT NULL,
  CONSTRAINT pk_partie PRIMARY KEY (id)
);


--  TABLE JOUEUR

CREATE TABLE joueur
(
  id NUMBER GENERATED ALWAYS AS IDENTITY, 
  couleur_R NUMBER(3),
  couleur_G NUMBER(3),
  couleur_B NUMBER(3),
  message VARCHAR2(4000),
  experience NUMBER(10),
  vie NUMBER(10),
  force NUMBER(10),
  agilite NUMBER(10),
  dexterite NUMBER(10),
  nom_usager VARCHAR2(150),
  mot_de_passe VARCHAR2(150),
  CONSTRAINT pk_joueur PRIMARY KEY (id)
);


--  TABLE ARME

CREATE TABLE arme
(
    id NUMBER GENERATED ALWAYS AS IDENTITY,     
    nom VARCHAR2(100) NOT NULL UNIQUE,
    CONSTRAINT pk_arme PRIMARY KEY (id)
);


--  TABLE JoueurArmePartie

CREATE TABLE JoueurArmePartie
(
    id_joueur NUMBER(3) NOT NULL,
    id_arme NUMBER(3) NOT NULL,    
    id_partie NUMBER(3) NOT NULL,
    nb_tirs NUMBER(3) NOT NULL,
    nb_tirs_succes NUMBER(3) NOT NULL,
    CONSTRAINT pk_joueur_arme_partie PRIMARY KEY (id_joueur, id_arme, id_partie)
);


-- *********************************************************************--
--                    AJOUT DES CLES ETRANGERES                         --
-- *********************************************************************--

ALTER TABLE partie ADD CONSTRAINT fk_id_joueur1 FOREIGN KEY (id_joueur1) REFERENCES joueur(id);
ALTER TABLE partie ADD CONSTRAINT fk_id_joueur2 FOREIGN KEY (id_joueur2) REFERENCES joueur(id);
ALTER TABLE partie ADD CONSTRAINT fk_id_gagnant FOREIGN KEY (id_gagnant) REFERENCES joueur(id);
ALTER TABLE partie ADD CONSTRAINT fk_id_map FOREIGN KEY (id_map) REFERENCES map(id);

ALTER TABLE JoueurArmePartie ADD CONSTRAINT fk_id_joueur FOREIGN KEY (id_joueur) REFERENCES joueur(id);
ALTER TABLE JoueurArmePartie ADD CONSTRAINT fk_id_arme FOREIGN KEY (id_arme) REFERENCES arme(id);
ALTER TABLE JoueurArmePartie ADD CONSTRAINT fk_id_partie FOREIGN KEY (id_partie) REFERENCES partie(id);


-- *********************************************************************--
--                           CREATION DES INDEX                         --
-- *********************************************************************--

CREATE UNIQUE INDEX idx_joueur_nom_usager ON joueur (lower(nom_usager));

-- *********************************************************************--
--                           CREATION DES PROCEDURES                      --
-- *********************************************************************--

-- Table Joueur
CREATE OR REPLACE PROCEDURE setJoueur ( val_id IN joueur.id%TYPE,
                                        val_couleur_R IN joueur.couleur_R%TYPE,
                                        val_couleur_G IN joueur.couleur_G%TYPE,
                                        val_couleur_B IN joueur.couleur_B%TYPE,
                                        val_message IN joueur.message%TYPE,
                                        val_experience IN joueur.experience%TYPE,
                                        val_vie IN joueur.vie%TYPE,
                                        val_force IN joueur.force%TYPE,
                                        val_agilite IN joueur.agilite%TYPE,
                                        val_dexterite IN joueur.dexterite%TYPE,
                                        val_nom_usager IN joueur.nom_usager%TYPE,
                                        val_mdp IN joueur.mot_de_passe%TYPE)
IS
BEGIN
  IF (val_id IS NOT NULL) 
  THEN -- UPDATE DU JOUEUR EXISTANT
      UPDATE joueur
      SET nom_usager = val_nom_usager,
          mot_de_passe = val_mdp,
          dexterite = val_dexterite,
          agilite = val_agilite,
          force = val_force,
          vie = val_vie,
          experience = val_experience,
          message = val_message,
          couleur_B = val_couleur_B,
          couleur_G = val_couleur_G,
          couleur_R = val_couleur_R
      WHERE id = val_id; 
  ELSE -- AJOUT D'UN NOUVEAU JOUEUR
      addJoueur(val_id, val_couleur_R, val_couleur_G, val_couleur_B, val_message, val_experience, val_vie, val_force, val_agilite, val_dexterite, val_nom_usager, val_mdp);
  END IF;
  COMMIT;
END;
/

CREATE OR REPLACE PROCEDURE addJoueur ( val_id IN joueur.id%TYPE,
                                        val_couleur_R IN joueur.couleur_R%TYPE,
                                        val_couleur_G IN joueur.couleur_G%TYPE,
                                        val_couleur_B IN joueur.couleur_B%TYPE,
                                        val_message IN joueur.message%TYPE,
                                        val_experience IN joueur.experience%TYPE,
                                        val_vie IN joueur.vie%TYPE,
                                        val_force IN joueur.force%TYPE,
                                        val_agilite IN joueur.agilite%TYPE,
                                        val_dexterite IN joueur.dexterite%TYPE,
                                        val_nom_usager IN joueur.nom_usager%TYPE,
                                        val_mdp IN joueur.mot_de_passe%TYPE)
IS
BEGIN
      INSERT INTO joueur( nom_usager, mot_de_passe, couleur_r,couleur_g, couleur_b, message, experience, vie, force, agilite, dexterite) 
      VALUES(val_nom_usager, val_mdp, val_couleur_R,val_couleur_G,val_couleur_B, val_message, val_experience, val_vie, val_force, val_agilite, val_dexterite);

  COMMIT;
END;
/

-- Table Arme
-- Cette proc�dure permet seulement l'insertion de donn�es dans la table Arme.
CREATE OR REPLACE PROCEDURE addArme ( val_id IN arme.id%TYPE,
                                    val_nom IN arme.nom%TYPE)

IS
BEGIN
      INSERT INTO arme( nom) 
      VALUES(val_nom);

  COMMIT;
END;
/

-- Table JoueurArmePartie
-- Cette proc�dure permet seulement l'insertion de donn�es dans la table JoueurArmePartie.
CREATE OR REPLACE PROCEDURE setJoueurArmePartie ( 
                                    val_id_joueur IN JoueurArmePartie.id_joueur%TYPE,
                                    val_id_arme IN JoueurArmePartie.id_arme%TYPE,
                                    val_id_partie IN JoueurArmePartie.id_partie%TYPE,
                                    val_nb_tirs IN JoueurArmePartie.nb_tirs%TYPE,
                                    val_tirs_succes IN JoueurArmePartie.nb_tirs_succes%TYPE)

IS
BEGIN
      INSERT INTO JoueurArmePartie( id_joueur, id_arme, id_partie, nb_tirs, nb_tirs_succes) 
      VALUES(val_id_joueur, val_id_arme, val_id_partie, val_nb_tirs, val_tirs_succes);

  COMMIT;
END;
/

-- Table Partie
-- Cette proc�dure permet seulement l'insertion de nouvelles parties.
CREATE OR REPLACE PROCEDURE addPartie ( val_id IN Partie.id%TYPE,
                                        val_date_debut IN Partie.date_debut%TYPE,
                                        val_date_fin IN Partie.date_fin%TYPE,
                                        val_id_map IN Partie.id_map%TYPE,
                                        val_id_joueur1 IN Partie.id_joueur1%TYPE,
                                        val_id_joueur2 IN Partie.id_joueur2%TYPE,
                                        val_id_gagnant IN Partie.id_gagnant%TYPE,
                                        val_dist_joueur1 IN Partie.dist_joueur1%TYPE,
                                        val_dist_joueur2 IN Partie.dist_joueur2%TYPE)

IS
BEGIN
      INSERT INTO Partie( date_debut, date_fin, id_map, id_joueur1, id_joueur2, id_gagnant, dist_joueur1, dist_joueur2) 
      VALUES(val_date_debut, val_date_fin, val_id_map, val_id_joueur1, val_id_joueur2, val_id_gagnant, val_dist_joueur1, val_dist_joueur2);

  COMMIT;
END;
/
