## -*- coding: utf-8 -*-

from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
import sys
from interfaceChoixCartes import *
from common.internal.gestionDAO import GestionDAO
from common.internal.gestionDTOJoueur import GestionDTOJoueur
from direct.actor.Actor import Actor
from direct.interval.IntervalGlobal import *
import tankPrevisualisation
from interface import *
from common.AnalyseFinPartie import AnalyseFinPartie 

class InterfaceFinPartie(ShowBase):
    
    def __init__(self):
        self.accept("gameOver", self.displayGameOver)
        self.logique = AnalyseFinPartie()

    def displayGameOver(self, args1, args2):
        self.resultatXp = self.gestionBarreExp(args1, args2)

        self.barreXp1 = DirectWaitBar(barColor = Vec4(args1.rouge, args1.vert, args1.bleu, 1),
                                        text = "",
                                        value = 100,
                                        pos = Vec3(0.68,0,0.25),
                                        scale=0.4)

        self.barreXp2 = DirectWaitBar(barColor = Vec4(args2.rouge, args2.vert, args2.bleu, 1),
                                        text = "",
                                        value = 100,
                                        pos = Vec3(-0.68,0,0.25),
                                        scale=0.4)

        self.phraseDuGagnant = OnscreenText(text = args2.joueur.nom_usager,
                                        pos = (0,-0.42,0), 
                                        scale = 0.25)

        self.phraseDuGagnant.setText(args1.joueur.message if args1.pointDeVie != 0 else args2.joueur.message)

        if args1.pointDeVie == 0:
            self.nomJoueur1 = OnscreenText(text = args1.joueur.nom_usager,
                                            pos = (0.68,0.8,0), 
                                            scale = 0.15)
            
            OnscreenText(text ="Défaite",
                            pos = (0.68,0.6,0), 
                            scale = 0.15)

            self.nomJoueur2 = OnscreenText(text = args2.joueur.nom_usager,
                                            pos = (-0.68,0.8,0), 
                                            scale = 0.15)

            OnscreenText(text ="Victoire",
                            pos = (-0.68,0.6,0), 
                            scale = 0.15)

        elif args2.pointDeVie == 0 :
            self.nomJoueur1 = OnscreenText(text = args1.joueur.nom_usager,
                                            pos = (0.68,0.8,0), 
                                            scale = 0.15)

            OnscreenText(text ="Victoire",
                            pos = (0.68,0.6,0), 
                            scale = 0.15)

            self.nomJoueur2 = OnscreenText(text = args2.joueur.nom_usager,
                                            pos = (-0.68,0.8,0), 
                                            scale = 0.15)

            OnscreenText(text ="Défaite",
                            pos = (-0.68,0.6,0), 
                            scale = 0.15)

        OnscreenText(text ="Exp gagné: " + str(int(self.logique.expGagne[0])) ,
                            pos = (0.68,0.4,0), 
                            scale = 0.15)

        OnscreenText(text ="Exp gagné: " + str(int(self.logique.expGagne[1])),
                            pos = (-0.68,0.4,0), 
                            scale = 0.15)

        self.barreXp1.accept("effetXp1", lambda : self.effetXp1(self.resultatXp[0]))
        self.barreXp2.accept("effetXp2", lambda : self.effetXp2(self.resultatXp[1]))

        messenger.send("effetXp1")  
        messenger.send("effetXp2")

        self.phraseDuGagnant.posInterval(8, (-2.3,0.8,0), (2.3,0.8,0)).loop()

        

        self.messageLevelUp = OnscreenText(text ="Level up",
                                        pos = (2, 2), 
                                        scale = 0.1)

        

    def changeLevelUpOpacity(self, value):
        self.messageLevelUp.setFg(value)

    def effetXp1(self, value):
        effetRecharge = LerpFunc(self.changerValeurCharge1,fromData=0,toData=value, duration=5)
        effetRecharge.start()

    def effetXp2(self, value):
        effetRecharge = LerpFunc(self.changerValeurCharge2,fromData=0,toData=value, duration=5)
        effetRecharge.start()

    def levelUp(self, numJoueur):
        if numJoueur == 1:
            self.messageLevelUp.setPos(1.3, 0.23)
            self.messageLevelUp.setFg((0, 0, 0, 1))

            LerpFunc(self.changeLevelUpOpacity,
                        fromData=Vec4(0, 0, 0, 1),
                        toData=Vec4(0, 0, 0, 0),
                        duration=4,
                        blendType='noBlend',
                        extraArgs=[],
                        name=None).start()

            self.messageLevelUp.posInterval(8, (1.3,0.23, 1), (1.3,0.23,0)).start()

        elif numJoueur == 2:              
            self.messageLevelUp.setPos(-0.02,0.23)
            self.messageLevelUp.setFg((0, 0, 0, 1))

            LerpFunc(self.changeLevelUpOpacity,
                        fromData=Vec4(0, 0, 0, 1),
                        toData=Vec4(0, 0, 0, 0),
                        duration=4,
                        blendType='noBlend',
                        extraArgs=[],
                        name=None).start()

        self.messageLevelUp.posInterval(8, (-0.02,0.23, 1), (-0.02,0.23,0)).start()

    def changerValeurCharge1(self, nouvelleValeur):
		self.barreXp1['value'] = nouvelleValeur
		if nouvelleValeur >= 100:
		    nouvelleValeur -= 100
		    self.effetXp1(nouvelleValeur)
		    self.levelUp(1)

    def changerValeurCharge2(self,nouvelleValeur):
		self.barreXp2['value'] = nouvelleValeur
		if nouvelleValeur >= 100:
		    nouvelleValeur -= 100
		    self.effetXp2(nouvelleValeur)
		    self.levelUp(2)

    def gestionBarreExp(self, tank1, tank2):
        return self.logique.gestionExpJoueur(tank1, tank2)

    #     btnScale = (0.15,0.15)
    #     text_scale = 0.12
    #     borderW = (0.01, 0.01)
    #     couleurBack = (0.243,0.325,0.121,1)
    #     self.boutonRejouer = DirectButton(text = ("Combattre", "GO", "Prêt ?", "disabled"),
    #                     text_scale=btnScale,
    #                     borderWidth = borderW,
    #                     text_bg=couleurBack,
    #                     frameColor=couleurBack,
    #                     relief=2,
    #                     #########################################
    #                     command=self.rejouer,
    #                     #########################################
    #                     pos = (0,27,-0.6))

    # def rejouer(self):
    #     InterfaceChoixCartes()