from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions

class TankPrevisualisation():
    def __init__(self, numeroJoueur, couleur, interface):
        self.numeroJoueur = numeroJoueur
        self.tank = loader.loadModel("../asset/Tank/tank")
        self.tank.setColorScale(couleur.getX(),couleur.getY(),couleur.getZ(),1)
        self.duree = 4
        self.tank.setScale(0.2, 0.2, 0.2)

        if (self.numeroJoueur == 1):
            self.tank.reparentTo(interface[0]) 
            self.posInitiale = Point3(-1.7, 0, 0.25)
            self.angleInitiale = Point3(-90,0,0)
            self.rotation = Point3(268,0,0)
            self.posArrive = Point3(0.1, 0, 0.25)
            self.tank.setHpr(-90, 0,0)

        elif (self.numeroJoueur == 2) :
            self.tank.reparentTo(interface[1]) 
            self.posInitiale = Point3(1.7,0, 0.25)
            self.angleInitiale = Point3(90, 0, 0)
            self.rotation = Point3(-268,0,0)
            self.posArrive = Point3(-0.1,0, 0.25)
            self.tank.setHpr(90, 0,0)


        self.deplacement = self.tank.posInterval(self.duree, self.posArrive, self.posInitiale)
        self.tourner = self.tank.hprInterval(self.duree, self.rotation, self.angleInitiale)

    def seDeplacer(self):
        Sequence(self.deplacement).start()

    def seTourner(self):
        self.tourner.loop()

    def destroy(self):
        self.tank.removeNode()
    
    def animer(self):
        self.animation = Sequence(self.deplacement, Func(self.seTourner))
        self.animation.start()

    def dernierTours(self):
        self.animation.finish()
        self.deplacement.finish()
        oldHpr = self.tank.getHpr()
        self.tourner.finish()

        faireUnDernierTour = self.tank.hprInterval(3, self.rotation, oldHpr)

        Sequence(faireUnDernierTour, Wait(1),Func(self.destroy)).start()