## -*- coding: utf-8 -*-

from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
import sys
from common.internal.gestionDAO import GestionDAO
from interfacePrevisualisation import *

class InterfaceChoixCartes(ShowBase):
    def __init__(self):
        #liste tempo à remplacer par liste du DTO
        self.index = 0
        gestionDAO = GestionDAO()
        dicoMap = gestionDAO.recupererMap()
        self.mapsInfo = []
        self.mapsInfo.append((u'Carte aléatoire', 0))
        for key in dicoMap:
            if key != 'Aleatoire':
                if (dicoMap[key][2] == 'Actif'):
                    info = (key, dicoMap[key][0])
                    self.mapsInfo.append(info)
        self.mapsInfo = sorted(self.mapsInfo, key=lambda x: x[0])

        self.gestionDtoJoueur = GestionDTOJoueur()
        
        #Initialisation de l'effet de transition
        curtain = loader.loadTexture("../asset/Menu/loading.jpg")

        self.transition = Transitions(loader)
        self.transition.setFadeColor(0, 0, 0)
        self.transition.setFadeModel(curtain)


        self.sound = loader.loadSfx("../asset/Menu/demarrage.mp3")

        #Image d'arrière plan
        self.background=OnscreenImage(parent=render2d, image="../asset/Menu/menuMaps.jpg")

        #On dit à la caméra que le dernier modèle doit s'afficher toujours en arrière
        self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
        base.cam.node().getDisplayRegion(0).setSort(20)

        #Titre du jeu
        self.textTitre = OnscreenText(text = "Choisissez une carte existante ou aléatoire",
                                      pos = (0,0.8), 
                                      scale = 0.15,
                                      fg=(0,0,0,1),
                                      align=TextNode.ACenter)
        #Params boutons
        btnScale = (0.18,0.18)
        text_scale = 0.12
        borderW = (0.04, 0.04)
        couleurBack = (0.243,0.325,0.121,1)
        separation = 0.5
        hauteur = -0.8

        #Ajout menu déroulant
        self.controlTextScale = 0.10
        self.controlBorderWidth = (0.005, 0.005)

        self.scrollItemButtons = self.createAllItems(self.mapsInfo)

        verticalOffsetControlButton = 0.225
        verticalOffsetCenterControlButton = -0.02
        self.myScrolledListLabel = DirectScrolledList(
                decButton_pos = (0.0, 0.0, verticalOffsetControlButton + verticalOffsetCenterControlButton),
                decButton_text = "Monter",
                #decButton_text_scale = 0.08,
                decButton_text_scale = text_scale,
                decButton_borderWidth = (0.0025, 0.0025),
                decButton_frameSize = (-0.35, 0.35, -0.0375, 0.075),
                #decButton_text_fg = (0.15, 0.15, 0.75, 1.0),
                decButton_text_bg=couleurBack,
                decButton_frameColor=couleurBack,

                incButton_pos = (0.0, 0.0, -0.625 - verticalOffsetControlButton + verticalOffsetCenterControlButton),
                incButton_text = "Descendre",
                incButton_text_scale = text_scale,
                incButton_borderWidth = (0.0025, 0.0025),
                incButton_frameSize = (-0.35, 0.35, -0.0375, 0.075),
                incButton_text_bg=couleurBack,
                incButton_frameColor=couleurBack,
                #incButton_text_fg = (0.15, 0.15, 0.75, 1.0),

                pos = (0, 0, 0.4),

                items = self.scrollItemButtons,
                numItemsVisible = 5,
                forceHeight = 0.175,
                
                frameSize = (-1.05, 1.05, -0.95, 0.325),
                frameColor = (0.243,0.325,0.121, 0.75),

                itemFrame_pos = (0.0, 0.0, 0.0),
                itemFrame_frameSize = (-1.025, 1.025, -0.775, 0.15),
                itemFrame_frameColor = (0.243,0.325,0.121, 0.75),
                itemFrame_relief = 1
            )

        self.b2 = DirectButton(text = ("Quitter", "Bye!", ":-(", "disabled"),
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          command = lambda : sys.exit(),
                          pos = (0,0,hauteur))


        self.accept("arrow_down",self.selectIndex, [1])
        self.accept("arrow_up",self.selectIndex, [-1])
        self.accept("enter",self.chargeInterfacePrevisualisation)

    def selectIndex(self,dir):
        couleurBackSelect = (0.529, 0.462, 1, 1)
        couleurBackBase = (0.529, 0.462, 0.439, 1)
        self.scrollItemButtons[self.index]["frameColor"]= couleurBackBase
        self.scrollItemButtons[self.index]["text_bg"]= couleurBackBase
        self.index = self.clamp(self.index + dir, 0, len(self.mapsInfo)-1)
        self.scrollItemButtons[self.index]["frameColor"]= couleurBackSelect
        self.scrollItemButtons[self.index]["text_bg"]= couleurBackSelect

        self.myScrolledListLabel.scrollTo(self.index,False)

    def clamp(self,value,min,max):
        if value < min:
            return min
        elif value > max:
            return max
        return value

    def createItemButton(self, mapName, mapId, selected = False):
        if selected:
            couleurBack = (0.529, 0.462, 1, 1)
        else:
            couleurBack = (0.529, 0.462, 0.439, 1)
        return DirectButton(
                text = mapName,
                text_bg=couleurBack,
                frameColor=couleurBack,
                text_scale = self.controlTextScale, 
                borderWidth = self.controlBorderWidth, 
                relief = 2,
                frameSize = (-1.0, 1.0, -0.0625, 0.105),
                command = lambda: self.chargeInterfacePrevisualisation)

    def createAllItems(self, mapsInfo):
        index = 0
        scrollItemButtons = []
        for mapInfo in mapsInfo:
            scrollItemButtons.append(self.createItemButton(self.formatText(mapInfo[0]), mapInfo[1], index == self.index)) 
            index +=1
        return scrollItemButtons

    def formatText(self, text, maxLength = 20):

        return text if len(text) <= maxLength else text[0:maxLength] + '...'

    def cacher(self):
            #Est esssentiellement un code de "loading"

            #On remet la caméra comme avant
            base.cam.node().getDisplayRegion(0).setSort(self.baseSort)
            #On cache les menus
            self.myScrolledListLabel.hide()
            self.background.hide()
            #self.b1.hide()
            self.b2.hide()
            self.textTitre.hide()



    def chargeInterfacePrevisualisation(self):
        self.cacher()
        if hasattr(self.gestionDtoJoueur.dao, 'cursor'):
            InterfacePrevisualisation(self.mapsInfo[self.index][0], self.mapsInfo[self.index][1])
        else:
            Sequence(Func(lambda : self.transition.irisOut(0.2)),
                    SoundInterval(self.sound),
                    Func(self.cacher),
                    Func(lambda : messenger.send("DemarrerPartie", [0, None, None])),
                    Wait(0.2),
                    Func(lambda : self.transition.irisIn(0.2))).start()