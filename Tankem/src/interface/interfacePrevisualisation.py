## -*- coding: utf-8 -*-

from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
import sys
from interfaceChoixCartes import *
from common.internal.gestionDAO import GestionDAO
from common.internal.gestionDTOJoueur import GestionDTOJoueur
from direct.actor.Actor import Actor
from direct.interval.IntervalGlobal import *
from tankPrevisualisation import *

class InterfacePrevisualisation(ShowBase):
    def __init__(self, nomMap, idMap):
        self.gestionnaire = GestionnairePrevisualisation()
        self.tankPrevisualisation = ["", ""]
        self.playerAreConnected = False
        self.nomMap = nomMap
        self.idMap = idMap
        self.listJoueurs = [[None, None],[None, None]]
        base.disableMouse()    
        
        #Initialisation de l'effet de transition
        curtain = loader.loadTexture("../asset/Menu/loading.jpg")

        self.transition = Transitions(loader)
        self.transition.setFadeColor(0, 0, 0)
        self.transition.setFadeModel(curtain)

        #On dit à la caméra que le dernier modèle doit s'afficher toujours en arrière
        self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
        base.cam.node().getDisplayRegion(0).setSort(20)

        self.sound = loader.loadSfx("../asset/Menu/demarrage.mp3")

        self.joueur1 = DirectLabel(text = "Joueur1",
                                    scale = 0.05,
                                    pos =(-0.55, 0, 0.9),
                                    frameColor=(0, 0, 0, 0))

        self.entryJoueur1 = DirectEntry(text = "",
                                        scale =(0.04, 0, 0.04),
                                        pos =(-0.42, 0, 0.9),
                                        command=None,
                                        initialText="nom1",
                                        numLines = 1,
                                        focus = 1,
                                        focusInCommand=lambda :self.clearText(self.entryJoueur1))

        self.bg = OnscreenImage(parent=render2d, image="../asset/Menu/menuTank.jpg")
        
        self.joueur2 = DirectLabel(text = "Joueur2",
                                    scale = 0.05,
                                    pos =(0.27, 0, 0.9),
                                    frameColor=(0, 0, 0, 0))

                                      
        self.entryJoueur2 = DirectEntry(text = "",
                                        scale =(0.04, 0, 0.04),
                                        pos =(0.38, 0, 0.9),
                                        command=None,
                                        initialText="nom2",
                                        numLines = 1,
                                        focus = 0,
                                        focusInCommand=lambda :self.clearText(self.entryJoueur2))

                                      
        self.mdp1 = OnscreenText(text = "Mot de passe",
                                pos = (-0.59,0.8,0), 
                                scale = 0.05)

        self.entryMdp1= DirectEntry(text = "",
                                        scale =(0.04, 0, 0.04),
                                        pos =(-0.42, 0, 0.8),
                                        command=None,
                                        initialText="mdp1",
                                        numLines = 1,
                                        focus = 0,
                                        focusInCommand=lambda :self.clearText(self.entryMdp1),
                                        obscured = 1)

        
        self.mdp2 = OnscreenText(text = "Mot de passe",
                                pos = (0.21,0.8,0), 
                                scale = 0.05)
                                      
        self.entryMdp2 = DirectEntry(text = "",
                                        scale =(0.04, 0, 0.04),
                                        pos =(0.38, 0, 0.8),
                                        command=None,
                                        initialText="mdp2",
                                        numLines = 1,
                                        focus = 0,
                                        focusInCommand=lambda :self.clearText(self.entryMdp2),
                                        obscured = 1)

        self.vs =  OnscreenText(text = "",
                                    pos = (0,0.35,0), 
                                    scale = 0.08,
                                    fg=(0,0,0,1))

        self.textNomMap = OnscreenText(text = "",
                                    pos = (0,0,0), 
                                    scale =  0.08,
                                    fg=(0,0,0,1))

        self.favori = OnscreenText(text = "",
                                    pos = (0, -0.24, 0),
                                    scale =  0.08,
                                    fg=(0, 0, 0, 1))

        self.frameTank2 = DirectFrame(frameColor=(0, 0, 0, 0),
                      frameSize=(-1, 1, -1, 1),
                      pos=(1, -1, -1))

        self.frameTank1 = DirectFrame(frameColor=(0, 0, 0, 0),
                      frameSize=(-1, 1, -1, 1),
                      pos=(-1, -1, -1))

        btnScale = (0.05,0.05)
        text_scale = 0.12
        borderW = (0.02, 0.02)
        couleurBack = (0.243,0.325,0.121,1)
        self.b1 = DirectButton(text = ("Connexion", "Connexion", "Connexion", "disabled"),
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          #########################################
                          command=self.connectPlayer,
                          #########################################
                          pos = (1,0,0.85))

        self.messageErreur = OnscreenText(text = "Merci de rentrer les informations...",
                                pos = (0.1,0.6,0), 
                                scale = 0.06)

        
    def changeFocus(self, contenu, args):
        pass

    def setText(self, item, textEntered):
        item.setText(textEntered)

    def connectPlayer(self):
        self.listJoueurs[0][0] = self.entryJoueur1.get()
        self.listJoueurs[0][1] = self.entryMdp1.get()
        self.listJoueurs[1][0] = self.entryJoueur2.get()
        self.listJoueurs[1][1] = self.entryMdp2.get()
        
        resultatLogin = self.gestionnaire.connecterJoueurs(self.listJoueurs)

        if resultatLogin:
            self.messageErreur.setText(resultatLogin[0])
            self.tankLogin = resultatLogin[1]
            
            for i in range(0, 2):
                if resultatLogin[1][i] == "OK":
                    rouge = float(resultatLogin[2][i].couleur_R) / float(255)
                    vert = float(resultatLogin[2][i].couleur_G) / float(255)
                    bleu = float(resultatLogin[2][i].couleur_B) / float(255)
                    couleurRGB = Vec3(rouge, vert, bleu)
                    if not isinstance(self.tankPrevisualisation[i], str):
                        self.tankPrevisualisation[i].destroy()
                        self.tankPrevisualisation[i] = ""
                    self.tankPrevisualisation[i] = TankPrevisualisation(i+1, couleurRGB, [self.frameTank1, self.frameTank2])
                    self.tankPrevisualisation[i].animer()
                elif resultatLogin[1][i] == "ERREUR":
                    if not isinstance(self.tankPrevisualisation[i], str):
                        self.tankPrevisualisation[i].destroy()
                        self.tankPrevisualisation[i] = ""

            if (resultatLogin[1][0] == "OK" or resultatLogin[1][0] == "ALREADY_OK") and (resultatLogin[1][1] == "OK" or resultatLogin[1][1] == "ALREADY_OK"):
                self.playerAreConnected = True
            else:
                self.playerAreConnected = False

            if self.playerAreConnected:
                self.calculerNom()
                btnScale = (0.15,0.15)
                text_scale = 0.12
                borderW = (0.01, 0.01)
                couleurBack = (0.243,0.325,0.121,1)
                self.textNomMap.setText("Combattrons dans\n"+self.nomMap)
                self.favori.setText(resultatLogin[3])
                if not hasattr(self, 'boutonCombattre') :
                    self.boutonCombattre = DirectButton(text = ("Combattre", "GO", "Prêt ?", "disabled"),
                                    text_scale=btnScale,
                                    borderWidth = borderW,
                                    text_bg=couleurBack,
                                    frameColor=couleurBack,
                                    relief=2,
                                    command=lambda: self.chargeJeu(self.idMap, [resultatLogin[2][0], resultatLogin[2][1]], self.gestionnaire.gestionDtoJoueur),
                                    pos = (0,25,-0.6))

                    self.animerBoutonCombattre()
            else :
                self.calculerNom()
                self.vs.setText("")
                self.textNomMap.setText("")
                self.favori.setText("")
                self.arreterAnimerTextVs()
                if hasattr(self, 'boutonCombattre'):
                    self.boutonCombattre.destroy()
                    del self.boutonCombattre

    def animerTextVs(self):
        self.cacher()
        grossir = LerpScaleInterval(self.vs, 1, (2.6, -3.1, 2.6), (2.5, -3, 2.5))
        retrecir = LerpScaleInterval(self.vs, 1,(2.5, -3, 2.5) , (2.6, -3.1, 2.6))
        megaGros = LerpScaleInterval(self.vs, 1, (2.5, -3,2.5), (1, 1, 1))

        LerpPosInterval(self.vs, 1, (0,0,-0.8), (0,0.35,0)).start()

        animationContinu = Sequence(grossir, retrecir, grossir)
        animationContinu.loop()
        Sequence(megaGros, animationContinu).start()

    def arreterAnimerTextVs(self):
        pass

    def clearText(self, value):
        None

    def animerBoutonCombattre(self):
        grossir1 = LerpScaleInterval(self.boutonCombattre, 0.3, (1.2, 1.2, 1.2), (1, 1, 1))
        grossir2 = LerpScaleInterval(self.boutonCombattre, 0.4, (1.2, 1.2, 1.2), (1, 1, 1))
        retrecir = LerpScaleInterval(self.boutonCombattre, 0.9, (1, 1, 1), (1, 1, 1),)

        Sequence(grossir1,grossir2 ,retrecir).loop()
    
    def calculerNom(self):
        resultatNom = self.gestionnaire.calculerNomJoueurs()
        self.vs.setText(resultatNom[0] + "\nvs\n" + resultatNom[1])

    
    def chargeJeu(self, mapId, joueurs, gestionDtoJoueur):
        self.gestionnaire.debutPartie([joueurs[0].id, joueurs[1].id])
        self.animerTextVs()
        self.boutonCombattre.destroy()
        for i in self.tankPrevisualisation:
            i.dernierTours()
        Sequence(Wait(4),
                    Func(lambda : self.transition.irisOut(0.2)),
                    SoundInterval(self.sound),
                    Func(self.cacher),
                    Func(lambda : messenger.send("DemarrerPartie", [mapId, joueurs, gestionDtoJoueur])),
                    Wait(0.2),
                    Func(self.bg.destroy),
                    Func(self.vs.destroy),
                    Func(self.frameTank1.destroy),
                    Func(self.frameTank2.destroy),
                    Func(lambda : self.transition.irisIn(0.2))
        ).start()
    

    def cacher(self):
        base.cam.node().getDisplayRegion(0).setSort(self.baseSort)
        self.joueur1.destroy()
        self.entryJoueur1.destroy()     
        self.joueur2.destroy()
        self.entryJoueur2.destroy()          
        self.mdp1.destroy()
        self.entryMdp1.destroy()
        self.mdp2.destroy()
        self.entryMdp2.destroy()
        self.b1.destroy()
        self.messageErreur.destroy()    
        self.textNomMap.destroy()
        self.boutonCombattre.destroy()
        self.favori.destroy()

class GestionnairePrevisualisation(object):
    def __init__(self):
        self.listJoueurs = [[], []]
        self.gestionDtoJoueur = GestionDTOJoueur()

    def connecterJoueurs(self, listJoueurs):
        responses = self.gestionDtoJoueur.recupererDTOJoueur(listJoueurs)

        message = ""
        info = []

        if len(responses) > 0:
            for i in range(0, 2):
                numeroJoueur = i+1
                if responses[i] == self.listJoueurs[i]:
                    message += "Le joueur "+ str(numeroJoueur) + "est déjà connecté. "
                    info.append("ALREADY_OK")
                else :
                    if (type(responses[i]) == str):
                        message += "Le joueur "+ str(numeroJoueur) + " n'a pas pu se connecter. "
                        info.append("ERREUR")
                    else:
                        message += "Le joueur "+ str(numeroJoueur) + " a pu se connecter. "
                        info.append("OK")
        else:
            return False

        
        self.listJoueurs = responses

        favori = ""
        indice = 0
        isPlayer = True

        for joueur in self.listJoueurs:
            if isinstance(joueur, str):
                isPlayer = False
            if indice == 1 and isPlayer :
                favori = self.gestionDtoJoueur.quiEstLeJoueurFavoriQuiALePlusDeChanceDeGagnerLePalpitantCombatDeTankemHashTagGrosSuspenceEstCeQueLaLogiqueVaEtreRespecte(self.listJoueurs)
            indice += 1

        for i in range(0, 2):
            if isinstance(self.listJoueurs[i], str):
                self.listJoueurs[i] = ""

        return [message, info, self.listJoueurs, favori]

    def calculerNomJoueurs(self):
        listNom = []
        for i in range(0, 2):
            if not isinstance(self.listJoueurs[i], str):
                listNom.append(self.gestionDtoJoueur.calculerLeNom(self.listJoueurs[i]))
            else:
                listNom.append("")
        return listNom 

    def debutPartie(self, idJoueurs):
        self.gestionDtoJoueur.debutPartie(idJoueurs)