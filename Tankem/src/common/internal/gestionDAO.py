#!/usr/bin/env python
# encoding: utf-8
from dao import *
"""
Fichier permettant de récupérer les données du DAO et de les nettoyées.
"""
class GestionDAO():

    def __init__(self):
        pass   

    def recupererDTO(self):
        #récupération données de la BD
        oracle = DAO_Oracle_Balance()
        nettoyeur = Nettoyeur()
        if hasattr(oracle, 'cursor'):
            dao = oracle.retrieve()
            #création de l'outil sanitize
            #min, max,courant
        else:
            dao = [None, None, None]
        if dao[0] is None:
            dtoMin = nettoyeur.initMinDTO( BalanceDTO())
        else:
            dtoMin = dao[0]

        if dao[1] is None:
            test = 1
            dtoMax = nettoyeur.initMaxDTO( BalanceDTO())
        else:
            dtoMax = dao[1]

        if dao[2] is None:
            dto = nettoyeur.initDTO( BalanceDTO())
        else:
            dto = dao[2]

            return nettoyeur.validerDTO(dto, dtoMin, dtoMax)
        return nettoyeur.validerDTO(dto, dtoMin, dtoMax)

    def recupererDTOMap(self, id_map):
        dao_map = DAO_Oracle_Map()
        dto_level_editor = dao_map.retrieve(id_map)
        # nettoyeurMap = NettoyeurMap()
        # nettoyeurMap.validerDTO(dto_level_editor)
        return dto_level_editor

    def recupererMap(self):
        dao_map = DAO_Oracle_Map()
        return dao_map.get_info_map()