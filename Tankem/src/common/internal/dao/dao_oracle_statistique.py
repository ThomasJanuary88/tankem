# -*- coding: utf-8 -*-

# voir : http://www.oracle.com/technetwork/articles/dsl/mastering-oracle-python-1391323.html

from __future__ import print_function

from dao_oracle import DAO_Oracle
from balanceDTO import BalanceDTO
from collections import namedtuple
from level_editor_dto import *
from statistiqueDTO import *

import cx_Oracle

class DAO_Oracle_Statistique(DAO_Oracle):

	class Interne_DAO_Oracle_Statistique(DAO_Oracle):
		def __init___(self):
			try:
				self.listeJoueur = None
			except cx_Oracle.DatabaseError as excep:
				print("Impossible de récupérer les données.")

		def retrieveJoueur(self, liste_id_joueur):
			try:
				# LISTE DES DEUX JOUEURS
				list_dto_joueur = []
				
				for id_joueur in liste_id_joueur:
					dto_joueur = Joueur()
					dto_joueur_dict = dto_joueur.__dict__
					if(type(id_joueur) == int):
						for key in dto_joueur_dict:
							self.execute_query("SELECT %s FROM %s WHERE id = %s" % (key, 'joueur', id_joueur))
							dto_joueur.__setattr__(key, self.cursor.fetchone()[0])
						list_dto_joueur.append(dto_joueur)
					else:
						list_dto_joueur.append("Usager non existant")

				return list_dto_joueur
				
			except cx_Oracle.DatabaseError as excep:
				print("Impossible de récupérer les données.")

		def insererJoueurArmePartie(self, dto_jap_dict):
			try:
				armes = ['canon', 'grenade', 'mitraillette', 'piege', 'shotgun', 'spring', 'missile']
				for arme in armes:
					self.insertion(dto_jap_dict, arme, 1)
					self.insertion(dto_jap_dict, arme, 2)
			except cx_Oracle.DatabaseError as excep:
				print("Impossible de récupérer les données.")

		def updateExp(self, expGagne):
			try:
				expTotal1 = self.listeJoueur[0].experience + expGagne[0]
				expTotal2 = self.listeJoueur[1].experience + expGagne[1]
				self.execute_query("UPDATE joueur SET experience = %s WHERE id = %s" % (expTotal1, self.listeJoueur[0].id))
				self.execute_query("UPDATE joueur SET experience = %s WHERE id = %s" % (expTotal2, self.listeJoueur[1].id))
				self.connection.commit()

			except cx_Oracle.DatabaseError as excep:
				print("Impossible de récupérer les noms des maps.")



		def insertion(self, dto_jap_dict, nomArme, quelJoueur):
			try:
				self.execute_query("SELECT %s FROM %s WHERE nom = '%s'" % ('id', 'arme', nomArme))
				idArme = self.cursor.fetchone()[0]

				#appel une fonction avec le id du joueur, nom de l'arme, id partie
				param = []
				if quelJoueur == 1:
					param.append(dto_jap_dict['id_joueur1'])
					param.append(idArme)
					param.append(dto_jap_dict['id_partie'])
					param.append(dto_jap_dict[nomArme]['nbTirsUn'])
					param.append(dto_jap_dict[nomArme]['nbTirsSuccesUn'])
				else:
					param.append(dto_jap_dict['id_joueur2'])
					param.append(idArme)
					param.append(dto_jap_dict['id_partie'])
					param.append(dto_jap_dict[nomArme]['nbTirsDeux'])
					param.append(dto_jap_dict[nomArme]['nbTirsSuccesDeux'])
				

				self.cursor.callproc('setJoueurArmePartie', param)
			except cx_Oracle.DatabaseError as excep:
			 	print("Impossible de récupérer les données.")

		def insererPartie(self, dto_partie):
			try:
				if dto_partie['id_map'] == 0:
					dto_partie['id_map'] = 1
				param = []
				param.append(None)
				param.append(dto_partie['date_debut'])
				param.append(dto_partie['date_fin'])
				param.append(dto_partie['id_map'])
				param.append(dto_partie['id_joueur1'])
				param.append(dto_partie['id_joueur2'])
				param.append(dto_partie['id_gagnant'])
				param.append(dto_partie['dist_joueur1'])
				param.append(dto_partie['dist_joueur2'])
				self.cursor.callproc('addPartie', param)

				self.execute_query("SELECT MAX(%s) FROM %s" % ('id', 'partie'))
				idPartie = self.cursor.fetchone()[0]
				
				return idPartie
				
			except cx_Oracle.DatabaseError as excep:
				print (excep)
				print("Impossible d'inserer les données")


		def verification_connexion(self, liste_joueur):
			''' Fonction retournant le id des joueurs qui se connectent '''
			
			try:
				liste_id_joueur = []
				for joueur in liste_joueur:
					nom = joueur[0]
					mdp = joueur[1]
					if(self.execute_query("SELECT %s FROM %s WHERE nom_usager = '%s' AND mot_de_passe = '%s'" % ('id', 'joueur', nom, mdp))):
						id = self.cursor.fetchone()
						
						if(id != None):
							liste_id_joueur.append(id[0])
						else:
							liste_id_joueur.append("Le nom de compte et/ou le mot de passe est invalide")
				print(liste_id_joueur)
				return liste_id_joueur


			except cx_Oracle.DatabaseError as excep:
				print("Impossible de récupérer les données.")

	dao_instance = None

	def __new__(cls):
		if not DAO_Oracle_Statistique.dao_instance:
			DAO_Oracle_Statistique.dao_instance = DAO_Oracle_Statistique.Interne_DAO_Oracle_Statistique()
		return DAO_Oracle_Statistique.dao_instance