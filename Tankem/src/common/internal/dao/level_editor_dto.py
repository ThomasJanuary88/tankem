#!/usr/bin/env python
# encoding: utf-8
"""
Class qui correspond aux données de la map
"""
class Level_editor_dto(object):
	def __init__(self):
		dto_map = None
		list_dto_relation = None
		list_dto_tuile = None
		list_dto_tank = None

class Map(object):
	def __init__(self):
		self.id = None
		self.nom = None
		self.date_creation = None
		self.id_statut = None
		self.taille_x = None
		self.taille_y = None
		self.id_tank1 = None
		self.id_tank2 = None 

class Relation(object):
	def __init__(self):
		self.id = None
		self.id_tuile = None
		self.id_map = None;
		self.coord_x = None;
		self.coord_y = None;

class Tuile(object):
	def __init__(self):
		self.id = None;
		self.id_type = None;
		self.arbre = None;

class Tank(object):
	def __init__(self):
		self.id = None
		self.coord_x = None
		self.coord_y = None