#!/usr/bin/env python
# encoding: utf-8
"""
Fichier permettant la balance du jeu.
"""
class BalanceDTO(object):
    # class __BalanceDTO:
    def __init__(self):
        self.vitesseLinChar = 7.0
        self.vitesseRotationChar = 1500.0
        self.pointDeVieChar = 200.0
        self.vitesseBloc = 0.8
        self.vitesseCanon = 14.0
        self.delaiCanon = 1.2
        self.vitesseMitraillette = 18.0
        self.delaiMitraillette = 0.4
        self.vitesseGrenade = 16.0
        self.delaiGrenade = 0.8
        self.vitesseShotgun = 13.0
        self.delaiShotgun = 1.8
        self.ouvertureShotgun = 0.4
        self.vitessePiege = 1.0
        self.delaiPiege = 0.8
        self.vitesseMissile = 30.0
        self.delaiMissile = 3.0
        self.vitesseSpring = 10.0
        self.delaiSpring = 0.5
        self.rayonExplosion = 8.0
        self.strMsgAccueuilContenu = "Appuyer sur F1 pour l'aide"
        self.msgAccueuilDuree = 3.0
        self.msgRebourDuree = 3.0
        self.strMsgDebutContenu = "Tankem!"
        self.strMsgFinContenu = "\n Joueur {0} a gagné!"