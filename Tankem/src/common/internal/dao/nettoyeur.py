#!/usr/bin/env python
# encoding: utf-8
"""
Fichier permettant de s'assurer de la qualité des données de la balance du jeu.
"""
class Nettoyeur():

    def __init__(self):
        self.erreur = []

    def verifTypeChiffre(self, value):
        """
        Fonction qui s'assure que le type est bien un float sinon assigne 0.0f
        """
        try:
            value = float(value)
        except ValueError:
            value = 0.0
        finally:
            return value

    def verifTypeString(self, value):
        """
        Fonction qui s'assure que le type est bien un string sinon mais une chaine vide.
        """
        try:
            value = str(value)
        except ValueError:
            value = ""
        finally:
            return value

    def clampChiffre(self,value, variable, min=0, max=0):
        """
        Fonction qui met le met le chiffre au maximum ou au minimum 
        """
         #test de type
        value = self.verifTypeChiffre(value)
        variable = self.verifTypeString(variable)
        min = self.verifTypeChiffre(min)
        max = self.verifTypeChiffre(max)

        message = ""
        if(max < min):
            max = min
            message += ("Le max est plus petit que le min. ")
        if(value < min):
            value = min
            message += ("La valeur est plus petite que le min. ")
        elif(value > max):
            value = max
            message += ("La valeur est plus grande que le max.")
        
        if len(message) != 0:
            self.erreur.append("Pour la variable: "+ variable + ": " + message)

        return value

    def clampTexte(self,texte, variable, min=0, max=0):
        """
        Fonction qui met le met coupe le texte s'il est trop long ou ajoute des espaces au besoin
        """
        #test de type
        texte = self.verifTypeString(texte)
        variable = self.verifTypeString(variable)
        min = self.verifTypeChiffre(min)
        max = self.verifTypeChiffre(max)

        longueur = len(texte)
        message = ""
        if(max < min):
            max = min
            message += ("Le max est plus petit que le min. ")
            
        if(longueur > max):
            texte = texte[0:int(max)]
            message += ("Le texte est trop long.")
        elif(longueur < min):
            for i in range(max-longueur):
                texte += " "
            message += ("Le texte est trop court. ")

        if len(message) != 0:
            self.erreur.append("Pour la variable: "+ variable + ": " + message)

        return texte

    def validerDTO(self, dto, dtoMin, dtoMax):
        """
        Fonction qui met s'assure que la valeur du dto est contenu entre le minimum et le maximum 
        """
        self.dto = dto
        for key in dto.__dict__:
            if key.startswith("str"):
                setattr(dto, key,self.clampTexte(dto.__getattribute__(key),key,dtoMin.__getattribute__(key),dtoMax.__getattribute__(key)))
            else:
                setattr(dto, key, self.clampChiffre(dto.__getattribute__(key),key,dtoMin.__getattribute__(key),dtoMax.__getattribute__(key)))
        return dto

    def validerTypeDTO(self, dto):
        """
        Fonction qui met s'assure que la valeur du dto est du bon type
        """
        for key in dto.__dict__:
            setattr(dto, key, self.verifTypeChiffre(dto.__getattribute__(key)))
        return dto

    def initMinDTO(self, dtoMin):
        """
        Fonction qui met initialise les valeurs du dtoMin 
        """
        dtoMin.vitesseLinChar = 4.0
        dtoMin.vitesseRotationChar = 1000.0
        dtoMin.pointDeVieChar = 100.0
        dtoMin.vitesseBloc = 0.2
        dtoMin.vitesseCanon = 4.0
        dtoMin.delaiCanon = 0.2
        dtoMin.vitesseMitraillette = 4.0
        dtoMin.delaiMitraillette = 0.2
        dtoMin.vitesseGrenade = 10.0
        dtoMin.delaiGrenade = 0.2
        dtoMin.vitesseShotgun = 4.0
        dtoMin.delaiShotgun = 0.2
        dtoMin.ouvertureShotgun = 0.1
        dtoMin.vitessePiege = 0.2
        dtoMin.delaiPiege = 0.2
        dtoMin.vitesseMissile = 20.0
        dtoMin.delaiMissile = 0.2
        dtoMin.vitesseSpring = 6.0
        dtoMin.delaiSpring = 0.2
        dtoMin.rayonExplosion = 1.0
        dtoMin.strMsgAccueuilContenu = 0
        dtoMin.msgAccueuilDuree = 1.0
        dtoMin.msgRebourDuree = 1.0
        dtoMin.strMsgDebutContenu = 0
        dtoMin.strMsgFinContenu = 0
        return dtoMin

    def initMaxDTO(self, dtoMax):
        """
        Fonction qui met initialise les valeurs du dtoMax
        """
        dtoMax.vitesseLinChar = 12.0
        dtoMax.vitesseRotationChar = 2000.0
        dtoMax.pointDeVieChar = 2000.0
        dtoMax.vitesseBloc = 2.0
        dtoMax.vitesseCanon = 30.0
        dtoMax.delaiCanon = 10.0
        dtoMax.vitesseMitraillette = 30.0
        dtoMax.delaiMitraillette = 10.0
        dtoMax.vitesseGrenade = 25.0
        dtoMax.delaiGrenade = 10.0
        dtoMax.vitesseShotgun = 30.0
        dtoMax.delaiShotgun = 10.0
        dtoMax.ouvertureShotgun = 1.5
        dtoMax.vitessePiege = 4.0
        dtoMax.delaiPiege = 10.0
        dtoMax.vitesseMissile = 40.0
        dtoMax.delaiMissile = 10.0
        dtoMax.vitesseSpring = 20.0
        dtoMax.delaiSpring = 10.0
        dtoMax.rayonExplosion = 30.0
        dtoMax.strMsgAccueuilContenu = 60
        dtoMax.msgAccueuilDuree = 10.0
        dtoMax.msgRebourDuree = 10.0
        dtoMax.strMsgDebutContenu = 50
        dtoMax.strMsgFinContenu =70
        return dtoMax

    def initDTO(self, dto):
        """
        Fonction qui met initialise les valeurs du dto
        """
        dto.vitesseLinChar = 7.0
        dto.vitesseRotationChar = 15000.0
        # dto.vitesseRotationChar = 1500.0
        dto.pointDeVieChar = 200.0
        dto.vitesseBloc = 0.8
        dto.vitesseCanon = 14.0
        dto.delaiCanon = 1.2
        dto.vitesseMitraillette = 18.0
        dto.delaiMitraillette = 0.4
        dto.vitesseGrenade = 16.0
        dto.delaiGrenade = 0.8
        dto.vitesseShotgun = 13.0
        dto.delaiShotgun = 1.8
        dto.ouvertureShotgun = 0.4
        dto.vitessePiege = 1.0
        dto.delaiPiege = 0.8
        dto.vitesseMissile = 30.0
        dto.delaiMissile = 3.0
        dto.vitesseSpring = 10.0
        dto.delaiSpring = 0.5
        dto.rayonExplosion = 8.0
        dto.strMsgAccueuilContenu = "Problème de connexion. Configuration par défaut utilisée."
        dto.msgAccueuilDuree = 3.0
        msgRebourDuree = 3.0
        dto.strMsgDebutContenu = "Tankem!"
        dto.strMsgFinContenu = "\n Joueur {0} a gagné!"
        return dto


class NettoyeurMap():

    def validerDTO(self, level_editor_dto):
        # Vérifie le tag qui témoigne de la présence d'un arbre
        for tuile in level_editor_dto.list_dto_tuile:
            if tuile.arbre != 'O' and tuile.arbre != 'N':
                tuile[0] = 'N'

        # Vérifie si deux tanks sont sur la même tuile
        # if level_editor_dto.list_dto_tank[0].coord_x == level_editor_dto.list_dto_tank[1].coord_x 
        #     and level_editor_dto.list_dto_tank[0].coord_y == level_editor_dto.list_dto_tank[1].coord_y:
        #     self.changePosTank()

        # Vérifie si les tanks ont des positions différentes
        if level_editor_dto.list_dto_tank[0].coord_x == level_editor_dto.list_dto_tank[1].coord_x and level_editor_dto.list_dto_tank[0].coord_y == level_editor_dto.list_dto_tank[1].coord_y:
            self.changePosTank()
        
        # Vérifie si les tanks ne sont pas sur une tuile avec un arbre
        # for tank in level_editor_dto.list_dto_tank:
        #     for tuile in level_editor_dto.list_dto_tuile:
        #         if tuile.coord_x == tank.coord_x and tuile.coord_y == tank.coord_y:
        #             if tuile.arbre == 'O':
        #                 self.changePosTank()
        #                 break;

    def changePosTank(self):
        for tuile in level_editor_dto.list_dto_tuile:
            if tuile.arbre == 'N':
                pass