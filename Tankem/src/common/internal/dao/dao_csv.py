# -*- coding: utf-8 -*-

# voir : http://www.oracle.com/technetwork/articles/dsl/mastering-oracle-python-1391323.html

from __future__ import print_function


from dao_abstract import DAO_Abstract
from balanceDTO import BalanceDTO

from Tkinter import Tk
from tkFileDialog import askopenfilename
from tkFileDialog import asksaveasfilename
import csv
import os


class DAO_Csv(DAO_Abstract):
	def __init__(self):
		pass

	def create(self, dtoMin, dtoMax, dto):
		filename = selectSaveAsCSV()
		if filename:
			with open(filename, "wb") as file:
				writer = csv.writer(file, "excel")
				compteur = 0
				for key in dtoMin:
					writer.writerow([key+"Min", dtoMin[key]])
				for key in dtoMax:
					writer.writerow([key+"Max", dtoMax[key]])
				for key in dto:
					writer.writerow([key, dto[key]])

				# while(compteur != len(dtoMin)):
				# 	writer.writerow([liste[compteur]+"Min", liste[compteur+1]])
				# 	compteur+=2	
				# while(compteur != len(dtoMax)):
				# 	writer.writerow([liste[compteur]+"Max", liste[compteur+1]])
				# 	compteur+=2
				# while(compteur != len(dto)):
				# 	writer.writerow([liste[compteur]+, liste[compteur+1]])
				# 	compteur+=2	
				
				#Ouverture du fichier excell
				os.startfile(filename)

	def retrieve(self):
		filename = self.selectOpenCSV()
		dtoMin = {}
		dtoMax = {}
		dto = {}

		# Variables permettant de garder en memoire la cle de la valeur qui va suivre
		newKey = ""
		# Permet de savoir que la prochaine sera une cle et non une valeur
		prochaineCellule = "key"
		# Permet de savoir si c'est le DTO min, max ou le courant
		quelDTO = ""
		# Permet de savoir si la prochiane valeur c'est un float ou un string
		str = False;

		if filename:
			with open(filename, "rb") as file:
				reader = csv.reader(file, dialect = "excel")
				for row in reader:
					for cell in row:
						# Si c'est une cle, on regarde de quel dto il appartient et on stock la nouvelle cle pour le mettre dans son dto
						if(prochaineCellule == "key"):
							# Cette condition permet de savoir si on va devoir transtyper en float
							if(cell[:3] == "str"):
								str = True;

							if(cell[-3:] == "Min"):
								cell = cell[:-3]
								newKey = cell
								quelDTO = "Min"
							elif(cell[-3:] == "Max"):
								cell = cell[:-3]
								newKey = cell
								quelDTO = "Max"
							else:
								newKey = cell
								quelDTO = "Normal"
							prochaineCellule = "value"

						# Si c'est une valeur, on transtype en float si ce n'est pas un string et on met la valeur a sa cle
						else:
							if(str == False):
								try:
									cell = float(cell)
								except ValueError:
									if type(cell) == str:
										cell = 0.0
							else:
								str = False;
							if(quelDTO == "Min"):
								dtoMin[newKey] = cell
							elif(quelDTO == "Max"):
								if(newKey == "strMsgFinContenu" or newKey == "strMsgAccueuilContenu" or newKey ==  "strMsgDebutContenu"):
									cell = float(cell)
								dtoMax[newKey] = cell
							else:
								dto[newKey] = cell
							prochaineCellule = "key"

					
		obj_dto_min = BalanceDTO()
		obj_dto_max = BalanceDTO()
		obj_dto_courant = BalanceDTO()
		# On rempli les dto avec les bonnes valeurs / cles
		for key in dtoMin:
			obj_dto_min.__setattr__(key, dtoMin[key])
		for key in dtoMax:
			obj_dto_max.__setattr__(key, dtoMax[key])
		for key in dto:
			obj_dto_courant.__setattr__(key, dto[key])
			
		liste = [obj_dto_min, obj_dto_max, obj_dto_courant]
		return liste

	def update(self, dtoMin, dtoMax, dto):
		# Ouverture du fichier pour savoir ou on sauvegarde
		filename = self.selectSaveAsCSV()

		# Transforme en dico les dto recu
		dict_dtoMin = dtoMin.__dict__
		dict_dtoMax = dtoMax.__dict__
		dict_dto = dto.__dict__

		if filename:
			with open(filename, "wb") as file:
				writer = csv.writer(file, "excel")
				compteur = 0
				# Afin de bien distinguer les dto, on ajoute les suffixes pour savoir les donnees appartiennent a quel dto
				for key in dict_dtoMin:
					if(key != "strMsgFinContenu"):
						writer.writerow([key+"Min", dict_dtoMin[key]])
				for key in dict_dtoMax:
					writer.writerow([key+"Max", dict_dtoMax[key]])
				for key in dict_dto:
					writer.writerow([key, dict_dto[key]])
				
		#Ouverture du fichier excell
		os.startfile(filename)

	def delete():
		pass

	def selectOpenCSV(self):
		Tk().withdraw()
		return askopenfilename(initialdir = "desktop", filetypes = [("CSV files", "*.csv"), ("All files", "*")], defaultextension = "*.csv", initialfile="BalanceTankem")

	def selectSaveAsCSV(self):
		Tk().withdraw()
		return asksaveasfilename(initialdir = "desktop", filetypes = [("CSV files", "*.csv"), ("All files", "*")], defaultextension = "*.csv", initialfile="BalanceTankem")