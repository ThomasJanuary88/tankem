# -*- coding: utf-8 -*-

# voir : http://www.oracle.com/technetwork/articles/dsl/mastering-oracle-python-1391323.html

from __future__ import print_function

from dao_oracle import DAO_Oracle
from balanceDTO import BalanceDTO
from collections import namedtuple

import cx_Oracle

class DAO_Oracle_Balance(DAO_Oracle):
		
	def retrieve(self):
		try:
			self.connection_db()
			dto_min = self.create_dto(BalanceDTO(), "min")
			dto_max = self.create_dto(BalanceDTO(), "max")
			dto_courant = self.create_dto(BalanceDTO(), "courant")

			self.close_connection_db()
			return [dto_min, dto_max, dto_courant]
		
		except cx_Oracle.DatabaseError as excep:
			print("Impossible de récupérer les données.")
			return [None, None, None]
			

	def create_dto(self, dto, dto_type):
		# Crée des dtos et les retoune dans une liste[min, max, courant]
		dict_dto = dto.__dict__
		for key in dict_dto:
			# A chaque erreur on s'assure que result = None
			# pour ne pas setter n'importe quoi
			try:
				# Selection pour la table unite
				self.execute_query("SELECT %s FROM %s WHERE %s = %s" % (dto_type, 'unite', 'nom' , "'"+key+"'"))
				result = self.cursor.fetchone()
				if (result is not None):
					dto.__setattr__(key, result[0])

				# Selection pour la table afficahge
				self.execute_query("SELECT %s FROM %s WHERE %s = %s" % ("contenu_"+dto_type, 'affichage', 'nom' , "'"+key+"'"))
				result = self.cursor.fetchone()
				if (result is not None):
					dto.__setattr__(key, result[0])
			except Exception as e:
				result = None

		return dto

	def update(self, dtoMin, dtoMax, dto):
		try:
			self.connection_db()

			dict_dto_min = dtoMin.__dict__
			dict_dto_max = dtoMax.__dict__
			dict_dto_courant = dto.__dict__

			# Dictionnaire qui va servir à fusionner les valeurs des 3 dtos
			args = {}

			# Création d'une liste vide pour chaque clé du dictionnaire args
			for key in dict_dto_courant:
				args[key] = []

			# Remplissage des listes du dictionnaire par les valeurs minimums
			for key in dict_dto_min:
				if (key != 'strMsgAccueuilContenu' and key != 'strMsgDebutContenu' and key != 'strMsgFinContenu'):
					args[key].append(dict_dto_min[key])

			# Remplissage des listes du dictionnaires par les valeurs maximales
			for key in dict_dto_max:
				args[key].append(dict_dto_max[key])

			# Remplissage des listes du dictionnaire pas les valeurs courantes
			for key in dict_dto_courant:
				args[key].append(dict_dto_courant[key])

			# A chaque clé du dictionnaire on y prend la liste associée
			for key in args:
				# La liste doit avoir pour premier argument la clé du dictionnaire
				parameters = []
				parameters.append(key)
				for values in args[key]:
					parameters.append(values)
				if (key == 'strMsgAccueuilContenu'  or (key == 'strMsgDebutContenu' or key == 'strMsgFinContenu')):
					self.cursor.callproc('procedure_affichage', parameters)
				else:
					self.cursor.callproc('procedure_unite', parameters)

			self.commit()

		except cx_Oracle.DatabaseError as excep:
			print("Impossible de mettre à jour la base de données.")