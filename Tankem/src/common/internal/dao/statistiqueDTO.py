#!/usr/bin/env python
# encoding: utf-8
"""
Class qui correspond aux données de la map
"""
class StatistiqueDTO(object):
	def __init__(self):

        # contient une liste de 2 joueurs
        # contient 1 dto partie
        # contient X dto joueur arme partie (1 pour chaque arme)
		list_dto_joueur = None
		dto_partie = None
		list_dto_joueur_arme_partie = None

class Joueur(object):
	def __init__(self):
		self.id = None
		self.couleur_R = None
		self.couleur_G = None
		self.couleur_B = None
		self.message = None
		self.experience = None
		self.vie = None
		self.force = None
		self.agilite = None
		self.dexterite = None
		self.nom_usager = None
		self.mot_de_passe = None

	def __eq__(self, other):
		if not isinstance(other, Joueur):
			return False

		for key, value in self.__dict__.iteritems():
			if self.__dict__[key] != other.__dict__[key]:
				return False

		return True

class Partie(object):
	def __init__(self):
		self.id = None
		self.date_debut = None
		self.date_fin = None
		self.id_joueur1 = None
		self.id_joueur2 = None
		self.id_gagnant = None
		self.dist_joueur1 = 0
		self.dist_joueur2 = 0
		self.id_map = None

class JoueurArmePartie(object):
	def __init__(self):

		self.id_joueur1 = None
		self.id_joueur2 = None
		self.id_partie = None

		self.grenade = { "nbTirsUn" : 0, 
						 "nbTirsSuccesUn" : 0,
						 "nbTirsDeux" : 0, 
					     "nbTirsSuccesDeux" : 0}
		self.canon = { "nbTirsUn" : 0, 
					   "nbTirsSuccesUn" : 0,
					   "nbTirsDeux" : 0, 
					   "nbTirsSuccesDeux" : 0}
		self.mitraillette = { "nbTirsUn" : 0, 
						      "nbTirsSuccesUn" : 0,
							  "nbTirsDeux" : 0, 
					          "nbTirsSuccesDeux" : 0}
		self.piege = { "nbTirsUn" : 0, 
					   "nbTirsSuccesUn" : 0,
					   "nbTirsDeux" : 0, 
					   "nbTirsSuccesDeux" : 0}
		self.shotgun = {"nbTirsUn" : 0, 
						"nbTirsSuccesUn" : 0,
						"nbTirsDeux" : 0, 
					    "nbTirsSuccesDeux" : 0}
		self.spring = { "nbTirsUn" : 0, 
						"nbTirsSuccesUn" : 0,
						"nbTirsDeux" : 0, 
					    "nbTirsSuccesDeux" : 0}
		self.missile = {"nbTirsUn" : 0, 
					  "nbTirsSuccesUn" : 0,
					  "nbTirsDeux" : 0, 
					  "nbTirsSuccesDeux" : 0}
