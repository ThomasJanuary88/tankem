# -*- coding: utf-8 -*-

# voir : http://www.oracle.com/technetwork/articles/dsl/mastering-oracle-python-1391323.html

from __future__ import print_function

from dao_oracle import DAO_Oracle
from balanceDTO import BalanceDTO
from collections import namedtuple
from level_editor_dto import *

import cx_Oracle

class DAO_Oracle_Map(DAO_Oracle):


	def retrieve(self, id_map):
		try:
			self.connection_db()

			# Création de liste pour conserver les dto
			list_dto_relation = []
			list_dto_tuile = []
			list_dto_tank = []


			# Création du dto map relatif à la base de données
			dto_map = Map()
			dto_map_dict = dto_map.__dict__
			for key in dto_map_dict:
				self.execute_query("SELECT %s FROM %s WHERE id = %s" % (key, 'map', id_map))
				dto_map.__setattr__(key, self.cursor.fetchone()[0])

			# Création des dto relation relatifs à la base de données
			self.execute_query("SELECT %s FROM %s WHERE id_map = %s" %('id', 'relation', id_map))
			for data in self.cursor.fetchall():
				dto_relation = Relation()
				dto_relation_dict = dto_relation.__dict__
				for key in dto_relation_dict:
					self.execute_query("SELECT %s FROM %s WHERE id = %s" % (key, 'relation', data[0]))
					dto_relation.__setattr__(key, self.cursor.fetchone()[0])
				list_dto_relation.append(dto_relation)

			# Création des dto tuile relatifs à la base de données
			for dto_relation in list_dto_relation:
				id_tuile = dto_relation.__getattribute__('id_tuile')
				dto_tuile = Tuile()
				dto_tuile_dict = dto_tuile.__dict__
				for key in dto_tuile_dict:
					self.execute_query("SELECT %s FROM %s WHERE id = %s" % (key, 'tuile', id_tuile))
					dto_tuile.__setattr__(key, self.cursor.fetchone()[0])
				list_dto_tuile.append(dto_tuile)

			# Création des dto tanks relatif à la base de données
			for key_dto_map in dto_map_dict:
				if (key_dto_map == 'id_tank1' or key_dto_map == "id_tank2"):
					dto_tank = Tank()
					dto_tank_dict = dto_tank.__dict__
					for key_dto_tank in dto_tank_dict:
						self.execute_query("SELECT %s FROM %s WHERE id = %s" % (key_dto_tank, 'Tank', dto_map_dict[key_dto_map]))
						dto_tank.__setattr__(key_dto_tank, self.cursor.fetchone()[0])
					list_dto_tank.append(dto_tank)

			# On change l'id_type de tuile pour son text relatif
			for dto_tuile in list_dto_tuile:
				id = dto_tuile.__getattribute__('id')
				id_type = dto_tuile.__getattribute__('id_type')
				self.execute_query("SELECT %s FROM %s WHERE (SELECT %s FROM tuile WHERE ID = %s) = id" % ('nom', 'Type', 'id_type', id))
				dto_tuile.__setattr__('id_type', self.cursor.fetchone()[0])

			# Création du dto Level_editor_dto
			level_editor_dto = Level_editor_dto()

			level_editor_dto.__setattr__('dto_map', dto_map)
			level_editor_dto.__setattr__('list_dto_relation', list_dto_relation)
			level_editor_dto.__setattr__('list_dto_tuile', list_dto_tuile)
			level_editor_dto.__setattr__('list_dto_tank', list_dto_tank)

			self.close_connection_db()

			return level_editor_dto
			
		except cx_Oracle.DatabaseError as excep:
			print("Impossible de récupérer les données.")

	def get_info_map(self):
		try:
			if hasattr(self, 'cursor'):
				self.connection_db()

				maps = {}

				self.execute_query("SELECT %s FROM %s" % ("nom", 'map'))
				for row in self.cursor:
					maps[row[0]] = []

				self.execute_query("SELECT %s FROM %s" % ("id, nom, date_creation", 'map'))
				for row in self.cursor:
					index = 0
					for data in row:
						if (index != 1):
							maps[row[1]].append(data)
						index += 1

				for key in maps:
					self.execute_query("SELECT %s FROM %s JOIN map ON map.id_statut = statut.id  WHERE map.nom = %s" % ('statut.nom', 'statut', "'" + key + "'"))
					result = self.cursor.fetchone()[0]
					maps[key].append(result)

				self.close_connection_db()

				return maps
			return []
		except cx_Oracle.DatabaseError as excep:
			print("Impossible de récupérer les noms des maps.")