#!/usr/bin/env python
# encoding: utf-8
from __future__ import print_function
from dao import *
from random import randint
import math
import datetime



class GestionDTOJoueur():

    def __init__(self):

		self.dao = DAO_Oracle_Statistique()
		self.dto_partie = Partie()
		self.dto_partie_dict = self.dto_partie.__dict__

		self.dto_JAP = JoueurArmePartie()
		self.dto_JAP_dict = self.dto_JAP.__dict__

		self.derniereArme = { "joueur1" : "",
							  "joueur2" : "" }

		self.dejaTouche= { "joueur1" : False,
						 "joueur2" : False }

		self.listeJoueur = None


    def debutPartie(self, tabIdJoueur):
		self.dto_partie_dict["date_debut"] = datetime.datetime.now()
		self.dto_partie_dict["id_joueur1"] = tabIdJoueur[0]
		self.dto_partie_dict["id_joueur2"] = tabIdJoueur[1]

		self.dto_JAP_dict["id_joueur1"] = tabIdJoueur[0]
		self.dto_JAP_dict["id_joueur2"] = tabIdJoueur[1]

    def setIdMap(self, idMap):
		self.dto_partie_dict["id_map"] = idMap

    def finPartie(self, perdant):
		self.dto_partie_dict["date_fin"] = datetime.datetime.now()
		if(perdant == 0): #Joueur 2 qui gagne
			gagnant = self.dto_partie_dict["id_joueur2"]
		else: #Joueur 1 qui gagne
			gagnant = self.dto_partie_dict["id_joueur1"]

		self.dto_partie_dict["id_gagnant"] = gagnant

		idPartie = self.dao.insererPartie(self.dto_partie_dict)
		self.dto_JAP_dict["id_partie"] = idPartie
		self.dao.insererJoueurArmePartie(self.dto_JAP_dict)


    def incrementeDistance(self, debut, fin, joueur):
		currentDistance = math.sqrt(math.pow(debut.getX() - fin.getX(), 2)) + math.sqrt(math.pow(debut.getY() - fin.getY(), 2))

		if joueur == 0:
			self.dto_partie_dict["dist_joueur1"] += currentDistance
		else:
			self.dto_partie_dict["dist_joueur2"] += currentDistance

    def incrementeArme(self, joueur, arme):
		if joueur == 0:
			stats = "nbTirsUn"
			self.derniereArme["joueur1"] = arme
			self.dejaTouche["joueur2"] = False
		else:
			stats = "nbTirsDeux"
			self.derniereArme["joueur2"] = arme
			self.dejaTouche["joueur1"] = False
		

		if(arme == "Canon"):
			self.dto_JAP_dict['canon'][stats] += 1
		elif(arme == "Grenade"):
			self.dto_JAP_dict['grenade'][stats] += 1
		elif(arme == "Piege"):
			self.dto_JAP_dict['piege'][stats] += 1
		elif(arme == "Shotgun"):
			self.dto_JAP_dict['shotgun'][stats] += 1
		elif(arme == "Guide"):
			self.dto_JAP_dict['missile'][stats] += 1
		elif(arme == "Mitraillette"):
			self.dto_JAP_dict['mitraillette'][stats] += 1
		elif(arme == "Spring"):
			self.dto_JAP_dict['spring'][stats] += 1

		
	


    def incrementeArmeSucces(self, joueur):
		arme = ""
		if joueur == 0:
			stats = "nbTirsSuccesDeux"
			arme = self.derniereArme["joueur2"]
		else:
			stats = "nbTirsSuccesUn"
			arme = self.derniereArme["joueur1"]
		if (joueur == 0 and self.dejaTouche["joueur1"] == False) or (joueur == 1 and self.dejaTouche["joueur2"] == False):
			if joueur == 0:
				self.dejaTouche["joueur1"] = True
			else:
				self.dejaTouche["joueur2"] = True

			if(arme == "Canon"):
				self.dto_JAP_dict['canon'][stats] += 1
			elif(arme == "Grenade"):
				self.dto_JAP_dict['grenade'][stats] += 1
			elif(arme == "Piege"):
				self.dto_JAP_dict['piege'][stats] += 1
			elif(arme == "Shotgun"):
				self.dto_JAP_dict['shotgun'][stats] += 1
			elif(arme == "Guide"):
				self.dto_JAP_dict['missile'][stats] += 1
			elif(arme == "Mitraillette"):
				self.dto_JAP_dict['mitraillette'][stats] += 1
			elif(arme == "Spring"):
				self.dto_JAP_dict['spring'][stats] += 1


    def recupererDTOJoueur(self, liste_joueur):
		DAOJoueur = DAO_Oracle_Statistique()
		listeIdJoueur = DAOJoueur.verification_connexion(liste_joueur)
		self.listeJoueur = DAOJoueur.retrieveJoueur(listeIdJoueur)
		DAOJoueur.listeJoueur = self.listeJoueur
		return self.listeJoueur


    def calculerLeNom(self, joueur):
		maxStat = 20
		liste_noms_calcules = []
		meilleur_stat = ["", 0]
		deuxieme_stat = ["", 0]
		qualificatifA = ""
		qualificatifB = ""
		god = True
		uneStat = 0
		liste_qualificatif = []
		liste_stats = {"vie" : joueur.vie, "force" : joueur.force, "agilite" : joueur.agilite, "dexterite" : joueur.dexterite}
		for stat in liste_stats:
			if(liste_stats.get(stat) > meilleur_stat[1]):
				deuxieme_stat = meilleur_stat
				meilleur_stat = [stat, liste_stats.get(stat)]
			elif(liste_stats.get(stat) > deuxieme_stat[1]):
				deuxieme_stat = [stat, liste_stats.get(stat)]

			if(liste_stats.get(stat) != maxStat):
				god = False
		
		# Si les 2 meilleurs sont egales, on choisi la meilleur de maniere aléatoire
		if(meilleur_stat[1] == deuxieme_stat[1]):
			if(randint(0,1) == 1):
				temp = deuxieme_stat
				deuxieme_stat = meilleur_stat
				meilleur_stat = temp


		if(meilleur_stat[0] == "vie"):
			if(meilleur_stat[1] >= 10):	
				qualificatifA = "L'immortel"
			elif(meilleur_stat[1] >= 5):
				qualificatifA = "Le pétulant"
			elif(meilleur_stat[1] >= 1):
				qualificatifA = "Le fougeux"

		elif(meilleur_stat[0] == "force"):
			if(meilleur_stat[1] >= 10):	
				qualificatifA = "Le tout puissant"
			elif(meilleur_stat[1] >= 5):
				qualificatifA = "Le Hulk"
			elif(meilleur_stat[1] >= 1):
				qualificatifA = "Le crossfiter"

		elif(meilleur_stat[0] == "agilite"):
			if(meilleur_stat[1] >= 10):	
				qualificatifA = "Le foudroyant"
			elif(meilleur_stat[1] >= 5):
				qualificatifA = "Le lynx"
			elif(meilleur_stat[1] >= 1):
				qualificatifA = "Le prompt"

		elif(meilleur_stat[0] == "dexterite"):
			if(meilleur_stat[1] >= 10):	
				qualificatifA = "Le chirurgien"
			elif(meilleur_stat[1] >= 5):
				qualificatifA = "L'habile"
			elif(meilleur_stat[1] >= 1):
				qualificatifA = "Le precis"
			

		if(deuxieme_stat[1] != 0):
			if(deuxieme_stat[0] == "vie"):
				if(deuxieme_stat[1] >= 10):	
					qualificatifB = "immortel"
				elif(deuxieme_stat[1] >= 5):
					qualificatifB = "pétulant"
				elif(deuxieme_stat[1] >= 1):
					qualificatifB = "fougeux"

			elif(deuxieme_stat[0] == "force"):
				if(deuxieme_stat[1] >= 10):	
					qualificatifB = "Marc-André"
				elif(deuxieme_stat[1] >= 5):
					qualificatifB = "brutal"
				elif(deuxieme_stat[1] >= 1):
					qualificatifB = "qui fait du crossfit"

			elif(deuxieme_stat[0] == "agilite"):
				if(deuxieme_stat[1] >= 10):	
					qualificatifB = "foudroyant"
				elif(deuxieme_stat[1] >= 5):
					qualificatifB = "lynx"
				elif(deuxieme_stat[1] >= 1):
					qualificatifB = "prompt"

			elif(deuxieme_stat[0] == "dexterite"):
				if(deuxieme_stat[1] >= 10):	
					qualificatifB = "chirurgien"
				elif(deuxieme_stat[1] >= 5):
					qualificatifB = "habile"
				elif(deuxieme_stat[1] >= 1):
					qualificatifB = "precis"

		if(god):
			qualificatifA = "Dominateur"
			qualificatifB = ""

		return joueur.nom_usager + " " + qualificatifA  + " " + qualificatifB

    def quiEstLeJoueurFavoriQuiALePlusDeChanceDeGagnerLePalpitantCombatDeTankemHashTagGrosSuspenceEstCeQueLaLogiqueVaEtreRespecte(self, listDtoJoueur):
		meilleur = 0
		nivMeilleur = None
		for joueur in listDtoJoueur:
			dictJoueur = joueur.__dict__
			exp = dictJoueur["experience"]
			niv = self.calculNiveauJoueur(exp)

			# En allant voir le niveau du 2ieme joueur, si c'est le meme niveau alors y'a pas de favori
			if nivMeilleur != None and nivMeilleur == niv:
				return "Aucun favori!"

			# Par défaut le premier entre ici et set le nivMeilleur apres reste a voir si le 2ieme entre ici
			if niv > nivMeilleur:
				nivMeilleur = niv
				meilleur = dictJoueur["nom_usager"]

			
		return "Le joueur favori est " + meilleur + " !" 
	
    def calculNiveauJoueur(self, exp):
		niveau = math.floor( ( (math.sqrt(exp - 50)) / (5*math.sqrt(2)) ) - 1)
		return niveau


	

			
