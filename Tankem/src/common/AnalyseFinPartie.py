#!/usr/bin/env python
# encoding: utf-8
from __future__ import print_function
from random import randint
import math
import datetime
from panda3d.core import *
from direct.showbase.ShowBase import ShowBase
from internal.dao.dao_oracle_statistique import DAO_Oracle_Statistique



class AnalyseFinPartie(ShowBase):

    def __init__(self):
        self.listJoueur = []
        self.accept("gameOver", self.setJoueur)
        self.favori = None
        self.expGagne = []
        self.niveau = []
        self.DAOJoueur = DAO_Oracle_Statistique()



    def setJoueur(self, joueur1, joueur2):
        self.listJoueur.append(joueur1)
        self.listJoueur.append(joueur2)
        
        self.favori = self.quiEstLeJoueurFavoriQuiALePlusDeChanceDeGagnerLePalpitantCombatDeTankemHashTagGrosSuspenceEstCeQueLaLogiqueVaEtreRespecte()
        self.expGagne.append(self.calculPointDexp(self.listJoueur[0].joueur.nom_usager))
        self.expGagne.append(self.calculPointDexp(self.listJoueur[1].joueur.nom_usager))

        self.niveau.append(self.calculNiveauJoueur(self.listJoueur[0].joueur.experience))
        self.niveau.append(self.calculNiveauJoueur(self.listJoueur[1].joueur.experience))

    
    def monterDeNiveau(self, exp):
        '''Fonction qui calcul si le joueur a lvl up'''
        niv = self.calculNiveauJoueur(exp)
        expProchainNiv = self.calculExpNecessaire(niv+1)
        if exp > expProchainNiv:
            return True
        return False

    def gestionExpJoueur(self, joueur1, joueur2):
        '''Fonction qui retourne le pourcentage des 2 joueurs au debut et a la fin'''
        expNivDebut = []
        expNiveauFin = []

        # L'experience complet a partir du debut du niveau jusqu'a la fin
        expTotalDuNiveau = []

        for niv in self.niveau:
             expNivDebut.append(self.calculExpNecessaire(niv))
             expNiveauFin.append(self.calculExpNecessaire(niv+1))
             expTotalDuNiveau.append(self.calculExpNecessaire(niv+1) - self.calculExpNecessaire(niv))

        pourcentages = []
        pourcentages.append(((joueur1.joueur.experience - expNivDebut[0] + self.expGagne[0]) * 100)/expTotalDuNiveau[0])
        pourcentages.append(((joueur2.joueur.experience - expNivDebut[1] + self.expGagne[1]) * 100)/expTotalDuNiveau[1])

        self.DAOJoueur.updateExp(self.expGagne)


        return pourcentages


            


    def calculNiveauJoueur(self, exp):
        '''Fonction qui le niveau en fonction de l'xp'''
        niveau = math.floor( ( (math.sqrt(exp - 50)) / (5*math.sqrt(2)) ) - 1)
        return niveau

    def calculExpNecessaire(self, niv):
        '''Fonction qui calcul l'xp necessaire selon un niveau'''
        exp = 100*(niv + 1) + 50 * math.pow(niv, 2)
        return exp

    def calculPointDexp(self, nomJoueur):
        '''Fonction qui calcul le nombre d'xp gagné par un joueur lors d'une partie'''
        f = 0
        joueur = None
        autreJoueur = None
        if nomJoueur == self.listJoueur[0].joueur.nom_usager:
            joueur = self.listJoueur[0]
            autreJoueur = self.listJoueur[1]
        else:
            joueur = self.listJoueur[1]
            autreJoueur = self.listJoueur[0]

        # Si gagnant et favori
        if(joueur.pointDeVie != 0 and joueur.joueur.nom_usager == self.favori):
            f = 1

        if(joueur.pointDeVie != 0):
            expGagne = 100 + 100*f + 2*(joueur.pointDeVie)
        else:
            viePerduGagnant = autreJoueur.pointDeVieMax - autreJoueur.pointDeVie
            expGagne = 2*(viePerduGagnant)
        
        self.dao = DAO_Oracle_Statistique()
        expTotal = expGagne + joueur.joueur.experience

        return expGagne

    def quiEstLeJoueurFavoriQuiALePlusDeChanceDeGagnerLePalpitantCombatDeTankemHashTagGrosSuspenceEstCeQueLaLogiqueVaEtreRespecte(self):
        '''Fonction qui retourne le nom du joueur favori'''
        meilleur = 0
        nivMeilleur = None
        for joueur in self.listJoueur:
            exp = joueur.joueur.experience
            niv = self.calculNiveauJoueur(exp)

			# En allant voir le niveau du 2ieme joueur, si c'est le meme niveau alors y'a pas de favori
            if nivMeilleur != None and nivMeilleur == niv:
                return "Aucun favori!"

			# Par défaut le premier entre ici et set le nivMeilleur apres reste a voir si le 2ieme entre ici
            if niv > nivMeilleur:
                nivMeilleur = niv
                meilleur = joueur.joueur.nom_usager

			
            return meilleur
