POUR LES NEOPHYTES

Afin de mettre a jour votre fichier CSV avec les donn�es de la BD, il s'agit d'allez dans le fichier util et
de double cliquer sur le script lecture.bat

Si au contraire, vous voulez mettre a jour les donn�es de la abse de donn�es Oracle avec vos donn�es CSV,
allez dans le dossier util de Tankem et double cliquez sur le script ecriture.bat


POUR LES MOINS NEOPHYTES :
Petites explication du fonctionnement des scripts

Le script de lecture permet de lire les donn�es de la base de donn�es d'Oracle.
Les donn�es sont ins�r�es dans le csv. Dans le fichier excel, le s�parateur est vraiment une virgule 
et non la cellule d'apr�s. Donc il faut faire attention lorsqu'on veut modifier.

Le script d'�criture permet de lire les donn�es du csv, les mets dans 3DTO diff�rents et les donn�es sont
envoy�es dans le nettoyeur afin de faire les v�rifications. C'est � dire v�rification du type, des donn�es
hors limites. Si les donn�es sont hors limites, ont clamp avec la donn�e maximum ou minimum d�pendant de
la situation. Dans le cas d'un type incorrect, s'il s'attend a recevoir un string et il recoit un chiffre,
on lui donne un string vide et dans le cas inverse, on lui met la valeur 0.

Lors du choix des fichiers a ouvrir ou a sauvegarder, on vous propose le nom BalanceCsv.



