# coding: utf-8

from direct.actor.Actor import Actor
from panda3d.core import * #Pour la classe Vec3

#Pour accepter des évènements Panda, notre classe doit pouvoir les recevoir
from direct.showbase.DirectObject import DirectObject


class CourseurPanda(DirectObject):
    def __init__(self, noeudParent, nomJoueur, positionArrive, touche):
        self.pandaActor = Actor("models/panda-model",
                                {"walk": "models/panda-walk4"})
        self.pandaActor.setScale(0.05, 0.05, 0.05)
        self.pandaActor.setHpr(90.0, 0.0, 0.0)
        self.pandaActor.reparentTo(noeudParent)

        self.positionArrive = positionArrive
        self.nomJoueur = nomJoueur
        self.touche = touche
        
        #On débute l'animation du panda
        self.pandaActor.loop("walk")
        

        #Évènement de touche de clavier
        #voir la page https://www.panda3d.org/manual/index.php/Keyboard_Support
        #pour la liste de touches suggérées

        self.accept(self.touche, self.deplacer, [Vec3(30, 0, 0)])
        self.accept('Course terminee', self.courseTerminee)

    def deplacer(self, deplacement):
        self.pandaActor.setPos(self.pandaActor.getPos()+deplacement)
        messenger.send("panda deplace")
        if self.pandaActor.getPos().x >= self.positionArrive.x:
            messenger.send("Course gagnee", [self])

    def courseTerminee(self, panda):
        self.ignore(self.touche)
        self.pandaActor.stop()


    def placer(self, position):
        self.pandaActor.setPos(position)