# coding: utf-8
from direct.showbase.ShowBase import ShowBase
from direct.actor.Actor import Actor
from panda3d.core import * #Pour la classe Vec3
from direct.interval.IntervalGlobal import Sequence
import random
import sys

from courseurPanda import CourseurPanda
from spectateur import Spectateur

class MyApp(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)

        #Si on appuit sur escape, on quitte l'application
        self.accept('escape', sys.exit)
        self.accept('Course gagnee', self.courseGagnee)

        #Fait apparaître du texte pour chaque message envoyé par le messenger
        #messenger.toggleVerbose()

        #Fait apparaître le widget qui sert à placer les noeuds.
        self.camera.place()
        #Place la caméra
        self.camera.setPos(0, -844, 324)
        self.camera.setHpr(356, 338, 0)

        self.parterre = self.loader.loadModel("models/env")
        #Reparente le parterre à la racine de la scène qui est nomme self.render
        self.parterre.reparentTo(self.render)
        #Change l'échelle
        self.parterre.setScale(500.0, 500.0, 500.0)

        #Charge le carroussel
        self.arbreMagique = loader.loadModel("models/carousel_base")
        self.arbreMagique.setScale(50, 50, 50)
        self.arbreMagique.setPos(Vec3(220, -80, -40))
        self.arbreMagique.reparentTo(render)

        #Création des courseurs pandas
        self.listePanda = [CourseurPanda(self.render, "Danny", self.arbreMagique.getPos(), 'a'), CourseurPanda(self.render, "Cath", self.arbreMagique.getPos(), 'k')]
        #On place les panda sur la ligne de départ
        self.listePanda[0].placer(Vec3(-200, -120, 0))
        self.listePanda[1].placer(Vec3(-200, -30, 0))

        #Création et placement des spectateurs
        self.listeSpectateur = []
        for i in range(0,10):
            #Première rangée (plus au fond)
            randomDeltaX, randomDeltaY = random.uniform(0, 5), random.uniform(0, 100)
            basePosition = Vec3(-170 + randomDeltaX + i * 50.0, randomDeltaY, 0)
            self.listeSpectateur.append(Spectateur(self.render, self.loader, basePosition, self.listePanda[1]))

            #Deuxième rangée (plus près)
            randomDeltaX, randomDeltaY = random.uniform(0, 5), random.uniform(0, 100)
            basePosition = Vec3(-170 + randomDeltaX + i * 50.0, -300 + randomDeltaY, 0)
            self.listeSpectateur.append(Spectateur(self.render, self.loader, basePosition, self.listePanda[0]))

    def courseGagnee(self, panda):
        self.messenger.send('Course terminee', [panda])
        print("panda a gagne: "+panda.nomJoueur)
        text = TextNode('node name')
        text.setText(panda.nomJoueur + " a gagné!")
        textNodePath = aspect2d.attachNewNode(text)
        # centrer
        text.setAlign(TextNode.ACenter)
        # box autour du texte
        textNodePath.setScale(0.15)
        text.setFrameColor(0, 0, 1, 1)
        text.setFrameAsMargin(0.2, 0.2, 0.1, 0.1)
        # couleur de fond
        text.setCardColor(0,0,0, 1)
        text.setCardAsMargin(0, 0, 0, 0)
        text.setCardDecal(True)

        # animation
        dureeSequence = 1.0
        amplitudeTaille = 0.90
        intervalTaille1 = textNodePath.getScale()
        tailleInitiale = textNodePath.getScale()
        tailleFinale = tailleInitiale * amplitudeTaille
        intervalTaille1 = textNodePath.hprScaleInterval(dureeSequence/2, tailleFinale, scale = tailleInitiale, blendType = "easeInOut")
        intervalTaille2 = textNodePath.hprScaleInterval(dureeSequence/2, tailleInitiale, scale = tailleFinale, blendType = "easeInOut")
        self.seqTailleTexte = Sequence(intervalTaille1,intervalTaille2)
        self.seqTailleTexte.loop()

app = MyApp()
app.run()